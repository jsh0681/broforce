#include "stdafx.h"
#include "DeviceMgr.h"

IMPLEMENT_SINGLETON(CDeviceMgr)

CDeviceMgr::CDeviceMgr()
	: m_p3D(nullptr), m_pDevice(nullptr), m_pSprite(nullptr), m_pFont(nullptr)
{
}


CDeviceMgr::~CDeviceMgr()
{
	Release();
}

HRESULT CDeviceMgr::InitDevice()
{
	// 장치 초기화 과정
	// 1. LPDIRECT3D9(IDirect3D9) 객체 생성
	m_p3D = Direct3DCreate9(D3D_SDK_VERSION);

	// 2. 장치 조사
	D3DCAPS9	DeviceCaps;	// 그래픽 카드에서 지원하는 속성들을 저장할 구조체
	ZeroMemory(&DeviceCaps, sizeof(D3DCAPS9));

	// HAL (Hardware Abstraction Layer): 하드웨어 추상화 계층
	if (FAILED(m_p3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &DeviceCaps)))
	{
		ERR_MSG(L"GetDeviceCaps Failed!!");
		return E_FAIL;
	}

	// 현재 장치가 버텍스 프로세싱 지원 유무 조사.
	// 버텍스 프로세싱: 정점 처리 + 조명 처리
	DWORD vp = 0;

	if (DeviceCaps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		vp |= D3DCREATE_HARDWARE_VERTEXPROCESSING;
	else
		vp |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;

	// 3. LPDIRECT3DDEVICE9(IDirect3DDevice9) 객체 생성
	D3DPRESENT_PARAMETERS	d3dpp;
	SetParameters(d3dpp);

	if (FAILED(m_p3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
		g_hWnd, vp, &d3dpp, &m_pDevice)))
	{
		ERR_MSG(L"Create Device Failed!");
		return E_FAIL;
	}

	// 스프라이트 Com객체 생성
	if (FAILED(D3DXCreateSprite(m_pDevice, &m_pSprite)))
	{
		ERR_MSG(L"Create Sprite Failed!");
		return E_FAIL;
	}

	// 폰트 Com객체 생성
	D3DXFONT_DESCW tFontInfo;
	ZeroMemory(&tFontInfo, sizeof(D3DXFONT_DESCW));

	tFontInfo.Height = 20;	// 높이
	tFontInfo.Width = 10;	// 너비
	tFontInfo.Weight = FW_HEAVY;	// 두께
	tFontInfo.CharSet = HANGEUL_CHARSET; // 한글
	lstrcpy(tFontInfo.FaceName, L"궁서"); // 글씨체	

	if (FAILED(D3DXCreateFontIndirect(m_pDevice, &tFontInfo, &m_pFont)))
	{
		ERR_MSG(L"Create Font Failed!");
		return E_FAIL;
	}

	return S_OK;
}

void CDeviceMgr::Render_Begin()
{
	// 렌더링 과정
	// 후면버퍼를 비운다 -> 후면버퍼에 그린다 -> 후면버퍼를 전면버퍼 교체

	// 1.후면버퍼를 비운다
	m_pDevice->Clear(0, nullptr, D3DCLEAR_STENCIL | D3DCLEAR_ZBUFFER | D3DCLEAR_TARGET,
		D3DCOLOR_ARGB(255, 0, 0, 255), 1.f, 0);

	// 2.후면버퍼에 그린다.
	// BeginScene: 여기서부터 후면버퍼에 그리기를 시작한다.
	m_pDevice->BeginScene();
	m_pSprite->Begin(D3DXSPRITE_ALPHABLEND);
}

void CDeviceMgr::Render_End(HWND hWnd)
{
	// 여기까지가 그리기 끝이다.
	m_pSprite->End();
	m_pDevice->EndScene();

	// 3.후면버퍼를 전면버퍼로 교체하여 최종 화면에 출력
	m_pDevice->Present(nullptr, nullptr, hWnd, nullptr);
}

void CDeviceMgr::Release()
{
	// 소멸 순서 주의!
	if (m_pFont)
		m_pFont->Release();

	if (m_pSprite)
		m_pSprite->Release();

	if (m_pDevice)
		m_pDevice->Release();

	if (m_p3D)
		m_p3D->Release();
}

void CDeviceMgr::SetParameters(D3DPRESENT_PARAMETERS & d3dpp)
{
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));

	d3dpp.BackBufferWidth = WINCX;	// 후면버퍼 가로 크기
	d3dpp.BackBufferHeight = WINCY;	// 후면버퍼 세로 크기
	d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;	// 후면버퍼 픽셀 포맷
	d3dpp.BackBufferCount = 1;	// 후면버퍼 개수

	d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;	// 멀티 샘플링 사용 안함.
	d3dpp.MultiSampleQuality = 0;	// 멀티 샘플링 품질

									// 스왑체인 방식
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;

	d3dpp.hDeviceWindow = g_hWnd;	// 장치를 사용할 윈도우
	d3dpp.Windowed = TRUE;	// 창 모드

	d3dpp.EnableAutoDepthStencil = TRUE; // Directx가 자동으로 깊이버퍼와 스텐실버퍼를 생성하고 관리.
	d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;

	// 전체화면시 모니터 주사율을 알아서 지정함.
	d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;

	// 모니터 주사율과 현재 FPS의 간격 설정.
	// D3DPRESENT_INTERVAL_IMMEDIATE: FPS에 따라 즉시 시연.
	// D3DPRESENT_INTERVAL_DEFAULT: 적절한 간격을 Direct가 판단함. 보통 모니터 주사율을 따라감.
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
}
