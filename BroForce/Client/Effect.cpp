#include "stdafx.h"
#include "Effect.h"
#include "EffectImp.h"

CEffect::CEffect()
	: m_pBridge(nullptr), m_iEffectType(0.f), m_fPopPow(0.f), m_fPopAcc(0.f), m_bIsThrow(true),
	m_fRotateAngle(0.f)
{
}


CEffect::~CEffect()
{
	Release();
}

HRESULT CEffect::Initialize() 
{
	m_bIsThrow = true;
	m_tInfo.vSize = { 1.f, 1.f, 0.f };
	m_fSpeed = 3.f;

	return S_OK;
}

void CEffect::UpdateRect()
{
}

void CEffect::LateInit()
{
}

int CEffect::Update()
{
	CObj::LateInit();
	m_fRotateAngle += 10.f;
	if (m_iEffectType == EFFECT_MOVING)
	{

		float fX = m_fPopPow*m_fPopAcc*cosf(D3DXToRadian(m_fAngle));
		float fParabola = m_fPopPow * m_fPopAcc*sinf(D3DXToRadian(m_fAngle)) - GRAVITY * m_fPopAcc * m_fPopAcc * 0.5f;

		m_tInfo.vPos.x += fX * m_tInfo.vSize.x;
		m_tInfo.vPos.y -= fParabola;
		m_fPopAcc += 0.05f; // 가속도는 꾸준히 증가한다.


		D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

		D3DXMATRIX matScale, matRotZ, matTrans;

		D3DXMatrixScaling(&matScale, m_tInfo.vSize.x, m_tInfo.vSize.y, 0.f);
		D3DXMatrixRotationZ(&matRotZ, D3DXToRadian(m_fRotateAngle));

		D3DXMatrixTranslation(&matTrans, m_tInfo.vPos.x - vScroll.x,
			m_tInfo.vPos.y - vScroll.y, 0.f);

		m_tInfo.matWorld = matScale *matRotZ * matTrans;
	}
	else
	{
		D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

		D3DXMATRIX matScale, matRotZ, matTrans;

		D3DXMatrixScaling(&matScale, m_tInfo.vSize.x, m_tInfo.vSize.y, 0.f);
		D3DXMatrixRotationZ(&matRotZ, D3DXToRadian(m_fAngle)*m_tInfo.vSize.x);

		D3DXMatrixTranslation(&matTrans, m_tInfo.vPos.x - vScroll.x,
			m_tInfo.vPos.y - vScroll.y, 0.f);

		m_tInfo.matWorld = matScale *matRotZ * matTrans;
	}
	return m_pBridge->Update();
}

void CEffect::LateUpdate()
{
	m_pBridge->LateUpdate();
}

void CEffect::Render()
{
	m_pBridge->SetInterface(this);
	m_pBridge->Render();
}

void CEffect::Release()
{
	SafeDelete(m_pBridge);
}
