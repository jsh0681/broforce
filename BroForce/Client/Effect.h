#pragma once
#include "Obj.h"

// �߻���

class CEffectImp;

class CEffect :
	public CObj
{
public:
	CEffect();
	virtual ~CEffect();

public:
	void SetObjKey(const wstring& wstrObyKey) { m_wstrObjKey = wstrObyKey; }
	void SetBridge(CEffectImp* pBridge) { m_pBridge = pBridge; }
	CEffectImp* GetBridge() { return m_pBridge; }
	void SetEffectType(int iEffectType) { m_iEffectType = iEffectType; }
	int GetEffectType() { return m_iEffectType; }
	void SetIsThrow(bool tf) { m_bIsThrow = tf; }
	void SetRoateAngle(float fRotateAngle) { m_fRotateAngle = fRotateAngle; }
	void SetPopPow(float fPopPow) { m_fPopPow = fPopPow; }
public:
	virtual HRESULT Initialize();
	virtual void UpdateRect();
	virtual void LateInit();
	virtual int Update();
	virtual void LateUpdate();
	virtual void Render();
	virtual void Release();

private:
	float m_fRotateAngle;
	int m_iEffectType;
	float m_fPopPow;
	float m_fPopAcc;
	bool m_bIsThrow;

protected:
	CEffectImp*	m_pBridge;
};

