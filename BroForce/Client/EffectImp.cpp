#include "stdafx.h"
#include "EffectImp.h"


CEffectImp::CEffectImp()
	: m_pInterface(nullptr), m_bInit(false), m_wstrStateKey(L"")
{
}


CEffectImp::~CEffectImp()
{
}

void CEffectImp::LateInit()
{
	if (!m_bInit)
	{
		this->LateInit();
		m_bInit = true;
	}
}
