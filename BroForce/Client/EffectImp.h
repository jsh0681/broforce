#pragma once

// ������

class CObj;
class CEffectImp
{
public:
	CEffectImp();
	virtual ~CEffectImp();

public:
	void SetObjKey(const wstring& wstrObjkey) { m_wstrObjKey = wstrObjkey; }
	void SetInterface(CObj* pInterface) { m_pInterface = pInterface; }
	void SetStateKey(const wstring& wstrStateKey) { m_wstrStateKey = wstrStateKey; }
	void SetSpeed(float fSpeed) { m_fSpeed = fSpeed; }
	wstring& GetStateKey() { return m_wstrStateKey; }
public:
	virtual HRESULT Initialize() PURE;
	virtual void LateInit();
	virtual int Update() PURE;
	virtual void LateUpdate() PURE;
	virtual void Render() PURE;
	virtual void Release() PURE;

protected:
	float m_fSpeed;
	CObj*	m_pInterface;
	bool	m_bInit;
	wstring m_wstrStateKey;
	wstring m_wstrObjKey;
};

