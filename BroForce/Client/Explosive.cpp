#include "stdafx.h"
#include "Explosive.h"
#include "Effect.h"
#include "AnimEffect.h"
#include "NonAnimEffect.h"
#include"Grenade.h"
#include"Player.h"

CExplosive::CExplosive() :m_fExplosiveTime(0.f), m_bIsStartCount(false)
{
}


CExplosive::~CExplosive()
{
	Release();
}

HRESULT CExplosive::Initialize()
{
	m_bIsDead = false;
	m_wstrObjKey = L"Explosive";
	m_wstrExplosiveStateKey = L"Gas";
	m_tExplosiveFrame = { 0.f, 1.f };
	return S_OK;
}

void CExplosive::LateInit()
{
}

void CExplosive::StateChange()
{
	if (m_tObj.byDrawID == 1)
	{
		m_wstrObjKey = L"Explosive";
		m_wstrExplosiveStateKey = L"Gas";
	}
	else if (m_tObj.byDrawID == 2)
	{
		m_wstrObjKey = L"Explosive";
		m_wstrExplosiveStateKey = L"Box";
	}
	else if (m_tObj.byDrawID == 5)
	{
		m_wstrObjKey = L"Explosive";
		m_wstrExplosiveStateKey = L"Cage";
	}
	else if (m_tObj.byDrawID == 7)
	{
		m_wstrObjKey = L"Explosive";
		m_wstrExplosiveStateKey = L"Back";
	}


}
void CExplosive::UpdateRect()
{
	m_tRect.left = LONG(m_tObj.vPos.x - 32 * 0.5);
	m_tRect.top = LONG(m_tObj.vPos.y - 32 * 0.5);
	m_tRect.right = LONG(m_tObj.vPos.x + 32 * 0.5);
	m_tRect.bottom = LONG(m_tObj.vPos.y + 32 * 0.5);
}

int CExplosive::Update()
{
	CObj::LateInit();
	UpdateRect();
	StateChange();
	Move();
	if (g_bDestroy)
	{
		if (this->GetObj().byDrawID == 7)
		{
			this->IsDead();
		}
	}
	if (m_tObj.vPos.x < 0 || g_StageX < m_tObj.vPos.x || m_tObj.vPos.y < 0 || g_StageY < m_tObj.vPos.y)
	{
		IsDead();
	}

	if (m_bIsDead)
	{
		m_bIsStartCount = true;
		m_fExplosiveTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();

		if (m_fExplosiveTime > 0.15f)
		{
			if (m_tObj.byDrawID == 1)
			{
				int iRand = GetRandomNumber(1, 2);
				switch (iRand)
				{
				case 1:
					CSoundMgr::GetInstance()->PlaySoundw(L"Explosion1.wav", CSoundMgr::EFFECT);
					break;
				case 2:
					CSoundMgr::GetInstance()->PlaySoundw(L"Explosion2.wav", CSoundMgr::EFFECT);
					break;
				}
				CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tObj.vPos, L"GrenadeEffect",
					L"BrommandoGrenadeExplosion", { 0.f, 30.f }, 1.5f, EFFECT_COLLISION);
				GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
				float fRand = GetRandomNumber(0, 10);
				GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, 0.f, 0.f), 130.f+ fRand), CObjMgr::GRENADE);
				GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, 0.f, 0.f), 110.f+ fRand), CObjMgr::GRENADE);
				GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, 0.f, 0.f), 85.f+ fRand), CObjMgr::GRENADE);
				GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, 0.f, 0.f), 70.f+ fRand), CObjMgr::GRENADE);
			}
			else if (g_bDestroy&&m_tObj.byDrawID == 7)
			{
				int iRand = GetRandomNumber(1, 2);
				switch (iRand)
				{
				case 1:
					CSoundMgr::GetInstance()->PlaySoundw(L"Explosion1.wav", CSoundMgr::EFFECT);
					break;
				case 2:
					CSoundMgr::GetInstance()->PlaySoundw(L"Explosion2.wav", CSoundMgr::EFFECT);
					break;
				}
				CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tObj.vPos, L"GrenadeEffect",
					L"BrommandoGrenadeExplosion", { 0.f, 30.f }, 1.5f, EFFECT_COLLISION);
				GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
				float fRand = GetRandomNumber(0, 10);
				GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, 0.f, 0.f), 130.f + fRand), CObjMgr::GRENADE);
				GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, 0.f, 0.f), 110.f + fRand), CObjMgr::GRENADE);
				GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, 0.f, 0.f), 85.f + fRand), CObjMgr::GRENADE);
				GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, 0.f, 0.f), 70.f + fRand), CObjMgr::GRENADE);
			}
			return DEAD_OBJ;
		}
	}
	
	D3DXMATRIX matScale, matTrans;
	D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

	D3DXMatrixScaling(&matScale, m_tObj.vSize.x, m_tObj.vSize.y, m_tObj.vSize.z);
	D3DXMatrixTranslation(&matTrans, m_tObj.vPos.x - vScroll.x,
		m_tObj.vPos.y - vScroll.y, m_tObj.vPos.z);

	m_tObj.matWorld = matScale * matTrans;

	return NO_EVENT;
}

void CExplosive::LateUpdate()
{
	MoveExplosiveFrame(1.f);
}

void CExplosive::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey,
		m_wstrExplosiveStateKey, int(m_tExplosiveFrame.fFrame));
	NULL_CHECK(m_pTexInfo);

	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height *0.67f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tObj.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CExplosive::Release()
{
}

void CExplosive::MoveExplosiveFrame(float fSpeed)
{
	m_tExplosiveFrame.fFrame += m_tExplosiveFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tExplosiveFrame.fFrame > m_tExplosiveFrame.fMaxCount)
	{
		m_tExplosiveFrame.fFrame = 0.f;
	}
}
void CExplosive::Move()
{

	const D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
	m_tObj.vPos.y += 4.f;

}