#pragma once
#include "Obj.h"
class CPlayer;

class CExplosive :
	public CObj
{
public:

	

	CExplosive();
	virtual ~CExplosive();
public:
template <typename T>
CObj* CreateGrenade(D3DXVECTOR3 vPos, float fAngle)
{
	return CObjFactory<T>::CreateGrenade(m_tObj.vPos + vPos, m_tObj.vSize, fAngle, 4);
}
	// CObj을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void LateInit();
	void StateChange();
	virtual void UpdateRect() override;
	void Move();
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
public:
	void MoveExplosiveFrame(float fSpeed);
private:
	bool m_bIsStartCount;
	float m_fExplosiveTime;
private:
	FRAME	m_tExplosiveFrame;

	wstring m_wstrExplosiveStateKey = L"";

};

