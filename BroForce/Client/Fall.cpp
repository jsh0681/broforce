#include "stdafx.h"
#include "Fall.h"


CFall::CFall()
{
	m_tFrame.m_wstrObjKey = L"BroHard";
	m_tFrame.m_wstrStateKey = L"Fall";
	m_tFrame.m_fFrameSpeed = 5.f;
	m_tFrame.fFrame = 0.f;
	m_tFrame.fMaxCount = 2.f;
}


CFall::~CFall()
{
}

void CFall::MoveFrame(float fSpeed)
{
	if (m_tFrame.fFrame < m_tFrame.fMaxCount)
	{
		m_tFrame.fFrame += m_tFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	}
}
