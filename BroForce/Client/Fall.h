#pragma once
#include "Motion.h"
class CFall :
	public CMotion
{
public:
	CFall();
	virtual ~CFall();

	// CMotion을(를) 통해 상속됨
	virtual void MoveFrame(float fSpeed) override;
};

