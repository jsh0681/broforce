#include "stdafx.h"
#include "Flag.h"
#include "Mouse.h"


CFlag::CFlag() :m_bIsFlap(false)
{
}


CFlag::~CFlag()
{
	Release();
}

HRESULT CFlag::Initialize()
{
	m_wstrObjKey = L"Flag";
	m_wstrFlagStateKey = L"AmericanFlag";
	m_tFlagFrame = { 0.f, 33.f };

	return S_OK;
}

void CFlag::LateInit()
{
}

int CFlag::Update()
{
	CObj::LateInit();
	UpdateRect();
	return NO_EVENT;
}

void CFlag::LateUpdate()
{
	MoveFlagFrame(1.f);
}

void CFlag::Render()
{
	D3DXMATRIX matScale, matTrans, matWorld;

	const D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
	TCHAR szIndex[MIN_STR] = L"";

	float fScrollX = vScroll.x;
	float fScrollY = vScroll.y;


	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixTranslation(&matTrans, m_tObj.vPos.x - fScrollX,
		m_tObj.vPos.y - fScrollY, 0.f);

	matWorld = matScale * matTrans;

	const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
		L"Stage", L"Obj", 0);
	NULL_CHECK(pTexInfo);

	float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;


	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	if (m_bIsFlap)
	{
		pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey,
			m_wstrFlagStateKey, int(m_tFlagFrame.fFrame));
		NULL_CHECK(pTexInfo);

		fCenterX = pTexInfo->tImgInfo.Width * 0.1f;
		fCenterY = pTexInfo->tImgInfo.Height * 0.5f;
		GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
			&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	}
}

void CFlag::Release()
{

}
void CFlag::UpdateRect()
{
	m_tRect.left = LONG(m_tObj.vPos.x - 32 * 0.5);
	m_tRect.top = LONG(m_tObj.vPos.y - 32 * 0.5);
	m_tRect.right = LONG(m_tObj.vPos.x + 32 * 0.5);
	m_tRect.bottom = LONG(m_tObj.vPos.y + 32 * 0.5);
}


void CFlag::MoveFlagFrame(float fSpeed)
{
	m_tFlagFrame.fFrame += m_tFlagFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tFlagFrame.fFrame > m_tFlagFrame.fMaxCount)
	{
		m_tFlagFrame.fFrame = 0.f;
	}
}

