#pragma once
#include "Obj.h"
class CFlag :
	public CObj
{
public:
	CFlag();
	virtual ~CFlag();
public:
	virtual HRESULT Initialize() override;
	virtual void LateInit();
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
	virtual void UpdateRect() override;
	void SetIsFlap(bool tf) { m_bIsFlap = tf; }
public:
	void MoveFlagFrame(float fSpeed);
private:
	bool m_bIsFlap;
private:
	FRAME	m_tFlagFrame;

	wstring m_wstrFlagStateKey = L"";


};

