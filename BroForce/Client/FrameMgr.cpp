#include "stdafx.h"
#include "FrameMgr.h"

CFrameMgr::CFrameMgr()
	: m_fDeltaTime(0.f), m_fSecPerFrame(0.f), m_iFps(0), m_szFPS(L""), m_fFpsTime(0.f)
{
	ZeroMemory(&m_CurTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_OldTime, sizeof(LARGE_INTEGER));
	ZeroMemory(&m_CpuTick, sizeof(LARGE_INTEGER));
}


CFrameMgr::~CFrameMgr()	
{
	
}

void CFrameMgr::InitFrame(float FramePerSec)
{
	m_fSecPerFrame = 1 / FramePerSec;

	QueryPerformanceCounter(&m_CurTime);
	QueryPerformanceCounter(&m_OldTime);
	QueryPerformanceFrequency(&m_CpuTick);
}

bool CFrameMgr::LockFrame()
{
	QueryPerformanceCounter(&m_CurTime);
	QueryPerformanceFrequency(&m_CpuTick);

	m_fDeltaTime += float(m_CurTime.QuadPart - m_OldTime.QuadPart) / m_CpuTick.QuadPart;
	m_OldTime = m_CurTime;

	if (m_fSecPerFrame <= m_fDeltaTime)
	{
		++m_iFps;
		m_fDeltaTime = 0.f;
		return true;
	}

	return false;
}

void CFrameMgr::RenderFrame()
{
	m_fFpsTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();

	if (1.f < m_fFpsTime)
	{
		swprintf_s(m_szFPS, L"FPS: %d", m_iFps);
		m_iFps = 0;
		m_fFpsTime = 0.f;		
	}	

	D3DXMATRIX matTrans;
	D3DXMatrixTranslation(&matTrans, 10.f, 10.f, 0.f);

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matTrans);
	GET_INSTANCE(CDeviceMgr)->GetFont()->DrawTextW(GET_INSTANCE(CDeviceMgr)->GetSprite(),
		m_szFPS, lstrlen(m_szFPS), nullptr, 0, D3DCOLOR_ARGB(255, 0, 255, 0));
}
