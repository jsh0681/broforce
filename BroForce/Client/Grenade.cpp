#include "stdafx.h"
#include "Grenade.h"
#include"Effect.h"
#include"AnimEffect.h"

bool g_bPlayerKeyLock = false;

CGrenade::CGrenade() :m_fThrowPow(10.f), m_fThrowAcc(0.f), m_bIsThrow(true),m_bIsGround(false), m_iExplosionRange(16), m_fBoomTime(0.f)
{
	ZeroMemory(&m_tGrenadeFrame, sizeof(FRAME));
	ZeroMemory(&m_tEffectFrame, sizeof(FRAME));
	ZeroMemory(&m_tWeapon, sizeof(WEAPON));
}


CGrenade::~CGrenade()
{
	Release();
}

HRESULT CGrenade::Initialize()
{


	m_gCurState = RAMBROGRENADE;
	m_gPreState = m_gCurState;

	m_vPlayerPos = { 0.f,0.f,0.f };
	m_tInfo.vPos = { 0.f,0.f,0.f };
	m_tInfo.vSize = { 0.0f,0.0f,0.0f };
	m_tInfo.vDir = { 1.f, 0.f, 0.f };
	m_tInfo.vLook = { 1.f, 0.f, 0.f };

	m_tWeapon = { 32.f,400.f,64.f,8.f,8.f };


	m_fSpeed = 10.f;

	m_wstrObjKey = L"Grenade";
	m_wstrGrenadeStateKey = L"RamBroGrenade"; 
	m_tGrenadeFrame = { 0.f, 2.f };


	m_wstrEffectStateKey = L"ChuckBroGrenadeExplosion";
	m_tEffectFrame = { 0.f, 30.f };




	return S_OK;
}

void CGrenade::LateInit()
{
}


int CGrenade::Update()
{
	CObj::LateInit();
	if (m_tInfo.vPos.x < 0 || g_StageX < m_tInfo.vPos.x || m_tInfo.vPos.y < 0 || g_StageY < m_tInfo.vPos.y)
	{
		IsDead();
	}
	Ignite();
	if (m_bIsDead)
	{
		CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos, L"GrenadeEffect",
			m_wstrEffectStateKey, m_tEffectFrame, 1.5f, EFFECT_COLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
		int iRand = GetRandomNumber(1, 2);
		switch (iRand)
		{
		case 1:
			CSoundMgr::GetInstance()->PlaySoundw(L"Explosion1.wav", CSoundMgr::EFFECT);
			break;
		case 2:
			CSoundMgr::GetInstance()->PlaySoundw(L"Explosion2.wav", CSoundMgr::EFFECT);
			break;
		}
		g_bPlayerKeyLock = false;

		return DEAD_OBJ;
	}
	UpdateRect();	
	if (g_bPlayerKeyLock)
	{
		KeyCheck();
	}
	GrenadeChange();

	if (!(m_gCurState == BROMMANDOGRENADE)&& !(m_gCurState == BRODREDDGRENADE))
	{
		CheckParabola();
	}
	else if(m_gCurState == BROMMANDOGRENADE)
		Move();
	
	if (m_gCurState == CHUCKBROGRENADE)
	{
		D3DXMATRIX matScale,matRotZ, matTrans;

		D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
		
		D3DXMatrixScaling(&matScale, m_tInfo.vSize.x, m_tInfo.vSize.y, m_tInfo.vSize.z);
		D3DXMatrixRotationZ(&matRotZ, -D3DXToRadian(-45)*m_tInfo.vSize.x);
		D3DXMatrixTranslation(&matTrans, m_tInfo.vPos.x - vScroll.x,
			m_tInfo.vPos.y - vScroll.y, m_tInfo.vPos.z);


		m_tInfo.matWorld = matScale *matRotZ * matTrans;
	}
	else if (m_gCurState == BRODREDDGRENADE)
	{
		
		D3DXMATRIX matScale, matRotZ, matTrans;
		D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

		D3DXMatrixScaling(&matScale, m_tInfo.vSize.x, m_tInfo.vSize.y, m_tInfo.vSize.z);
		D3DXMatrixRotationZ(&matRotZ, -D3DXToRadian(m_fAngle));
		D3DXMatrixTranslation(&matTrans, m_tInfo.vPos.x - vScroll.x,
			m_tInfo.vPos.y - vScroll.y, m_tInfo.vPos.z);

		m_tInfo.matWorld = matScale * matRotZ * matTrans;

		D3DXVec3TransformNormal(&m_tInfo.vDir, &m_tInfo.vLook, &m_tInfo.matWorld);

		m_tInfo.vPos += 3.5f*m_tInfo.vDir;
	}
	else
	{
		D3DXMATRIX matScale, matTrans;

		D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

		D3DXMatrixScaling(&matScale, m_tInfo.vSize.x, m_tInfo.vSize.y, m_tInfo.vSize.z);

		D3DXMatrixTranslation(&matTrans, m_tInfo.vPos.x - vScroll.x,
			m_tInfo.vPos.y - vScroll.y, m_tInfo.vPos.z);


		m_tInfo.matWorld = matScale  * matTrans;
	}
	return NO_EVENT;
}

void CGrenade::LateUpdate()
{

	const D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

	if ((m_tInfo.vPos.x<0.f+ vScroll.x||m_tInfo.vPos.x>800.f+vScroll.x)&&(m_tInfo.vPos.y>600.f + vScroll.y || m_tInfo.vPos.y<0.f + vScroll.y))
		m_bIsDead = true;

	MoveGrenadeFrame(3.f);
}
void CGrenade::KeyCheck()
{
	if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_UP))
	{
		m_fAngle = 90.f*m_tInfo.vSize.x;
	}
	if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_DOWN))
	{
		m_fAngle = 270.f*m_tInfo.vSize.x;
	}
	if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_LEFT))
	{
		if (m_tInfo.vSize.x == 1)
		{
			m_fAngle = 180.f;
		}
		else if (m_tInfo.vSize.x == -1)
		{
			m_fAngle = 0.f;
		}
	}
	if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_RIGHT))
	{
		if (m_tInfo.vSize.x == 1)
		{
			m_fAngle = 0.f;
		}
		else if (m_tInfo.vSize.x == -1)
		{
			m_fAngle = 180.f;
		}
	}

}
void CGrenade::Render()
{
	
	if (m_gCurState == BROMMANDOGRENADE)
	{
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"BrommandoGrenadeSound", CSoundMgr::EFFECT);
		CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(-15.f*m_tInfo.vSize.x, 0.f, 0.f) , L"ShootEffect",
			L"BrommandoBulletSkid", { 0.f, 18.f }, 4.f, EFFECT_NONCOLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
	}
	if (m_gCurState == CHUCKBROGRENADE)
	{

		CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos, m_tInfo.vSize, L"ShootEffect",
			L"BrommandoBulletSkid", { 0.f, 18.f }, 50.f, 5.f, EFFECT_NONCOLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
	}
	if (m_gCurState == FRAGMENT)
	{
		CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos, m_tInfo.vSize, L"ShootEffect",
			L"BrommandoBulletSkid", { 0.f, 18.f }, 50.f, 5.f, EFFECT_NONCOLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
	}
	if (m_gCurState == BRODREDDGRENADE)
	{
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"BrommandoGrenadeSound", CSoundMgr::EFFECT);
		CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(-1.f*m_tInfo.vSize.x, 0.f, 0.f), L"ShootEffect",
			L"BrommandoBulletSkid", { 0.f, 18.f }, 2.5f, EFFECT_NONCOLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
	}
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey, m_wstrGrenadeStateKey, int(m_tGrenadeFrame.fFrame));
	NULL_CHECK(m_pTexInfo);

	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.5f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CGrenade::Release()
{
}




void CGrenade::SetPlayerType(int iPlayerType)
{
	m_gCurState = (GRENADESTATE)iPlayerType;
}

void CGrenade::GrenadeChange()
{
	if (m_gCurState != m_gPreState)
	{
		switch (m_gCurState)
		{
		case RAMBROGRENADE:
			m_iExplosionRange = 16;
			m_wstrGrenadeStateKey = L"RamBroGrenade";
			m_tGrenadeFrame = { 0.f, 2.f };

			m_wstrEffectStateKey = L"ChuckBroGrenadeExplosion";
			m_tEffectFrame = {0.f,30.f};

			m_tWeapon = { 100.f,400.f,32.f,8.f,8.f };
			break;


		case BROHARDGRENADE:
			m_iExplosionRange = 100;
			m_wstrGrenadeStateKey = L"BroHardGrenade";
			m_tGrenadeFrame = { 0.f, 1.f };

			m_wstrEffectStateKey = L"BroHardGrenadeExplosion";
			m_tEffectFrame = {0.f,21.f};

			m_tWeapon = { 100.f,400.f,200.f,8.f,8.f };
			break;

		case CHUCKBROGRENADE:
			m_iExplosionRange = 16;
			m_wstrGrenadeStateKey = L"ChuckBroGrenade";
			m_tGrenadeFrame = { 0.f, 5.f };

			m_wstrEffectStateKey = L"ChuckBroGrenadeExplosion"; 
			m_tEffectFrame = {0.f,30.f};

			m_tWeapon = { 100.f,400.f,32.f,8.f,8.f };
			break;

		case BROMMANDOGRENADE:
			m_iExplosionRange = 16;
			m_wstrGrenadeStateKey = L"BrommandoGrenade";
			m_tGrenadeFrame = { 0.f, 5.f };

			m_wstrEffectStateKey = L"BrommandoGrenadeExplosion"; 
			m_tEffectFrame = {0.f,30.f};

			m_tWeapon = { 100.f,400.f,64.f,8.f,8.f };
			break;

		case FRAGMENT:
			m_iExplosionRange = 16;

			m_wstrGrenadeStateKey = L"Fragment";
			m_tGrenadeFrame = { 0.f, 25.f };

			m_wstrEffectStateKey = L"FragmentExplosion";
			m_tEffectFrame = { 0.f,25.f };

			m_tWeapon = { 32.f,100.f,8.f,8.f,8.f };
			break;
		case BRODREDDGRENADE:
			m_iExplosionRange = 16;
			m_wstrGrenadeStateKey = L"BroDreddGrenade";
			m_tGrenadeFrame = { 0.f, 4.f };

			m_wstrEffectStateKey = L"BrommandoGrenadeExplosion";
			m_tEffectFrame = { 0.f,30.f };

			m_tWeapon = { 100.f,400.f,64.f,8.f,8.f };
			g_bPlayerKeyLock = true;// 플레이어 키입력못받고 유도미사일쏘기 위한 변수 
			break;
		}
		m_gPreState = m_gCurState;
	}
}

void CGrenade::Move()
{
	m_tInfo.vPos.x += m_fSpeed*m_tInfo.vSize.x;
	m_tInfo.vPos.y += cos(D3DXToRadian(m_tInfo.vPos.x))*3;
}
void CGrenade::MoveGrenadeFrame(float fSpeed)
{
	m_tGrenadeFrame.fFrame += m_tGrenadeFrame.fMaxCount*fSpeed*GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tGrenadeFrame.fFrame > m_tGrenadeFrame.fMaxCount)
		m_tGrenadeFrame.fFrame = 0.f;
}

void CGrenade::CheckParabola()//포물선운동 체크 
{
	float fX=0.f;
	if (m_bIsThrow&&!m_bIsGround)
	{
	//	x=속도*cos세타*시간
	//	y=속소*sin세타*시간-0.5*9.81*시간*시간
		fX = m_fThrowPow*m_fThrowAcc*cosf(D3DXToRadian(m_fAngle));
		float fParabola = m_fThrowPow * m_fThrowAcc*sinf(D3DXToRadian(m_fAngle)) -
			GRAVITY * m_fThrowAcc * m_fThrowAcc * 0.5f;


		m_tInfo.vPos.x += fX*m_tInfo.vSize.x;
		m_tInfo.vPos.y -= fParabola;
		m_fThrowAcc += 0.05f; // 가속도는 꾸준히 증가한다.
	}
	else if(m_gCurState == RAMBROGRENADE)
	{
		m_tInfo.vPos.y += 3.5f;
	}
}

void CGrenade::UpdateRect()
{
	m_tRect.left = LONG(m_tInfo.vPos.x - m_tWeapon.fImageSizeX*0.5);
	m_tRect.top = LONG(m_tInfo.vPos.y - m_tWeapon.fImageSizeY*0.5);
	m_tRect.right = LONG(m_tInfo.vPos.x + m_tWeapon.fImageSizeX*0.5);
	m_tRect.bottom = LONG(m_tInfo.vPos.y + m_tWeapon.fImageSizeY*0.5);
}
void CGrenade::Ignite()
{
	if (m_gCurState == RAMBRO_GRENADE)
	{
		if (m_bIsGround)
		{
			m_fBoomTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			if (m_fBoomTime > 2.f)
			{
				m_bIsDead = true;
				m_fBoomTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			}
		}
	}
}