#pragma once
#include "Obj.h"
class CGrenade :
	public CObj
{
	enum GRENADESTATE { RAMBROGRENADE, BROHARDGRENADE, CHUCKBROGRENADE, BROMMANDOGRENADE, FRAGMENT,BRODREDDGRENADE};
public:
	CGrenade();
	virtual ~CGrenade();

	// CObj��(��) ���� ��ӵ�
	virtual HRESULT Initialize() override;
	virtual void LateInit();
	virtual void UpdateRect() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	void KeyCheck();
	virtual void Render() override;
	virtual void Release() override;	
	
public:

public:
	void Ignite();
	GRENADESTATE& GetGrenadeState() { return m_gCurState; }
	void GrenadeChange();
	void SetPlayerType(int iPlayerType);
	void CheckParabola();
	void Move();
	void MoveGrenadeFrame(float fSpeed);
	void SetIsThrow(bool tf) { m_bIsThrow = tf; }
	void SetIsGround(bool tf) { m_bIsGround = tf; }
	const WEAPON& GetWeapon() const { return m_tWeapon; }
private:
	float m_fBoomTime;
	bool m_bIsGround;
	bool m_bIsThrow;
private:
	int m_iExplosionRange; //���� ����

private:
	float m_fThrowPow;
	float m_fThrowAcc;

private:

	WEAPON m_tWeapon;

	wstring m_wstrEffectStateKey = L"";
	FRAME	m_tEffectFrame;
	D3DXVECTOR3 m_vPlayerPos;

	FRAME	m_tGrenadeFrame;

	wstring m_wstrGrenadeStateKey = L"";
	GRENADESTATE m_gCurState;
	GRENADESTATE m_gPreState;
};

