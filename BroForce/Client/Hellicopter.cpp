#include "stdafx.h"
#include "Hellicopter.h"
CHellicopter::CHellicopter() :m_bBroCarry(false)
{
	ZeroMemory(&m_tFrame, sizeof(FRAME));
}


CHellicopter::~CHellicopter()
{
	Release();
}

HRESULT CHellicopter::Initialize()
{
	m_tInfo.vPos = { 0.f,0.f,0.f};
	m_tInfo.vSize = { 1.f, 1.f,0.f };
	m_wstrObjKey = L"Stage";
	m_wstrStateKey = L"Hellicopter";
	m_tFrame = { 0.f,4.f };
	return S_OK;
}
void CHellicopter::UpdateRect()
{
}

void CHellicopter::LateInit()
{
	
}

int CHellicopter::Update()
{
	
	CObj::LateInit();
	if (m_tInfo.vPos.x <= g_StageX-600.f)
	{
		m_tInfo.vPos.x += 8.f;
	}
	if (m_bBroCarry)
	{
		g_bDestroy = true;
		m_bBroCarry = false;
	}

	D3DXMATRIX matScale, matTrans;
	D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
	D3DXMatrixScaling(&matScale, m_tInfo.vSize.x, m_tInfo.vSize.y, m_tInfo.vSize.z);
	D3DXMatrixTranslation(&matTrans, m_tInfo.vPos.x - vScroll.x,
		m_tInfo.vPos.y - vScroll.y, m_tInfo.vPos.z);

	m_tInfo.matWorld = matScale * matTrans;
	
	return NO_EVENT;
}

void CHellicopter::LateUpdate()
{
	MoveFrame(3.f);
}

void CHellicopter::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey, m_wstrStateKey, (int)m_tFrame.fFrame);
	NULL_CHECK(m_pTexInfo);

	float fCenterX = m_pTexInfo->tImgInfo.Width*0.5;
	float fCenterY = m_pTexInfo->tImgInfo.Height*0.5;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));

}

void CHellicopter::Release()
{
}


void CHellicopter::MoveFrame(float fSpeed)
{
	m_tFrame.fFrame += m_tFrame.fMaxCount*fSpeed*GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tFrame.fFrame > m_tFrame.fMaxCount)
	{
		m_tFrame.fFrame = 0;
	}

}
