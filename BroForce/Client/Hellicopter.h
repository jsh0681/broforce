#pragma once
#include "Obj.h"
class CHellicopter :
	public CObj
{
public:
	CHellicopter();
	virtual ~CHellicopter();

public:
	// CObj을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void UpdateRect() override;
	virtual void LateInit();
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
	bool GetBroCarry() { return m_bBroCarry; }
public:
	void MoveFrame(float fSpeed);
	void SetBroCarry(bool tf) { m_bBroCarry = tf; }
private:
	bool m_bBroCarry;
	wstring m_wstrStateKey = L"";
	FRAME m_tFrame; 

};

