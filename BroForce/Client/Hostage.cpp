#include "stdafx.h"
#include "Hostage.h"
#include"Player.h"
#include"Effect.h"
#include"AnimEffect.h"

CHostage::CHostage()
{
}


CHostage::~CHostage()
{
	Release();
}

HRESULT CHostage::Initialize()
{
	m_tObj.vSize = { 1.0f,1.0f,0.0f };
	m_tObj.vDir = { 1.0f,0.0f,0.0f };
	m_tObj.vLook = { 1.0f,0.0f,0.0f };
	m_wstrObjKey = L"Hostage";	
	m_wstrStateKey = L"HostageIdle";
	m_tFrame = { 0.f,1.f };
	m_hCurState = IDLE;
	m_hPreState = m_hCurState;

	return S_OK;
}

void CHostage::UpdateRect()
{
}

int CHostage::Update()
{
	CObj::LateInit();
	StateChange();
	MoveFrame();

	if (m_bIsDead)
		return DEAD_OBJ;


	if (m_bIsChange)
	{
		m_hCurState = CHANGE;
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"YeahBroYeah.wav", CSoundMgr::UI);
		m_bIsChange = false;
	}
	m_tObj.vPos.y += 2.f;
	D3DXMATRIX matScale, matTrans;
	D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

	D3DXMatrixScaling(&matScale, m_tObj.vSize.x, m_tObj.vSize.y, m_tObj.vSize.z);
	D3DXMatrixTranslation(&matTrans, m_tObj.vPos.x - vScroll.x,
		m_tObj.vPos.y - vScroll.y, m_tObj.vPos.z);

	m_tObj.matWorld = matScale * matTrans;
	return 0;
}

void CHostage::LateInit()
{
}

void CHostage::LateUpdate()
{
}

void CHostage::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey , m_wstrStateKey, (int)m_tFrame.fFrame);
	NULL_CHECK(m_pTexInfo);
	UpdateRect();


	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.76f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tObj.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CHostage::Release()
{
}
void CHostage::MoveFrame()
{
	m_tFrame.fFrame += m_tFrame.fMaxCount*1.5f*GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tFrame.fFrame > m_tFrame.fMaxCount)
	{
		if (m_hCurState == CHANGE)
		{
			CPlayer* m_pPlayer = dynamic_cast<CPlayer*>(GET_INSTANCE(CObjMgr)->GetPlayer());
			m_pPlayer->LifeUp();
			m_pPlayer->SetPos(m_tObj.vPos);
			IsDead();
		}
		else 
			m_tFrame.fFrame = 0;
	}
}

void CHostage::StateChange()
{
	if (m_hCurState != m_hPreState)
	{
		switch (m_hCurState)
		{
		case HOSTAGESTATE::IDLE:
			m_wstrStateKey = L"HostageIdle";
			m_tFrame = { 0.f,1.f };
			break;

		case HOSTAGESTATE::CHANGE:

			m_wstrStateKey = L"HostageChange";
			m_tFrame = { 0.f,11.f };
			break;
		}
		m_hPreState = m_hCurState;
	}
}