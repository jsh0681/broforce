#pragma once
#include "Obj.h"
class CHostage :
	public CObj
{
public:
	enum HOSTAGESTATE {IDLE, CHANGE};
public:
	CHostage();
	virtual ~CHostage();

	// CObj을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void UpdateRect() override;
	virtual int Update() override;
	virtual void LateInit();
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
	void MoveFrame();
	bool GetIsDetect() { return m_bIsDetect; }
	void SetIsDetect(bool tf) { m_bIsDetect = tf; }
	void StateChange();
	void SetIsChange(bool tf) { m_bIsChange = tf; }
	bool GetIsChange() { return m_bIsChange; }
private:
	bool m_bIsDetect;
	HOSTAGESTATE m_hCurState;
	HOSTAGESTATE m_hPreState;
	bool m_bIsChange;
	wstring m_wstrStateKey = L"";
	FRAME m_tFrame;
};

