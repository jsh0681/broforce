#pragma once
#include "Motion.h"

class CJump :
	public CMotion
{
public:
	CJump();
	virtual ~CJump();

	// CMotion을(를) 통해 상속됨
	virtual void MoveFrame(float fSpeed) override;
};

