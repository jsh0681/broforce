#pragma once
class CKeyMgr
{
	DECLARE_SINGLETON(CKeyMgr)

public:
	CKeyMgr();
	~CKeyMgr();

public:
	void KeyCheck();
	bool KeyUp(DWORD dwKey);
	bool KeyDown(DWORD dwKey);
	bool KeyPressing(DWORD dwKey);
	bool KeyCombine(DWORD dwFirst, DWORD dwSecond);

	bool KeyCombine2(DWORD dwFirst, DWORD dwSecond);

private:
	DWORD m_dwKey;
	DWORD m_dwKeyPressed;
	DWORD m_dwKeyDown;
};

