#include "stdafx.h"
#include "Logo.h"


CLogo::CLogo():m_fSelectBarY(0.f)
{
	ZeroMemory(&m_tFrame, sizeof(FRAME));
}


CLogo::~CLogo()
{
	Release();
}

HRESULT CLogo::Initialize()
{
	if (FAILED(GET_INSTANCE(CTextureMgr)->ReadImgPath(L"../Data/ImgPath.txt")))
	{
		ERR_MSG(L"../Data/ImgPath.txt");
		return E_FAIL;
	}

	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/LogoBackGround.png", L"LogoBackGround")))
	{
		ERR_MSG(L"../Data/LogoBackGround.png");
		return E_FAIL;
	}

	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/StartGame.png", L"StartGame")))
	{
		ERR_MSG(L"../Data/StartGame.png");
		return E_FAIL;
	}
	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/ExitGame.png", L"ExitGame")))
	{
		ERR_MSG(L"../Data/ExitGame.png");
		return E_FAIL;
	}
	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/MenuBar.png", L"MenuBar")))
	{
		ERR_MSG(L"../Data/MenuBar.png");
		return E_FAIL;
	}
	
	m_wstrObjKey = L"Logo";
	m_wstrStateKey = L"AnimatedLogo";
	m_tFrame = { 0.f,51.f };
	m_fSelectBarY =  350.f ; 
	GET_INSTANCE(CSoundMgr)->PlayBGM(L"MenuIdle.wav");
	return S_OK;
}

void CLogo::Update()
{

	KeyCheck();
	MoveFrame();
}

void CLogo::LateUpdate()
{
}

void CLogo::Render()
{
	const D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

	const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"LogoBackGround");
	NULL_CHECK(pTexInfo);

	float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

	D3DXMATRIX matScale, matTrans, matWorld;

	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixTranslation(&matTrans, pTexInfo->tImgInfo.Width * 0.5f - vScroll.x, pTexInfo->tImgInfo.Height * 0.5f - vScroll.y, 0.f);

	matWorld = matScale  * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	////////////////////////////////////////////////////////////////////////////////////////////
	pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"StartGame");
	NULL_CHECK(pTexInfo);

	fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	fCenterY = pTexInfo->tImgInfo.Height * 0.5f;


	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixTranslation(&matTrans, 400.f,350.f, 0.f);

	matWorld = matScale  * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));


	pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"ExitGame");
	NULL_CHECK(pTexInfo);

	fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixTranslation(&matTrans, 400.f, 400.f, 0.f);

	matWorld = matScale  * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	//////////////////////////////////////////////////////////////////////////////////////////////////
	

	pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"MenuBar");
	NULL_CHECK(pTexInfo);

	fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixTranslation(&matTrans, 400.f, m_fSelectBarY, 0.f);

	matWorld = matScale  * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	///////////////////////////////////////////////////////////////////////////////////////////////////
	pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey,m_wstrStateKey,(int)m_tFrame.fFrame);
	NULL_CHECK(pTexInfo);

	fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

	D3DXMatrixScaling(&matScale, 1.7f, 1.7f, 0.f);
	D3DXMatrixTranslation(&matTrans, WINCX*0.5, WINCY*0.4, 0.f);

	matWorld = matScale * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CLogo::Release()
{
	CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
}

void CLogo::MoveFrame()
{
	m_tFrame.fFrame += m_tFrame.fMaxCount*1.f*GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tFrame.fFrame > m_tFrame.fMaxCount)
	{
		m_tFrame.fFrame = m_tFrame.fMaxCount - 1;
	}
}
void CLogo::KeyCheck()
{
	if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_UP))
	{
		
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"MenuChange.wav",CSoundMgr::UI);
		m_fSelectBarY = 350.f;
	}
	if(GET_INSTANCE(CKeyMgr)->KeyDown(KEY_DOWN))
	{
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"MenuChange.wav", CSoundMgr::UI);
		m_fSelectBarY = 400.f;
	}
	if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_ENTER))
	{
		if (m_fSelectBarY == 350.f)
		{
			GET_INSTANCE(CSoundMgr)->PlaySoundw(L"MenuStart.wav", CSoundMgr::UI);
			GET_INSTANCE(CSceneMgr)->SceneChange(CSceneMgr::SCENE_ID::STAGEONE);
		}
		if (m_fSelectBarY == 400.f)
		{
			GET_INSTANCE(CSoundMgr)->PlaySoundw(L"MenuStart.wav", CSoundMgr::UI);
			DestroyWindow(g_hWnd);
		}

	}
}
