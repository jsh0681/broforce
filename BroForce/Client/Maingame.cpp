#include "stdafx.h"
#include "Maingame.h"

bool g_bDestroy = false;
float g_StageX = 0.f;
float g_StageY = 0.f;
CMaingame::CMaingame()
{
	
}


CMaingame::~CMaingame()
{
	Release();
}

HRESULT CMaingame::Initialize()
{

	if (FAILED(GET_INSTANCE(CDeviceMgr)->InitDevice()))
	{
		ERR_MSG(L"DeviceMgr Init Failed!!");
		return E_FAIL;
	}

	GET_INSTANCE(CTimeMgr)->InitTime();
	GET_INSTANCE(CSoundMgr)->Initialize();
	if (FAILED(GET_INSTANCE(CSceneMgr)->SceneChange(CSceneMgr::LOGO)))
	{
		ERR_MSG(L"Stage Load Failed!");
		return E_FAIL;
	}


	return S_OK;
}

void CMaingame::Update()
{
	GET_INSTANCE(CSoundMgr)->Update();
	GET_INSTANCE(CKeyMgr)->KeyCheck();
	GET_INSTANCE(CTimeMgr)->UpdateTime();
	GET_INSTANCE(CSceneMgr)->Update();
}

void CMaingame::LateUpdate()
{
	GET_INSTANCE(CSceneMgr)->LateUpdate();
}

void CMaingame::Render(CFrameMgr& frameMgr)
{
	GET_INSTANCE(CDeviceMgr)->Render_Begin();

	GET_INSTANCE(CSceneMgr)->Render();
	frameMgr.RenderFrame();

	GET_INSTANCE(CDeviceMgr)->Render_End();
}

void CMaingame::Release()
{
	GET_INSTANCE(CKeyMgr)->DestroyInstance();
	GET_INSTANCE(CAstarMgr)->DestroyInstance();
	GET_INSTANCE(CTimeMgr)->DestroyInstance();
	GET_INSTANCE(CSceneMgr)->DestroyInstance();
	GET_INSTANCE(CObjMgr)->DestroyInstance();
	GET_INSTANCE(CDataSubject)->DestroyInstance();
	GET_INSTANCE(CSoundMgr)->DestroyInstance();
	GET_INSTANCE(CTextureMgr)->DestroyInstance();
	GET_INSTANCE(CDeviceMgr)->DestroyInstance();
}
