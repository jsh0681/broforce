#pragma once

#include "FrameMgr.h"

class CMaingame
{
public:
	CMaingame();
	~CMaingame();

public:
	HRESULT Initialize();
	void Update();
	void LateUpdate();
	void Render(CFrameMgr& frameMgr);
	void Release();
};

