#include "stdafx.h"
#include "MathMgr.h"


CMathMgr::CMathMgr()
{
}


CMathMgr::~CMathMgr()
{
}

float CMathMgr::CalcDistance(const CObj* Dst, const CObj* Src)
{
	D3DXVECTOR3 vDir = Dst->GetInfo().vPos - Src->GetInfo().vPos;
	return D3DXVec3Length(&vDir);
}

float CMathMgr::CalcRadian(const CObj* Dst, const CObj* Src, const D3DXVECTOR3& vAxis)
{
	D3DXVECTOR3 vDir = Dst->GetInfo().vPos - Src->GetInfo().vPos;
	D3DXVECTOR3 vLook = {};

	D3DXVec3Normalize(&vDir, &vDir);
	D3DXVec3Normalize(&vLook, &vAxis);

	float fCos = D3DXVec3Dot(&vDir, &vLook);
	float fAngle = acosf(fCos);

	if (Src->GetInfo().vPos.y < Dst->GetInfo().vPos.y)
		fAngle *= -1.f;

	return fAngle;
}

float CMathMgr::CalcDegree(const CObj * Dst, const CObj * Src, const D3DXVECTOR3& vAxis
)
{
	D3DXVECTOR3 vDir = Dst->GetInfo().vPos - Src->GetInfo().vPos;
	D3DXVECTOR3 vLook = {};

	D3DXVec3Normalize(&vDir, &vDir);
	D3DXVec3Normalize(&vLook, &vAxis);

	float fCos = D3DXVec3Dot(&vDir, &vLook);
	float fAngle = acosf(fCos);

	if (Src->GetInfo().vPos.y < Dst->GetInfo().vPos.y)
		fAngle *= -1.f;

	return D3DXToDegree(fAngle);
}
