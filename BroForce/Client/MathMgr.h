#pragma once

#include "Obj.h"

class CMathMgr
{
public:
	CMathMgr();
	~CMathMgr();

public:
	static float CalcDistance(const CObj* Dst, const CObj* Src);
	static float CalcRadian(const CObj* Dst, const CObj* Src, const D3DXVECTOR3& vAxis);
	static float CalcDegree(const CObj* Dst, const CObj* Src, const D3DXVECTOR3& vAxis);
};

