#include "stdafx.h"
#include "MegaHellicopter.h"
#include"Bullet.h"
#include"Player.h"
#include"AnimEffect.h"
#include"Effect.h"
#include"NonAnimEffect.h"


CMegaHellicopter::CMegaHellicopter() :m_bIsDetect(false), m_bIsReady(false), m_fReadyTime(0.f), m_fShootTime(0.f), m_fReloadTime(0.f),
m_bPatternOne(false), m_bPatternTwo(false), m_bPatternThree(false), m_bPatternFour(false), m_bPatternFive(false), m_bPatternSix(true)
, m_bHeadDead(false), m_bBodyDead(false), m_bSetPos(false), m_fPlayerCurPos(0.f),m_iHp(600.f), m_fBoomTime(0.f)
{
}


CMegaHellicopter::~CMegaHellicopter()
{
	Release();
}

HRESULT CMegaHellicopter::Initialize()
{
	m_tInfo.vPos = { 2150.f,1767.f,0.f };
	m_tInfo.vSize = { 1.0f,1.0f,0.f };
	m_tInfo.vDir = { 1.0f,0.f,0.f };
	m_tInfo.vLook = { 1.0f,0.f,0.f };

	m_fSpeed = 3.f;

	m_wstrObjKey = L"MegaHelliCopterHead";
	m_wstrObjKey2 = L"MegaHelliCopterBody";

	m_wstrHeadStateKey = L"Idle";
	m_wstrBodyStateKey = L"Idle";

	m_hCurState = HEADIDLE;
	m_hPreState = m_hCurState;

	m_bCurState = BODYIDLE;
	m_bPreState = m_bCurState;

	m_tHeadFrame = { 0.f, 1.f };
	m_tBodyFrame = { 0.f, 8.f };
	m_fAngle = 0.f;
	return S_OK;
}


void CMegaHellicopter::LateInit()
{
	m_pPlayer = dynamic_cast<CPlayer*>(GET_INSTANCE(CObjMgr)->GetPlayer());
}

int CMegaHellicopter::Update()
{
	CObj::LateInit(); 
	if (m_tInfo.vPos.x < 0 || g_StageX < m_tInfo.vPos.x || m_tInfo.vPos.y < 0 || g_StageY < m_tInfo.vPos.y)
	{
		CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(0.f, -10.f, 0.f), L"BulletEffect",
			L"BrommandoBulletPuff", { 0.f,30.f }, 3.5f, EFFECT_NONCOLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
		pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(70.f, -10.f, 0.f), L"BulletEffect",
			L"BrommandoBulletPuff", { 0.f,30.f }, 2.0f, EFFECT_NONCOLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
		pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(0.f, 50.f, 0.f), L"BulletEffect",
			L"BrommandoBulletPuff", { 0.f,30.f }, 3.0f, EFFECT_NONCOLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
		pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(-60.f, -30.f, 0.f), L"BulletEffect",
			L"BrommandoBulletPuff", { 0.f,30.f }, 2.5f, EFFECT_NONCOLLISION);

		pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(-60.f, 0.f, 0.f), L"BulletEffect",
			L"BrommandoBulletPuff", { 0.f,30.f }, 4.5f, EFFECT_NONCOLLISION);

		pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(60.f, 50.f, 0.f), L"BulletEffect",
			L"BrommandoBulletPuff", { 0.f,30.f }, 2.5f, EFFECT_NONCOLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
		IsDead();
	}

	if (m_iHp < 300)
	{
		m_bHeadDead = true;
		if (m_iHp < 100)
		{
			m_bBodyDead = true;
		}
	}
	
	if (m_bIsDead)
	{
		if (!m_bIsBombShoot)
		{
			int iRand = GetRandomNumber(1, 2);
			switch (iRand)
			{
			case 1:
				CSoundMgr::GetInstance()->PlaySoundw(L"Explosion1.wav", CSoundMgr::EFFECT);
				break;
			case 2:
				CSoundMgr::GetInstance()->PlaySoundw(L"Explosion2.wav", CSoundMgr::EFFECT);
				break;
			}
			CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(0.f, -10.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 3.5f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(70.f, -10.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 2.0f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(0.f, 50.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 3.0f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(-60.f, -30.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 2.5f, EFFECT_NONCOLLISION);

			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(-60.f, 0.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 4.5f, EFFECT_NONCOLLISION);

			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(60.f, 50.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 2.5f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);

			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(100.f, -10.f, 0.f), L"BulletEffect",
					L"BrommandoBulletPuff", { 0.f,30.f }, 2.0f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(0.f, 80.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 3.0f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(-90.f, 0.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 2.5f, EFFECT_NONCOLLISION);

			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(-80.f, 60.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 4.5f, EFFECT_NONCOLLISION);

			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(80.f, 60.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 2.5f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
		}
		m_bIsBombShoot = true;
		m_bCurState = BODYDEADALL;
		m_hCurState = HEADDEADALL;
		m_tInfo.vPos.y += 2.f;
	}


	ChangeState();
	HeadStateChange();
	BodyStateChange();

	MoveBodyFrame(1.f);
	MoveHeadFrame(1.f);

	

	D3DXMATRIX matScale, matRotZ, matTrans;
	D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
	D3DXMatrixScaling(&matScale, m_tInfo.vSize.x, m_tInfo.vSize.y, m_tInfo.vSize.z);
	D3DXMatrixRotationZ(&matRotZ, D3DXToRadian(m_fAngle));
	D3DXMatrixTranslation(&matTrans, m_tInfo.vPos.x - vScroll.x, m_tInfo.vPos.y - vScroll.y, m_tInfo.vPos.z);

	m_tInfo.matWorld = matScale*matRotZ*matTrans;

	return NO_EVENT;
}

void CMegaHellicopter::LateUpdate()
{
}

void CMegaHellicopter::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey,
		m_wstrHeadStateKey, int(m_tHeadFrame.fFrame));
	NULL_CHECK(m_pTexInfo);


	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.89f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.5f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));

	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey2,
		m_wstrBodyStateKey, int(m_tBodyFrame.fFrame));
	NULL_CHECK(m_pTexInfo);
	fCenterX = m_pTexInfo->tImgInfo.Width * 0.1f;
	fCenterY = m_pTexInfo->tImgInfo.Height * 0.5f;
	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CMegaHellicopter::Release()
{
}

void CMegaHellicopter::MoveBodyFrame(float fSpeed)
{
	m_tBodyFrame.fFrame += m_tBodyFrame.fMaxCount*m_fSpeed*GET_INSTANCE(CTimeMgr)->GetDeltaTime();

	if (m_tBodyFrame.fFrame > m_tBodyFrame.fMaxCount)
	{
		if (m_bCurState == BODYMISSILE)
		{
			m_bIsMissileShoot = true;
			m_tBodyFrame.fFrame = 0;
		}
		else if (m_bCurState == BODYBOMB)
		{
			m_bIsBombShoot = true;
			m_tBodyFrame.fFrame = m_tBodyFrame.fMaxCount - 1;
		}
		else
			m_tBodyFrame.fFrame = 0;
	}
}

void CMegaHellicopter::MoveHeadFrame(float fSPeed)
{
	m_tHeadFrame.fFrame += m_tHeadFrame.fMaxCount*m_fSpeed*GET_INSTANCE(CTimeMgr)->GetDeltaTime();

	if (m_tHeadFrame.fFrame > m_tHeadFrame.fMaxCount)
	{
		if (m_hCurState == HEADREADY)
		{
			m_bIsBulletShoot = true;
			m_hCurState = HEADSHOOT;
			m_tHeadFrame.fFrame = 0;
		}
		else
			m_tHeadFrame.fFrame = 0;
	}
}

void CMegaHellicopter::HeadStateChange()
{
	if (m_hCurState != m_hPreState)
	{
		switch (m_hCurState)
		{
		case HEADIDLE:
			m_wstrHeadStateKey = L"Idle";
			m_tHeadFrame = { 0.f, 1.f };
			break;

		case HEADDEAD:
			m_wstrHeadStateKey = L"Dead";
			m_tHeadFrame = { 0.f, 1.f };
			break;

		case HEADDEADALL:
			m_wstrHeadStateKey = L"DeadAll";
			m_tHeadFrame = { 0.f, 1.f };
			break;
		case HEADREADY:
			m_wstrHeadStateKey = L"Ready";
			m_tHeadFrame = { 0.f, 20.f };
			break;
		case HEADSHOOT:
			m_wstrHeadStateKey = L"Shoot";
			m_tHeadFrame = { 0.f, 8.f };
			break;
		}
		m_hPreState = m_hCurState;
	}

}
void CMegaHellicopter::BodyStateChange()
{
	if (m_bCurState != m_bPreState)
	{
		switch (m_bCurState)
		{
		case BODYIDLE:
			m_wstrBodyStateKey = L"Idle";
			m_tBodyFrame = { 0.f, 8.f };
			break;

		case BODYDEAD:
			m_wstrBodyStateKey = L"Dead";
			m_tBodyFrame = { 0.f, 1.f };
			break;

		case BODYDEADALL:
			m_wstrBodyStateKey = L"DeadAll";
			m_tBodyFrame = { 0.f, 1.f };
			break;
		case BODYBOMB:
			m_wstrBodyStateKey = L"Bomb";
			m_tBodyFrame = { 0.f, 8.f };
			break;

		case BODYMISSILE:
			m_wstrBodyStateKey = L"Missile";
			m_tBodyFrame = { 0.f, 8.f };
			break;

		}
		m_bPreState = m_bCurState;
	}
}
void CMegaHellicopter::UpdateRect()
{
}
void CMegaHellicopter::ChangeState()
{
	if(!m_bIsDead)
	{
		if (m_bIsDetect)//나를 발견하게되면
		{
			if (!m_bHeadDead)
			{
				int iRand = GetRandomNumber(1, 2);//1,2,3

				if (iRand == 1)
				{

					if (!m_bIsReady)
					{
						m_hCurState = HEADIDLE;
						m_bCurState = BODYIDLE;
						m_bSetPos = true;
						m_fReloadTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();

						if (m_fReloadTime > 3.5f)
						{
							m_hCurState = HEADREADY;
							m_bCurState = BODYIDLE;
							m_fReloadTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
							m_bIsReady = true;
							m_bSetPos = false;
						}
					}

				}
				else if (iRand == 2)
				{
					if (!m_bIsReady)
					{
						m_fReloadTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
						m_hCurState = HEADIDLE;
						m_bCurState = BODYIDLE;
						if (m_fReloadTime > 3.5f)
						{
							m_hCurState = HEADIDLE;
							m_bCurState = BODYMISSILE;
							m_fReloadTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
							m_bIsReady = true;
						}
					}
				}
			}
			else
			{
				int iRand = 1;// GetRandomNumber(0, 1);//1,2,3
				if (iRand == 0)
				{
					if (!m_bIsReady)
					{
						m_fReloadTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
						m_hCurState = HEADDEAD;
						m_bCurState = BODYIDLE;
						if (m_fReloadTime > 2.5f)
						{
							m_hCurState = HEADDEAD;
							m_bCurState = BODYMISSILE;
							m_fReloadTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
							m_bIsReady = true;
						}
					}
				}
				else if (iRand == 1)
				{
					if (!m_bIsReady)
					{
						m_fReloadTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
						m_hCurState = HEADDEAD;
						m_bCurState = BODYIDLE;
						if (m_fReloadTime > 3.5f)
						{
							m_hCurState = HEADDEAD;
							m_bCurState = BODYBOMB;
							m_fReloadTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
							m_bIsReady = true;
						}
					}

				}
			}
		}


		if (m_bIsBulletShoot)
		{
			m_fReadyTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			ShootTime(1);
			if (m_fReadyTime > 1.5f)
			{
				m_bIsReady = false;
				m_fReadyTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
				m_bIsBulletShoot = false;
			}
		}
		else if (m_bIsMissileShoot)
		{
			m_fReadyTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			ShootTime(2);
			if (m_fReadyTime > 1.5f)
			{
				m_bIsReady = false;
				m_fReadyTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
				m_bIsMissileShoot = false;
			}
		}
		else if (m_bIsBombShoot)
		{
			m_fReadyTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			ShootTime(3);
			if (m_fReadyTime > 5.f)
			{
				m_bIsReady = false;
				m_fReadyTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
				m_bIsBombShoot = false;
			}
		}
	}
	if (m_hCurState == HEADIDLE)
	{

		m_tInfo.vDir.y = (m_pPlayer->GetInfo().vPos.y-57.f) - m_tInfo.vPos.y;
		D3DXVec3Normalize(&m_tInfo.vDir, &m_tInfo.vDir);
		m_tInfo.vPos.y += m_tInfo.vDir.y*3.f;
	}

	if (m_bCurState == BODYBOMB)
	{
		//m_tInfo.vPos.x += 5.f * -m_tInfo.vSize.x;
		m_tInfo.vDir = m_pPlayer->GetInfo().vPos - m_tInfo.vPos;
		D3DXVec3Normalize(&m_tInfo.vDir, &m_tInfo.vDir);
		m_tInfo.vPos += m_tInfo.vDir*3.f;
	}
}


void CMegaHellicopter::ShootTime(int iNumber)
{

	if (iNumber == 1)
	{
		m_fShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
		if (m_fShootTime > 0.1f)
		{
			if (m_hCurState == HEADSHOOT)
			{
				ShootBullet();
			}
			m_fShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
		}
	}
	else if (iNumber == 2)
	{
		m_fShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
		if (m_fShootTime > 0.3f)
		{
			if (m_bCurState == BODYMISSILE)
			{
				ShootMissile();
			}
			m_fShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
		}
	}
	else if (iNumber == 3)
	{
		m_fShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
		if (m_fShootTime > 0.3f)
		{
			if (m_bCurState == BODYBOMB)
			{
				ShootBomb();
			}
			m_fShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
		}
	}
}

void CMegaHellicopter::ShootBullet()
{
	
	CSoundMgr::GetInstance()->PlaySoundw(L"BossBulletSound.wav", CSoundMgr::BULLET);
		
	GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>(D3DXVECTOR3(105.f*-m_tInfo.vSize.x, 50.f, 0.f), 0.f), CObjMgr::BULLET);

}
void CMegaHellicopter::ShootMissile()
{
	int iSoundRand = GetRandomNumber(0, 4);
	switch (iSoundRand)
	{
	case 0:
		CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher0.wav", CSoundMgr::BULLET);
		break;
	case 1:
		CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher1.wav", CSoundMgr::BULLET);
		break;
	case 2:
		CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher2.wav", CSoundMgr::BULLET);
		break;
	case 3:
		CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher3.wav", CSoundMgr::BULLET);
		break;
	case 4:
		CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher4.wav", CSoundMgr::BULLET);
		break;

	}
	GET_INSTANCE(CObjMgr)->AddObject(CreateMissile<CBullet>(D3DXVECTOR3(30.f*-m_tInfo.vSize.x + 5, 40.f, 0.f), 0.f), CObjMgr::BULLET);

}

void CMegaHellicopter::ShootBomb()
{
	GET_INSTANCE(CObjMgr)->AddObject(CreateBomb<CBullet>(D3DXVECTOR3(70.f*m_tInfo.vSize.x, 55.f, 0.f), D3DXVECTOR3(0.f, 0.f, 0.f), 0.f), CObjMgr::BULLET);
}
