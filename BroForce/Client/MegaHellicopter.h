#pragma once
#include "Obj.h"
class CPlayer;
class CMegaHellicopter :
	public CObj
{
public:
	enum HEADSTATE { HEADIDLE,HEADREADY , HEADSHOOT, HEADDEAD, HEADDEADALL,};
	enum BODYSTATE { BODYIDLE, BODYDEAD, BODYDEADALL,BODYBOMB, BODYMISSILE};
public:
	CMegaHellicopter();
	virtual ~CMegaHellicopter();

	// CObj을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void UpdateRect() override;
	virtual void LateInit();
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;

public:
	bool GetIsDetect() { return m_bIsDetect; }
	void SetIsDetect(bool tf) { m_bIsDetect = tf; }
	bool GetIsReady() { return m_bIsReady; }
	void SetIsReady(bool tf) { m_bIsReady = tf; }

	bool GetIsShoot() { return m_bIsShoot; }
	void SetIsShoot(bool tf) { m_bIsShoot = tf; }

public:
	void ChangeState();
	void ShootTime(int iNumber);
	void ShootBullet();
	void ShootMissile();
	void ShootBomb();
	void MoveBodyFrame(float fSpeed);
	void MoveHeadFrame(float fSPeed);

	void BodyStateChange();
	void HeadStateChange();
	int GetHp() { return m_iHp; }
	void SetHp(float fDamage) { m_iHp -= (int)fDamage; }
private:
	float m_fBoomTime;
	int m_iHp;

	float m_fPlayerCurPos;
	bool m_bSetPos;
	bool m_bHeadDead;
	bool m_bBodyDead;
	bool m_bPatternOne;
	bool m_bPatternTwo;
	bool m_bPatternThree;
	bool m_bPatternFour;
	bool m_bPatternFive;
	bool m_bPatternSix;

	float m_fReloadTime;
	bool m_bIsDetect;//나를 발견했는 지를 판별
	bool m_bIsReady;// 준비상태로 들어갔을때를판별
	bool m_bIsShoot;//발사를 시작하는 것을 판별


	bool m_bIsMissileShoot;//발사를 시작하는 것을 판별
	bool m_bIsBombShoot;//발사를 시작하는 것을 판별
	bool m_bIsBulletShoot;//발사를 시작하는 것을 판별

	float m_fReadyTime;
	float m_fShootTime;

private:

	template <typename T>
	CObj* CreateBullet(float fAngle)
	{
		return CObjFactory<T>::CreateBullet(m_tInfo.vPos, -m_tInfo.vSize, fAngle , BOSS_BULLET, MOOK_BULLET);
	}

	template <typename T>
	CObj* CreateBullet(D3DXVECTOR3 vPos, float fAngle)
	{
		return CObjFactory<T>::CreateBullet(m_tInfo.vPos + vPos, -m_tInfo.vSize, fAngle, BOSS_BULLET, MOOK_BULLET);
	}

	template <typename T>
	CObj* CreateMissile(D3DXVECTOR3 vPos, float fAngle)
	{
		return CObjFactory<T>::CreateBullet(m_tInfo.vPos + vPos, m_tInfo.vSize, fAngle, TANK_MISSILE, MOOK_BULLET);
	}

	template <typename T>
	CObj* CreateBomb(D3DXVECTOR3 vPos, D3DXVECTOR3 vSize, float fAngle)
	{
		return CObjFactory<T>::CreateBullet(m_tInfo.vPos + vPos, m_tInfo.vSize + vSize, fAngle, BOSS_BOMB, MOOK_BULLET);
	}

private:
	CPlayer* m_pPlayer;
private:
	FRAME m_tHeadFrame;
	FRAME m_tBodyFrame;

	wstring m_wstrHeadStateKey;
	wstring m_wstrBodyStateKey;

	HEADSTATE m_hCurState;
	HEADSTATE m_hPreState;

	BODYSTATE m_bCurState;
	BODYSTATE m_bPreState;

};

