#include "stdafx.h"
#include "Mook.h"
#include"Bullet.h"
#include"AnimEffect.h"
#include"Effect.h"

#include"NonAnimEffect.h"



CMook::CMook() : m_bIsGround(false), m_bIsShoot(false), m_bIsStand(false), m_bIsDetect(false), m_fShootTime(0.f), m_bIsHit(false), m_bIsFaint(false),
m_fFaintTime(0.f), m_fMoveTime(0.f), m_bIsMove(true)
{
}


CMook::~CMook()
{
	Release();
}

HRESULT CMook::Initialize()
{
	m_tObj.vSize = { 1.0f,1.0f,0.0f };
	m_tObj.vDir = { 1.0f,0.0f,0.0f };
	m_tObj.vLook = { 1.0f,0.0f,0.0f };

	m_wstrObjKey = L"Mook";
	m_wstrObjKey2 = L"MookGun";


	m_wstrBodyStateKey = L"Run";
	m_tBodyFrame = { 0.f, 8.f };

	m_wstrGunStateKey = L"GunShoot";
	m_tGunFrame = { 0.f,4.f };

	m_fSpeed = 5.f;
	return S_OK;
}

void CMook::LateInit()
{
	int iRand = (((rand() % 2) * 2) - 1);//1.-1;
	m_tObj.vSize = { (float)iRand,1.0f,0.0f };
}

int CMook::Update()
{
	CObj::LateInit();
	if (m_tObj.vPos.x < 0 || g_StageX < m_tObj.vPos.x || m_tObj.vPos.y < 0 || g_StageY < m_tObj.vPos.y)
	{
		int iRand = GetRandomNumber(60, 120);
		int iRandPow = GetRandomNumber(10, 15);
		CObj* pEffect = CEffectFactory<CNonAnimEffect, CEffect>::CreateEffect(m_tObj.vPos, L"DeadMook", L"DeadHead", (float)iRand, 5.f, EFFECT_MOVING, (float)iRandPow);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
		iRand = GetRandomNumber(60, 120);
		iRandPow = GetRandomNumber(10, 15);

		pEffect = CEffectFactory<CNonAnimEffect, CEffect>::CreateEffect(m_tObj.vPos, L"DeadMook", L"DeadBody", (float)iRand, 5.f, EFFECT_MOVING, (float)iRandPow);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);

		iRand = GetRandomNumber(60, 120);
		iRandPow = GetRandomNumber(10, 15);
		pEffect = CEffectFactory<CNonAnimEffect, CEffect>::CreateEffect(m_tObj.vPos, L"DeadMook", L"DeadLeg", (float)iRand, 5.f, EFFECT_MOVING, (float)iRandPow);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);

		iRand = GetRandomNumber(60, 120);
		iRandPow = GetRandomNumber(10, 15);
		pEffect = CEffectFactory<CNonAnimEffect, CEffect>::CreateEffect(m_tObj.vPos, L"DeadMook", L"DeadArm", (float)iRand, 5.f, EFFECT_MOVING, (float)iRandPow);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
	}
	MoveBodyFrame(3.f);
	MoveGunFrame(3.f);

	if (m_bIsDead)
		return DEAD_OBJ;

	StateSet();
	SceneBodyChange();
	SceneGunChange();
	
	D3DXMATRIX matScale, matTrans;
	D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

	D3DXMatrixScaling(&matScale, m_tObj.vSize.x, m_tObj.vSize.y, m_tObj.vSize.z);
	D3DXMatrixTranslation(&matTrans, m_tObj.vPos.x - vScroll.x,
		m_tObj.vPos.y - vScroll.y, m_tObj.vPos.z);

	m_tObj.matWorld = matScale * matTrans;


	return NO_EVENT;
}

void CMook::LateUpdate()
{
	
	if (m_tObj.vPos.y > g_StageY)//바닥에 빠지면 죽어용
	{
		IsDead();
	}


}

void CMook::Move()
{
	
}

void CMook::Render()
{

	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey,
		m_wstrBodyStateKey, int(m_tBodyFrame.fFrame));
	NULL_CHECK(m_pTexInfo);
	UpdateRect();


	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.8f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tObj.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));

	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey2,
		m_wstrGunStateKey, int(m_tGunFrame.fFrame));
	NULL_CHECK(m_pTexInfo);

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tObj.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY + 1.f, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));

}

void CMook::Release()
{

}


void CMook::UpdateRect()
{
	m_tRect.left = LONG(m_tInfo.vPos.x - (m_pTexInfo->tImgInfo.Width*0.5f) * 0.5f);
	m_tRect.top = LONG(m_tInfo.vPos.y - (m_pTexInfo->tImgInfo.Height*0.5f) * 0.5f);
	m_tRect.right = LONG(m_tInfo.vPos.x + (m_pTexInfo->tImgInfo.Width*0.5f) * 0.5f);
	m_tRect.bottom = LONG(m_tInfo.vPos.y + (m_pTexInfo->tImgInfo.Height*0.5f) * 0.5f);
}


void CMook::MoveBodyFrame(float fSpeed)
{
	m_tBodyFrame.fFrame += m_tBodyFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tBodyFrame.fFrame > m_tBodyFrame.fMaxCount)
	{
		if (!m_bIsHit)
			m_tBodyFrame.fFrame = 0.f;
		else
		{

			IsDead();
		}
	}
}

void CMook::MoveGunFrame(float fSpeed)
{
	m_tGunFrame.fFrame += m_tGunFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tGunFrame.fFrame > m_tGunFrame.fMaxCount)
		m_tGunFrame.fFrame = 0.f;
}

void CMook::StateSet()
{
	m_tObj.vPos.y += m_fSpeed;
	if (m_bIsGround)
	{
		if (m_bIsHit)
		{
			m_bCurState = MOOKDEAD;
			m_gCurState = MOOKGUNEND;

		}
		else if (m_bIsFaint)//기절 당한거  
		{
			m_fFaintTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			m_bCurState = MOOKSTAND;
			m_gCurState = MOOKGUNSTAND;
			if (m_fFaintTime > 3.f)
			{
				m_bIsDetect = false;
				m_bIsFaint = false; 
				m_fFaintTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			}

		}
		else if ( m_bIsDetect && !m_bIsShoot)
		{
			m_bCurState = MOOKSTAND;
			m_gCurState = MOOKGUNREADY;
			m_fShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			if (m_fShootTime>0.2f)
			{
				m_bCurState = MOOKSTAND;
				m_gCurState = MOOKGUNIDLE;

				if (m_fShootTime > 2.f)
				{
					m_bCurState = MOOKSTAND;
					m_gCurState = MOOKGUNSHOOT;
					if (m_fShootTime > 2.1f)
					{
					m_bCurState = MOOKSTAND;
					m_gCurState = MOOKGUNIDLE;
					GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>(0.0f), CObjMgr::BULLET);
						

						m_fShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
					}
				}
			}
		}
		else
		{
			m_fMoveTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			m_bCurState = MOOKSTAND;
			m_gCurState = MOOKGUNIDLE;
			if (m_fMoveTime > 1.f)
			{
				m_tObj.vPos.x += 2.f*m_tObj.vSize.x;
				m_bCurState = MOOKRUN;
				m_gCurState = MOOKGUNWALK;
				if (m_fMoveTime > 2.f)
				{
					int iRand = (((rand() % 2) * 2) - 1);//1.-1;
					m_tObj.vSize = { (float)iRand,1.0f,0.0f };
					m_fMoveTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
				}
			}
		}
	}
}


void CMook::SceneBodyChange()
{
	if (m_bCurState != m_bPreState)
	{
		switch (m_bCurState)
		{

		case MOOKDEAD://7
			m_wstrBodyStateKey = L"Dead";
			m_tBodyFrame = { 0.f, 16.f };
			break;


		case MOOKRUN://10
			m_wstrBodyStateKey = L"Run";
			m_tBodyFrame = { 0.f, 8.f };
			break;

		case MOOKRUNAWAY://1
			m_wstrBodyStateKey = L"RunAway";
			m_tBodyFrame = { 0.f, 8.f };
			break;

		case MOOKSTAND://3
			m_wstrBodyStateKey = L"Stand";
			m_tBodyFrame = { 0.f, 1.f };
			break;
		}
		m_bPreState = m_bCurState;
	}
}

void CMook::SceneGunChange()
{
	if (m_gCurState != m_gPreState)
	{
		switch (m_gCurState)
		{
		case MOOKGUNREADY:
			m_wstrGunStateKey = L"GunReady";
			m_tGunFrame = { 0.f, 7.f };
			break;


		case MOOKGUNSHOOT:
			m_wstrGunStateKey = L"GunShoot";
			m_tGunFrame = { 0.f, 4.f };
			break;

		case MOOKGUNSTAND:
			m_wstrGunStateKey = L"GunStand";
			m_tGunFrame = { 0.f, 1.f };
			break;

		case MOOKGUNWALK:
			m_wstrGunStateKey = L"GunWalk";
			m_tGunFrame = { 0.f, 8.f };
			break;

		case MOOKGUNIDLE:
			m_wstrGunStateKey = L"GunIdle";
			m_tGunFrame = { 0.f, 1.f };
			break;
		case MOOKGUNEND:
			m_wstrGunStateKey = L"GunEnd";
			m_tGunFrame = { 0.f, 1.f };
			break;
		}
		m_gPreState = m_gCurState;
	}
}

