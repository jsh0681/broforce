#pragma once
#include "Obj.h"
class CMook :
	public CObj
{

public:
	enum BODYSTATE { MOOKRUN, MOOKRUNAWAY, MOOKSTAND, MOOKDEAD };
	enum GUNSTATE { MOOKGUNREADY, MOOKGUNSHOOT, MOOKGUNSTAND, MOOKGUNWALK, MOOKGUNIDLE, MOOKGUNEND };

public:
	CMook();
	virtual ~CMook();

	// CObj��(��) ���� ��ӵ�
	virtual HRESULT Initialize() override;
	virtual void LateInit() override;
	virtual void UpdateRect() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
public:
	template <typename T>
	CObj* CreateBullet(float fAngle)
	{
		return CObjFactory<T>::CreateBullet(m_tObj.vPos, m_tObj.vSize, fAngle, 1, MOOK_BULLET);
	}
public:

	void Move();
	void MoveBodyFrame(float fSpeed);
	void MoveGunFrame(float fSpeed);
	void StateSet();
	void SceneBodyChange();
	void SceneGunChange();
public:
	void SceneChange() {};
public:
	void SetIsGround(bool bIsGround) { m_bIsGround = bIsGround; }
	void SetIsShoot(bool bIsShoot) { m_bIsShoot = bIsShoot; }
	void SetIsDetect(bool bIsDetect) { m_bIsDetect = bIsDetect; }
	void SetIsStand(bool bIsStand) { m_bIsStand = bIsStand; }
	void SetIsHit(bool bIsHit) { m_bIsHit = bIsHit; }
	void SetIsFaint(bool bIsFaint) { m_bIsFaint = bIsFaint; }
	bool GetIsDetect() { return m_bIsDetect; }

private:
	float m_fFaintTime;
	bool m_bIsFaint;
	float m_fShootTime;
private:
	bool m_bIsHit;//�Ѹ����� 
	bool m_bIsGround;
	bool m_bIsShoot;
	bool m_bIsStand;
	bool m_bIsDetect;
	float m_fMoveTime;
	bool m_bIsMove;
private:
	
	FRAME	m_tBodyFrame;
	FRAME	m_tGunFrame;

	wstring m_wstrBodyStateKey = L"";
	wstring m_wstrGunStateKey = L"";

	BODYSTATE m_bCurState;
	BODYSTATE m_bPreState;

	GUNSTATE m_gCurState;
	GUNSTATE m_gPreState;
};

