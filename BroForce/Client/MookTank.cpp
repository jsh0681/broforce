#include "stdafx.h"
#include "MookTank.h"
#include"Player.h"
#include"Bullet.h"
#include"Effect.h"
#include"AnimEffect.h"
#include"NonAnimEffect.h"

CMookTank::CMookTank() :m_fShootTime(0.f), m_fTurnTime(0.f), m_bIsTurn(false), m_iHp(300.f), m_bIsFix(false)
{

	m_pPlayer = nullptr;
}


CMookTank::~CMookTank()
{
	Release();
}

HRESULT CMookTank::Initialize()
{
	m_tInfo.vPos = { 340.f,675.f,0.f };
	m_tInfo.vSize = { 1.f , 1.f,0.f };
	m_tInfo.vDir = { 1.f,0.f,0.f };
	m_tInfo.vLook = { 1.f,0.f,0.f };

	ZeroMemory(&m_tFrame, sizeof(FRAME));
	m_wstrObjKey = L"Tank";
	m_wstrStateKey = L"TankRight";
	m_tFrame = { 0.f ,1.f };
	m_fAngle = 30.f;
	m_fSpeed = 3.f;
	return S_OK;
}

void CMookTank::LateInit()
{
	m_pPlayer = dynamic_cast<CPlayer*>(GET_INSTANCE(CObjMgr)->GetPlayer());
}

int CMookTank::Update()
{
	CObj::LateInit();
	if (m_tInfo.vPos.x < 0 || g_StageX < m_tInfo.vPos.x || m_tInfo.vPos.y < 0 || g_StageY < m_tInfo.vPos.y)
	{
		IsDead();
	}
	if (m_bIsDead)
	{
		
		if (m_tCurState != TANKEND)
		{
			CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(0.f, -10.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 1.5f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(50.f, -10.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 1.5f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(0.f, 50.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 1.5f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
			pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3(-30.f, -30.f, 0.f), L"BulletEffect",
				L"BrommandoBulletPuff", { 0.f,30.f }, 1.5f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);

			int iRand = GetRandomNumber(1, 2);
			switch (iRand)
			{
			case 1:
				CSoundMgr::GetInstance()->PlaySoundw(L"Explosion1.wav", CSoundMgr::EFFECT);
				break;
			case 2:
				CSoundMgr::GetInstance()->PlaySoundw(L"Explosion2.wav", CSoundMgr::EFFECT);
				break;
			}
		}
		m_tCurState = TANKEND;
	}
	m_tInfo.vPos.y += m_fSpeed;
	if(m_bIsDetect)
		ShootTime();
	StateChange();
	
	D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
	D3DXMATRIX matScale, matTrans;
	if (!m_bIsDead)
		D3DXMatrixScaling(&matScale, m_tInfo.vSize.x, m_tInfo.vSize.y, m_tInfo.vSize.z);
	D3DXMatrixTranslation(&matTrans, m_tInfo.vPos.x - vScroll.x, m_tInfo.vPos.y - vScroll.y, m_tInfo.vPos.z);

	m_tInfo.matWorld = matScale * matTrans;
	
	return NO_EVENT;
}

void CMookTank::LateUpdate()
{
	MoveFrame(3.f);
}

void CMookTank::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey,
		m_wstrStateKey, int(m_tFrame.fFrame));
	NULL_CHECK(m_pTexInfo);

	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.5f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));

}

void CMookTank::UpdateRect()
{
}

void CMookTank::Release()
{
}


void CMookTank::MoveFrame(float fSpeed)
{
	m_tFrame.fFrame += m_tFrame.fMaxCount*m_fSpeed*GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	
	if (m_tFrame.fFrame > m_tFrame.fMaxCount)
			m_tFrame.fFrame = 0;
}

void CMookTank::StateChange()
{
	if (m_tCurState != m_tPreState)
	{
		switch (m_tCurState)
		{
		case TANKLEFT:
			m_wstrStateKey = L"TankLeft";
			m_tFrame = { 0.f ,1.f };
			break;
		case TANKTURNLEFT:
			m_wstrStateKey = L"TankTurnToLeft";
			m_tFrame = { 0.f ,8.f };
			break;
		case TANKTURNRIGHT:
			m_wstrStateKey = L"TankTurnRight";
			m_tFrame = { 0.f ,8.f };
			break;
		case TANKRIGHT:
			m_wstrStateKey = L"TankRight";
			m_tFrame = { 0.f ,1.f };
			break;
		case TANKEND:
			m_wstrStateKey = L"TankDead";
			m_tFrame = { 0.f ,1.f };
			break;
		}
		m_tPreState = m_tCurState;
	}
}


void CMookTank::ShootTime()
{
	m_fShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_fShootTime > 1.f)
	{
		if (m_tCurState == 0)
		{
			int iSoundRand = GetRandomNumber(0, 4);
			switch (iSoundRand)
			{
			case 0:
				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher0.wav", CSoundMgr::BULLET);
				break;
			case 1:
				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher1.wav", CSoundMgr::BULLET);
				break;
			case 2:
				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher2.wav", CSoundMgr::BULLET);
				break;
			case 3:
				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher3.wav", CSoundMgr::BULLET);
				break;
			case 4:
				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher4.wav", CSoundMgr::BULLET);
				break;
			}
			ShootMissile();
		}
		m_fShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	}
}

void CMookTank::ShootMissile()
{
	GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>(D3DXVECTOR3(0.f,0.f,0.f),0.f), CObjMgr::BULLET);
}
