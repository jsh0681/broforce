#pragma once
#include "Obj.h"
class CPlayer;

class CMookTank :
	public CObj
{
public:
	enum TANKSTATE { TANKLEFT, TANKTURNLEFT, TANKTURNRIGHT, TANKRIGHT,TANKEND};
	
public:
	CMookTank();
	virtual ~CMookTank();
public:
	// CObj을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void LateInit();
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void UpdateRect() override;
	virtual void Release() override;
public:
	bool GetIsDetect() { return m_bIsDetect; }
	void SetIsDetect(bool tf) { m_bIsDetect = tf; }
	void ShootTime();
	void MoveFrame(float fSpeed);
	void StateChange();
	void ShootMissile();
	void SetHp(float fDamage) { m_iHp -= (int)fDamage; }
	int GetHp() { return m_iHp; }
private:
	bool m_iShootCount;
	bool m_bIsDetect;
	bool m_bIsTurn;
	bool m_bIsFix;
	float m_fTurnTime;
	int m_iHp;
	float m_fShootTime;
private:
	template <typename T>
	CObj* CreateBullet(D3DXVECTOR3 vPos, float fAngle)
	{
		return CObjFactory<T>::CreateBullet(m_tInfo.vPos + vPos, m_tInfo.vSize, fAngle, TANK_MISSILE, MOOK_BULLET);
	}

private:
	CPlayer* m_pPlayer;
	wstring m_wstrStateKey;
	TANKSTATE m_tCurState;
	TANKSTATE m_tPreState;
	
	FRAME m_tFrame;
};

