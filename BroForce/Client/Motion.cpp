#include "stdafx.h"
#include "Motion.h"


CMotion::CMotion()
{
}


CMotion::~CMotion()
{
}

void CMotion::MoveFrame(float fSpeed)
{
	m_tFrame.fFrame += m_tFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tFrame.fFrame > m_tFrame.fMaxCount)
	{
		m_tFrame.fFrame = 0.f;
	}
}

