#pragma once
class CMotion
{
public:
	CMotion();
	virtual ~CMotion();
public:
	 FRAME2& GetFrame() { return m_tFrame; }
	void SetFrame(FRAME2 tFrame) { m_tFrame = tFrame; }
	virtual void MoveFrame(float fSpeed) PURE;
protected:
	FRAME2 m_tFrame;

};

