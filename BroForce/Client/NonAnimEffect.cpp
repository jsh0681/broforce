#include "stdafx.h"
#include "NonAnimEffect.h"
#include "Effect.h"


CNonAnimEffect::CNonAnimEffect()
	: m_fLifeTime(0.f), m_fCurTime(0.f)
{
}


CNonAnimEffect::~CNonAnimEffect()
{
	Release();
}

HRESULT CNonAnimEffect::Initialize()
{
	return S_OK;
}

void CNonAnimEffect::LateInit()
{
}

int CNonAnimEffect::Update()
{
	CEffectImp::LateInit();

	if (m_fCurTime > m_fLifeTime)
		return DEAD_OBJ;

	m_fCurTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();




	return NO_EVENT;
}

void CNonAnimEffect::LateUpdate()
{
}

void CNonAnimEffect::Render()
{
	const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
		m_wstrObjKey, m_wstrStateKey, 0);
	NULL_CHECK(pTexInfo);

	float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_pInterface->GetInfo().matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CNonAnimEffect::Release()
{
}
