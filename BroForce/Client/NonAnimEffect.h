#pragma once
#include "EffectImp.h"
class CNonAnimEffect :
	public CEffectImp
{
public:
	CNonAnimEffect();
	virtual ~CNonAnimEffect();

public:
	void SetLifeTime(float fLifeTime) { m_fLifeTime = fLifeTime; }

public:
	// CEffectImp을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;

private:
	float m_fLifeTime;	// 생존시간
	float m_fCurTime;
};

