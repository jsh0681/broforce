#include "stdafx.h"
#include "Obj.h"


CObj::CObj()
	: m_bIsInit(false), m_bIsDead(false), m_fSpeed(0.f), m_fAngle(0.f)
{
	ZeroMemory(&m_tInfo, sizeof(INFO));
	ZeroMemory(&m_tRect, sizeof(RECT));
	ZeroMemory(&m_tObj, sizeof(OBJ));


	m_tInfo.vLook = { 1.f, 0.f, 0.f };

}


CObj::~CObj()
{
}

void CObj::LateInit()
{
	if (!m_bIsInit)
	{
		this->LateInit();
		m_bIsInit = true;
	}
}
