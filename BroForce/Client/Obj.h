#pragma once
class CObj
{
public:
	CObj();
	virtual ~CObj();

public:
	const INFO& GetInfo() const { return m_tInfo; }
	 OBJ& GetObj()  { return m_tObj; }

	const RECT& GetRect() const { return m_tRect; }
	 wstring& GetObjKey()  { return m_wstrObjKey; }
	void IsDead() { m_bIsDead = true; }
	bool GetIsDead() { return m_bIsDead; }

public:
	void SetDir(const D3DXVECTOR3& vDir) { m_tInfo.vLook.x *= vDir.x; }
	void SetPos(const D3DXVECTOR3& vPos) { m_tInfo.vPos = vPos; }
	void SetSize(const D3DXVECTOR3& vSize) { m_tInfo.vSize = vSize; }
	void SetObjSize(const D3DXVECTOR3& vSize) { m_tObj.vSize = vSize; }
	void SetAngle(float angle) { m_fAngle += angle; }
	void SetPosX(float fPosX) { m_tInfo.vPos.x = fPosX; }


	void SetPosition(float x, float y) { m_tInfo.vPos.x = x, m_tInfo.vPos.y = y; }
	void SetPosition2(float x, float y) { m_tObj.vPos.x = x, m_tObj.vPos.y = y; }

public:
	virtual HRESULT Initialize() PURE;
	virtual void LateInit();
	virtual void  UpdateRect() PURE;
	virtual int Update() PURE;
	virtual void LateUpdate() PURE;
	virtual void Render() PURE;
	virtual void Release() PURE;
	CObjMgr::OBJ_LAYER GetLayer() { return m_eLayer; }

protected:
	CObjMgr::OBJ_LAYER	m_eLayer;

	float   m_fSpeed;
	float   m_fAngle;
	INFO	m_tInfo; 
	OBJ m_tObj;
	RECT	m_tRect;
	const TEXINFO* m_pTexInfo;
	bool	m_bIsInit;
	bool	m_bIsDead;
	wstring m_wstrObjKey = L"";

	wstring m_wstrObjKey2 = L"";
};

