#include "stdafx.h"
#include "ObjMgr.h"
#include "Obj.h"
#include"Terrain.h"
#include"Flag.h"
#include"Explosive.h"

IMPLEMENT_SINGLETON(CObjMgr)

CObjMgr::CObjMgr()
{
}


CObjMgr::~CObjMgr()
{
	Release();
}

void CObjMgr::AddObject(CObj * pObj, OBJ_ID eID)
{
	m_ObjLst[eID].push_back(pObj);
}

void CObjMgr::Update()
{
	OBJITER iter = {};

	for (int i = 0; i < END; ++i)
	{
		iter = m_ObjLst[i].begin();

		for (; iter != m_ObjLst[i].end();)
		{
			int iEvent = (*iter)->Update();

			if (DEAD_OBJ == iEvent)
			{
				SafeDelete(*iter);
				iter = m_ObjLst[i].erase(iter);
			}
			else
				++iter;
		}
	}
}

void CObjMgr::LateUpdate()
{
	for (int i = 0; i < END; ++i)
	{
		for (auto& pObject : m_ObjLst[i])
			pObject->LateUpdate();
	}


	if (GET_INSTANCE(CSceneMgr)->GetCurScene() == CSceneMgr::SCENE_ID::STAGEONE || GET_INSTANCE(CSceneMgr)->GetCurScene() == CSceneMgr::SCENE_ID::STAGETWO
		|| GET_INSTANCE(CSceneMgr)->GetCurScene() == CSceneMgr::SCENE_ID::STAGEBOSS)
	{
		m_pTerrain = dynamic_cast<CTerrain*>(GET_INSTANCE(CObjMgr)->GetTerrain());
		vector<TILE*>& vecTile = dynamic_cast<CTerrain*>(m_pTerrain)->GetVecTile();


		CCollisionMgr::TileToBro(vecTile, m_ObjLst[PLAYER]);

		CCollisionMgr::MookDetectBro(m_ObjLst[MOOK], m_ObjLst[PLAYER]);
		CCollisionMgr::SuicideMookDetectBro(m_ObjLst[SUICIDEMOOK], m_ObjLst[PLAYER]);

		CCollisionMgr::TileToMook(vecTile, m_ObjLst[MOOK]);
		CCollisionMgr::TileToSuicideMook(vecTile, m_ObjLst[SUICIDEMOOK]);
		CCollisionMgr::TileToMookTank(vecTile, m_ObjLst[MOOKTANK]);

		CCollisionMgr::TileToBox(vecTile, m_ObjLst[EXPLOSIVE]);
		CCollisionMgr::TileToBroBullet(vecTile, m_ObjLst[BULLET]);
		CCollisionMgr::TileToGrenade(vecTile, m_ObjLst[GRENADE]);
		CCollisionMgr::TileToHostage(vecTile, m_ObjLst[HOSTAGE]);
		
		

		CCollisionMgr::CollisionPlayerToFlag(m_ObjLst[FLAG], m_ObjLst[PLAYER]);
		CCollisionMgr::MookDetectBro(m_ObjLst[MOOK], m_ObjLst[PLAYER]);
		CCollisionMgr::HostageDetectBro(m_ObjLst[HOSTAGE], m_ObjLst[PLAYER]);

		CCollisionMgr::BroBulletToMook(m_ObjLst[BULLET], m_ObjLst[MOOK]);
		CCollisionMgr::BroBulletToSuicideMook(m_ObjLst[BULLET], m_ObjLst[SUICIDEMOOK]);

		CCollisionMgr::MookBulletToBro(m_ObjLst[BULLET], m_ObjLst[PLAYER]);
		CCollisionMgr::CollisionBulletToExplosive(m_ObjLst[BULLET], m_ObjLst[EXPLOSIVE], vecTile);

		CCollisionMgr::BroGrenadeToMook(m_ObjLst[GRENADE], m_ObjLst[MOOK]);
		CCollisionMgr::BroGrenadeToSuicideMook(m_ObjLst[GRENADE], m_ObjLst[SUICIDEMOOK]);

		CCollisionMgr::BoomEffectToTile(m_ObjLst[EFFECT], vecTile);
		CCollisionMgr::BoomEffectToMook(m_ObjLst[EFFECT], m_ObjLst[MOOK]);
		CCollisionMgr::BoomEffectToGas(m_ObjLst[EFFECT], m_ObjLst[EXPLOSIVE]);
		CCollisionMgr::BoomEffectToSuicideMook(m_ObjLst[EFFECT], m_ObjLst[SUICIDEMOOK]);

		CCollisionMgr::BroToHostage(m_ObjLst[PLAYER], m_ObjLst[HOSTAGE]);
		CCollisionMgr::BroToHellicopter(m_ObjLst[PLAYER], m_ObjLst[HELLICOPTER]);
		
		if (GET_INSTANCE(CSceneMgr)->GetCurScene() == CSceneMgr::SCENE_ID::STAGETWO)
		{
			CCollisionMgr::MookTankDetectBro(m_ObjLst[MOOKTANK], m_ObjLst[PLAYER]);
			CCollisionMgr::BroBulletToMookTank(m_ObjLst[MOOKTANK], m_ObjLst[BULLET]);
			CCollisionMgr::BroBulletToMookTankBullet(m_ObjLst[BULLET], m_ObjLst[BULLET]);
			CCollisionMgr::BroGrenadeToMookTank(m_ObjLst[GRENADE], m_ObjLst[MOOKTANK]);
		}
		if (GET_INSTANCE(CSceneMgr)->GetCurScene() == CSceneMgr::SCENE_ID::STAGEBOSS)
		{
			CCollisionMgr::BroGrenadeToMegaHellicopter(m_ObjLst[GRENADE], m_ObjLst[MEGAHELLICOPTER]);

			CCollisionMgr::TileToMegaHellicopter(vecTile, m_ObjLst[MEGAHELLICOPTER]);
			CCollisionMgr::MegaHellicopterDetectBro(m_ObjLst[MEGAHELLICOPTER], m_ObjLst[PLAYER]);
			CCollisionMgr::BroBulletToMegaHellicopter(m_ObjLst[MEGAHELLICOPTER], m_ObjLst[BULLET]);
		}

	}

	
}

void CObjMgr::Render()
{
	for (int i = 0; i < END; ++i)
	{
		for (auto& pObject : m_ObjLst[i])
			pObject->Render();
	}
}

void CObjMgr::Release()
{
	for (int i = 0; i < END; ++i)
	{
		for_each(m_ObjLst[i].begin(), m_ObjLst[i].end(), SafeDelete<CObj*>);
		m_ObjLst[i].clear();
	}
}

void CObjMgr::GroupRelease(OBJ_ID eID)
{
	for_each(m_ObjLst[eID].begin(), m_ObjLst[eID].end(), SafeDelete<CObj*>);
	m_ObjLst[eID].clear();
}
