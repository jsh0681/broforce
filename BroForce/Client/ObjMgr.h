#pragma once

class CObj;
class CObjMgr
{
	DECLARE_SINGLETON(CObjMgr)

public:
	enum OBJ_ID { TERRAIN, FLAG ,EXPLOSIVE, HELLICOPTER ,  HOSTAGE, MOOK, SUICIDEMOOK, MOOKTANK, MEGAHELLICOPTER, EFFECT,  PLAYER, BULLET, GRENADE, UI, END };
	
	enum OBJ_LAYER { LAYER_TERRAIN, LAYER_FLAG, LAYER_EXPLOSIVE, LAYER_HELLICOPTER, LAYER_PLAYER, 
		LAYER_BULLET, LAYER_GRENADE, LAYER_HOSTAGE, LAYER_UI, LAYER_EFFECT , LAYER_MOOK , LAYER_SUICIDEMOOK, LAYER_END};

private:
	CObjMgr();
	~CObjMgr();
public:
	CObj* GetPlayer() { return m_ObjLst[PLAYER].front(); }
	CObj* GetTerrain() { return m_ObjLst[TERRAIN].front(); }
	CObj* GetObstacle() {return m_ObjLst[FLAG].front();}
	OBJLIST* GetObjLst() { return m_ObjLst; }
public:
	void AddObject(CObj* pObj, OBJ_ID eID);
	void Update();
	void LateUpdate();
	void Render();
	void Release();
	void GroupRelease(OBJ_ID eID);

private:
	CObj* m_pTerrain;
	CObj* m_pObstacle;
	OBJLIST m_ObjLst[END];
	OBJLIST	m_RenderLst[LAYER_END];
};

