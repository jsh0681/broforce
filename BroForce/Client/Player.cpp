#include "stdafx.h"
#include "Player.h"
#include "Mouse.h"
#include "Bullet.h"
#include "Grenade.h"
#include"Effect.h"
#include "AnimEffect.h"
#include "NonAnimEffect.h"
#include "UIFace.h"
#include "UILive.h"
#include "UIBomb.h"

CPlayer::CPlayer() :m_bIsJump(false), m_bIsFall(false), m_fJumpPow(17.f), m_fJumpAcc(0.3f), m_bDownLeft(false), m_bDownRight(false), m_bIsAttack(false), m_bIsGround(false)
, m_bIsHang(false), iMotionCount(0), m_bIsRadder(false), m_iLifeCount(3), m_vRebirthPoint{ 0.f,0.f,0.f }, fShootSpeed(10.f), m_bBroHardShootHand(SHOOT_HAND_LEFT), m_fGunFrameSpeed(0.f),
m_bShootTime(0.f), m_fBodyFrameSpeed(0.f), m_bRebirth(false), m_bNoDamage(false), m_dHitAlpha(0), m_iTickCount(0), m_bPause(false), m_fYeahTime(0.f)
, m_bIsShift(true), m_fWalkTime(0.f)
{
	ZeroMemory(&m_tBodyFrame, sizeof(FRAME));
	ZeroMemory(&m_tGunFrame, sizeof(FRAME));
}

CPlayer::~CPlayer()
{
	Release();
}

HRESULT CPlayer::Initialize()
{
	srand(unsigned(time(nullptr)));

	m_tInfo.vPos = { 0.f, 0.f, 0.f };
	m_vRebirthPoint = {200.f,500.f,0.f};

	m_tInfo.vSize = { SCALEX, SCALEY, 0.f };
	m_fSpeed = 5.f;

	m_fBodyFrameSpeed = 4.f;
	m_fGunFrameSpeed = 6.f;

	m_pCurState = RAMBRO;
	m_pPreState = m_pCurState;

	m_bCurState = STAND;
	m_bPreState = m_bCurState;


	m_gCurState = GUNSTAND;
	m_gPreState = m_gCurState;

	m_wstrObjKey = L"RamBro";
	m_wstrBodyStateKey = L"Stand";
	m_tBodyFrame = { 0.f, 1.f };

	m_wstrObjKey2 = L"RamBroGun";
	m_wstrGunStateKey = L"GunStand";
	m_tGunFrame = { 0.f, 1.f };

	m_tData = { 0,0,3,3 };

	CObj* pObj = CObjFactory<CUIFace>::CreateObj();
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::UI);
	
	pObj = CObjFactory<CUILive>::CreateObj();
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::UI);

	pObj = CObjFactory<CUIBomb>::CreateObj();
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::UI);

	GET_INSTANCE(CDataSubject)->AddData(CDataSubject::PLAYER_DATA, &m_tData);

	return S_OK;
}

void CPlayer::LateInit()
{
}

int CPlayer::Update()
{
	m_iTickCount++;
	CObj::LateInit();
	if (m_tInfo.vPos.x < 0 || g_StageX < m_tInfo.vPos.x || m_tInfo.vPos.y < 0 || g_StageY < m_tInfo.vPos.y)
	{
		IsDead();
	}

	if (m_bIsDead)
		return DEAD_OBJ;

	if (!g_bPlayerKeyLock)
	{
		if(!g_bDestroy)
			KeyInput();
		KeyShoot();
	}
	BroChange();
	IsJumping();
	SceneBodyChange();
	SceneGunChange();
	D3DXMATRIX matScale, matTrans;
	D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
	D3DXMatrixScaling(&matScale, m_tInfo.vSize.x, m_tInfo.vSize.y, m_tInfo.vSize.z);
	D3DXMatrixTranslation(&matTrans, m_tInfo.vPos.x - vScroll.x,
		m_tInfo.vPos.y - vScroll.y, m_tInfo.vPos.z);

	m_tInfo.matWorld = matScale * matTrans;

	return NO_EVENT;
}
void CPlayer::LifeDeduct()
{
	float fDeltaTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	m_tData.iLife--; 
	m_bShootTime = 0.f;
	D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
	CScrollMgr::SettingScroll(m_vRebirthPoint-vScroll);
	m_tInfo.vPos = m_vRebirthPoint;
	m_tInfo.vSize = { 1.0f,1.0f,0.f };
	m_bShootTime = 0.f;
	int iRand = rand() % 5;
	m_pCurState = (PLAYERNUMBER)iRand;
	switch (iRand)
	{
	case RAMBRO:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"RambroChange", CSoundMgr::UI);
		break;
	case BROHARD:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"BroHardChange", CSoundMgr::UI);
		break;
	case CHUCKBRO:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"ChuckBroChange", CSoundMgr::UI);
		break;
	case BROMMANDO:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"BrommandoChange", CSoundMgr::UI);
		break;
	case BRODREDD:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"BrommandoChange", CSoundMgr::UI);
		break;
	}
	m_fBodyFrameSpeed = 4.f;
	m_fGunFrameSpeed = 6.f;
	m_tData.iBombCount = 3;
	m_bRebirth = true;
	m_bNoDamage = true;
}

void CPlayer::LifeUp()
{
	m_tData.iLife++;
	int iRand = rand() % 5;
	m_pCurState = (PLAYERNUMBER)iRand;
	switch (iRand)
	{
	case RAMBRO:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"RambroChange", CSoundMgr::UI);
		break;
	case BROHARD:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"BroHardChange", CSoundMgr::UI);
		break;
	case CHUCKBRO:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"ChuckBroChange", CSoundMgr::UI);
		break;
	case BROMMANDO:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"BrommandoChange", CSoundMgr::UI);
		break;
	case BRODREDD:
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"BrommandoChange", CSoundMgr::UI);
		break;

	}
	m_tData.iBombCount = 3;

	m_bNoDamage = true;
}

void CPlayer::LateUpdate()
{
	
	if (m_tInfo.vPos.y > g_StageY)//바닥에 빠지면 죽어용 ㅎ
	{
		LifeDeduct();
	}
	if (m_bNoDamage)
	{
		m_bShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
		if (m_iTickCount % 2 == 0)
		{
			m_dHitAlpha = 0;
		}
		else
		{
			m_dHitAlpha = 200;
		}

		if (m_bShootTime > 3.f)
		{
			m_bNoDamage = false; 
			m_dHitAlpha = 0;
		}
	}


	if (m_bRebirth)
	{

		CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3{ 0.f, -10.f, 0.f }, L"Mark",
			L"PlayerMark", { 0.f, 17.f }, 1.f, EFFECT_NONCOLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
		m_bRebirth = false;
	}
	if (m_tData.iLife <= 0)
		m_tData.iLife = 3;
		//IsDead();

	MoveBodyFrame(m_fBodyFrameSpeed);
	MoveGunFrame(m_fGunFrameSpeed);
}

void CPlayer::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey,
		m_wstrBodyStateKey, int(m_tBodyFrame.fFrame));
	NULL_CHECK(m_pTexInfo);
	UpdateRect();


	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.8f;
	//GET_INSTANCE(CDeviceMgr)->GetDevice()->SetRenderState(D3DRS_LIGHTING, true);

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255- m_dHitAlpha, 255, 255, 255));

	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey2,
		m_wstrGunStateKey, int(m_tGunFrame.fFrame));
	NULL_CHECK(m_pTexInfo);

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY + 1.f, 0.f), nullptr, D3DCOLOR_ARGB(255- m_dHitAlpha, 255, 255, 255));

}

void CPlayer::Release()
{
}


void CPlayer::UpdateRect()
{
	m_tRect.left = LONG(m_tInfo.vPos.x - (m_pTexInfo->tImgInfo.Width*0.5f) * 0.5f);
	m_tRect.top = LONG(m_tInfo.vPos.y - (m_pTexInfo->tImgInfo.Height*0.5f) * 0.5f);
	m_tRect.right = LONG(m_tInfo.vPos.x + (m_pTexInfo->tImgInfo.Width*0.5f) * 0.5f);
	m_tRect.bottom = LONG(m_tInfo.vPos.y + (m_pTexInfo->tImgInfo.Height*0.5f) * 0.5f);
}



void CPlayer::MoveBodyFrame(float fSpeed)
{

	m_tBodyFrame.fFrame += m_tBodyFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tBodyFrame.fFrame > m_tBodyFrame.fMaxCount)
	{
		m_tBodyFrame.fFrame = 0.f;
	}

}


void CPlayer::MoveGunFrame(float fSpeed)
{
	m_tGunFrame.fFrame += m_tGunFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();

	if (m_tGunFrame.fFrame > m_tGunFrame.fMaxCount)
		m_tGunFrame.fFrame = 0.f;
}

void CPlayer::KeyShoot()
{//////////////////////////////////////총쏘기//////////////////////////////////////
	if (GET_INSTANCE(CKeyMgr)->KeyUp(KEY_X))
	{
		m_bIsShift = false;
	}
	if (m_bIsShift == true)//화살이 플레이어 몸에 붙어있을 때 
	{
		if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_X))
		{
			fShootSpeed += 1 * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			cout << fShootSpeed << endl;
		}
		else if (GET_INSTANCE(CKeyMgr)->KeyUp(KEY_X))
		{
			m_bIsShift = false;
		}
	}
	if (m_bIsShift == false)
	{
		if (fShootSpeed > 10.f)
		{
			fShootSpeed -= 2 * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			cout << fShootSpeed << endl;
		}
		else if (fShootSpeed <= 10.f)
		{
			if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_X))
			{
				cout << "화살 땡겨옵니다."<< endl;
			}
		}
	}
	//if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_Z))
	//{
	//	if (m_pCurState == RAMBRO)
	//	{
	//		m_tData.iFaceNumber = 1;
	//		GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
	//		m_bShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		if (m_bShootTime > 0.1f)
	//		{
	//			int iRand = (rand() % 3) - 1;

	//			CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3{ 20.f*m_tInfo.vSize.x, -2.f, 0.f }, L"ShootEffect",
	//				L"ShootEffect", { 0.f, 3.f }, 3.f, EFFECT_NONCOLLISION);
	//			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);

	//			int iSoundRand = GetRandomNumber(0, 2);
	//			switch (iSoundRand)
	//			{
	//			case 0:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"GunPistol3.wav", CSoundMgr::BULLET);
	//				break;
	//			case 1:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"GunPistol4.wav", CSoundMgr::BULLET);
	//				break;
	//			case 2:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"GunPistol5.wav", CSoundMgr::BULLET);
	//				break;
	//			}
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>((float)iRand), CObjMgr::BULLET);

	//			m_tData.iFaceNumber = 2;
	//			GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
	//			m_bShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		}
	//	}
	//	else if (m_pCurState == BROHARD)
	//	{
	//		m_tData.iFaceNumber = 1;
	//		GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
	//		m_bShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		if (m_bShootTime > 0.1f)
	//		{

	//			m_tData.iFaceNumber = 2;
	//			GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
	//			if (m_bBroHardShootHand == SHOOT_HAND_LEFT)
	//			{
	//				int iRand = (rand() % 3) - 1;
	//				GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>({ 5.f*m_tInfo.vSize.x,0.f,0.f }, (float)iRand), CObjMgr::BULLET);
	//				int iSoundRand = GetRandomNumber(0, 2);
	//				switch (iSoundRand)
	//				{
	//				case 0:
	//					CSoundMgr::GetInstance()->PlaySoundw(L"GunPistol0.wav", CSoundMgr::BULLET);
	//					break;
	//				case 1:
	//					CSoundMgr::GetInstance()->PlaySoundw(L"GunPistol1.wav", CSoundMgr::BULLET);
	//					break;
	//				case 2:
	//					CSoundMgr::GetInstance()->PlaySoundw(L"GunPistol2.wav", CSoundMgr::BULLET);
	//					break;
	//				}
	//				m_bBroHardShootHand = SHOOT_HAND_RIGHT;

	//				CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3{ 5.f*m_tInfo.vSize.x, 0.f, 0.f }, L"ShootEffect",
	//					L"ShootEffect", { 0.f, 3.f }, 7.f, EFFECT_NONCOLLISION);

	//				GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
	//			}
	//			else if (m_bBroHardShootHand == SHOOT_HAND_RIGHT)
	//			{
	//				int iRand = (rand() % 3) - 1;
	//				GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>({ 28.f*m_tInfo.vSize.x,0.f,0.f }, (float)iRand), CObjMgr::BULLET);
	//				int iSoundRand = GetRandomNumber(0, 2);
	//				switch (iSoundRand)
	//				{
	//				case 0:
	//					CSoundMgr::GetInstance()->PlaySoundw(L"GunPistol0.wav", CSoundMgr::BULLET);
	//					break;
	//				case 1:
	//					CSoundMgr::GetInstance()->PlaySoundw(L"GunPistol1.wav", CSoundMgr::BULLET);
	//					break;
	//				case 2:
	//					CSoundMgr::GetInstance()->PlaySoundw(L"GunPistol2.wav", CSoundMgr::BULLET);
	//					break;
	//				}
	//				m_bBroHardShootHand = SHOOT_HAND_LEFT;
	//				CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3{ 29.f*m_tInfo.vSize.x, -1.f, 0.f }, L"ShootEffect",
	//					L"ShootEffect", { 0.f, 3.f }, 7.f, EFFECT_NONCOLLISION);

	//				GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
	//			}
	//			m_bShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		}
	//	}
	//	else if (m_pCurState == CHUCKBRO)
	//	{
	//		m_tData.iFaceNumber = 1;
	//		GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
	//		m_bShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		if (m_bShootTime > 0.3f)
	//		{
	//			CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3{ 20.f*m_tInfo.vSize.x, -1.f, 0.f }, L"ShootEffect",
	//				L"ShootEffect", { 0.f, 3.f }, 5.f, EFFECT_NONCOLLISION);
	//			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);

	//			m_tData.iFaceNumber = 2;
	//			GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData); 
	//			int iSoundRand = GetRandomNumber(0, 4);
	//			switch (iSoundRand)
	//			{
	//			case 0:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"ShotgunPistol0.wav", CSoundMgr::BULLET);
	//				break;
	//			case 1:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"ShotgunPistol1.wav", CSoundMgr::BULLET);
	//				break;
	//			case 2:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"ShotgunPistol2.wav", CSoundMgr::BULLET);
	//				break;
	//			case 3:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"ShotgunPistol3.wav", CSoundMgr::BULLET);
	//				break;
	//			case 4:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"ShotgunPistol4.wav", CSoundMgr::BULLET);
	//				break;
	//			}
	//			int iRand = (rand() % 6) - 3;
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>(13.f + iRand), CObjMgr::BULLET);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>(4.f + iRand), CObjMgr::BULLET);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>(0.f + iRand), CObjMgr::BULLET);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>(-4.f + iRand), CObjMgr::BULLET);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>(-13.f + iRand), CObjMgr::BULLET);
	//			m_bShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		}
	//	}
	//	else if (m_pCurState == BROMMANDO)
	//	{
	//		m_tData.iFaceNumber = 1;
	//		GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
	//		int iRand = (rand() % 3) - 1;
	//		m_bShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		if (m_bShootTime > 0.3f)
	//		{
	//			m_tData.iFaceNumber = 2;
	//			GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);		
	//			int iSoundRand = GetRandomNumber(0, 4);
	//			switch (iSoundRand)
	//			{
	//			case 0:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher0.wav", CSoundMgr::BULLET);
	//				break;
	//			case 1:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher1.wav", CSoundMgr::BULLET);
	//				break;
	//			case 2:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher2.wav", CSoundMgr::BULLET);
	//				break;
	//			case 3:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher3.wav", CSoundMgr::BULLET);
	//				break;
	//			case 4:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"RocketLauncher4.wav", CSoundMgr::BULLET);
	//				break;

	//			}
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>(D3DXVECTOR3{ 23.f*m_tInfo.vSize.x, -3.f, 0.f }, (float)iRand), CObjMgr::BULLET);
	//			m_bShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		}
	//	}
	//	else if (m_pCurState == BRODREDD)
	//	{
	//		m_tData.iFaceNumber = 1;
	//		GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
	//		m_bShootTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		if (m_bShootTime > 0.3f)
	//		{
	//			int iRand = (rand() % 3) - 1;

	//			CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3{ 20.f*m_tInfo.vSize.x, -2.f, 0.f }, L"ShootEffect",
	//				L"ShootEffect", { 0.f, 3.f }, 3.f, EFFECT_NONCOLLISION);
	//			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);

	//			int iSoundRand = GetRandomNumber(0, 2);
	//			switch (iSoundRand)
	//			{
	//			case 0:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"BroDreddGun0.wav", CSoundMgr::BULLET);
	//				break;
	//			case 1:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"BroDreddGun1.wav", CSoundMgr::BULLET);
	//				break;
	//			case 2:
	//				CSoundMgr::GetInstance()->PlaySoundw(L"BroDreddGun2.wav", CSoundMgr::BULLET);
	//				break;
	//			}
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateBullet<CBullet>((float)iRand,7), CObjMgr::BULLET);

	//			m_tData.iFaceNumber = 2;
	//			GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
	//			m_bShootTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		}
	//	}

	//	if (!m_bIsHang)
	//	{
	//		m_gCurState = GUNSHOOT;
	//	}
	//}
	//else
	//{
	//	m_tData.iFaceNumber = 0;
	//	GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);

	//}


	///////////////////////////////////////////수류탄//////////////////////////////////////////
	//
	//if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_X))
	//{
	//	if (m_tData.iBombCount > 0)
	//	{
	//		if (m_pCurState == RAMBRO&&m_tBodyFrame.fFrame > 2.f&& m_tBodyFrame.fFrame < 2.5f)
	//		{
	//			GET_INSTANCE(CSoundMgr)->PlaySoundw(L"ThrowBomb.wav", CSoundMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, -10.f, 0.f), 40.f), CObjMgr::GRENADE);

	//			//m_tData.iBombCount--;
	//		}
	//		else if (m_pCurState == BROHARD&&m_tBodyFrame.fFrame > 2.f&& m_tBodyFrame.fFrame < 2.7f)
	//		{
	//			GET_INSTANCE(CSoundMgr)->PlaySoundw(L"ThrowBomb.wav", CSoundMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, -10.f, 0.f), 45.f), CObjMgr::GRENADE);

	//			//m_tData.iBombCount--;
	//		}
	//		else if (m_pCurState == CHUCKBRO&&m_tBodyFrame.fFrame > 4.f&& m_tBodyFrame.fFrame < 4.7f)
	//		{
	//			GET_INSTANCE(CSoundMgr)->PlaySoundw(L"ThrowBomb.wav", CSoundMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(-200, -330, 0.f), -30.f), CObjMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(-150, -330, 0.f), -30.f), CObjMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(-100, -330, 0.f), -30.f), CObjMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(-50, -330, 0.f), -30.f), CObjMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0, -330, 0.f), -30.f), CObjMgr::GRENADE);
	//			//m_tData.iBombCount--;
	//		}
	//		else if (m_pCurState == BROMMANDO&&m_tBodyFrame.fFrame > 2.f&& m_tBodyFrame.fFrame < 2.7f)
	//		{

	//			GET_INSTANCE(CSoundMgr)->PlaySoundw(L"ThrowBomb.wav", CSoundMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, 0.f, 0.f), 45.f), CObjMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(20.f, 0.f, 0.f), 45.f), CObjMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(40.f, 0.f, 0.f), 45.f), CObjMgr::GRENADE);

	//			//m_tData.iBombCount--;
	//		}
	//		else if (m_pCurState == BRODREDD&&m_tBodyFrame.fFrame > 2.f&& m_tBodyFrame.fFrame < 2.5f)
	//		{
	//			GET_INSTANCE(CSoundMgr)->PlaySoundw(L"ThrowBomb.wav", CSoundMgr::GRENADE);
	//			GET_INSTANCE(CObjMgr)->AddObject(CreateGrenade<CGrenade>(D3DXVECTOR3(0.f, -10.f, 0.f), 0.f,5.f), CObjMgr::GRENADE);

	//			//m_tData.iBombCount--;
	//		}
	//		GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
	//	}
	//	m_bCurState = THROW;
	//	m_gCurState = GUNEND;
	//}
}

void CPlayer::KeyInput()
{
	
	//if (m_bIsHang && !m_bIsRadder)//매달린 상태에서의 점프 
	//{
	//	if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_UP))
	//	{
	//		CSoundMgr::GetInstance()->PlaySoundw(L"BroClimb.wav", CSoundMgr::PLAYER);
	//		iMotionCount++;
	//		m_bIsJump = true;
	//	}

	//}
	//
	//if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_UP) && !m_bIsRadder && m_bIsGround)//그냥 맨땅의 점프 
	//{
	//	CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos, L"MoveEffect",
	//		L"JumpEffect", { 0.f, 6.f }, 5.f, EFFECT_NONCOLLISION);
	//	GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
	//	int iRand = GetRandomNumber(0, 1);
	//	if(iRand ==0)
	//		CSoundMgr::GetInstance()->PlaySoundw(L"BroJump0.wav", CSoundMgr::PLAYER);
	//	else if(iRand ==1)
	//		CSoundMgr::GetInstance()->PlaySoundw(L"BroJump1.wav", CSoundMgr::PLAYER);

	//	m_bIsJump = true;
	//}

	//if (m_bIsRadder)
	//{
	//	if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_UP))
	//	{
	//		m_bDownUp = true;
	//		m_tInfo.vPos.y -= RADDER_SPEED;
	//	}
	//	else
	//		m_bDownUp = false;

	//	if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_DOWN))
	//	{
	//		m_bDownDown = true;
	//		m_tInfo.vPos.y += RADDER_SPEED;
	//	}
	//	else
	//		m_bDownDown = false;

	//}

	////////////////////////////////////////////////왼쪽//////////////////////////////////////////
	//
	//if (m_bIsGround)
	//{
	//	
	//	if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_LEFT))
	//	{
	//		m_bIsHang = false;
	//		m_bDownLeft = true;
	//		m_tInfo.vPos.x -= m_fSpeed;
	//		m_tInfo.vSize.x = -SCALEX;
	//		m_bCurState = WALK;
	//		m_gCurState = GUNWALK;	
	//		m_fWalkTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		if (m_fWalkTime > 0.11f)
	//		{
	//			CSoundMgr::GetInstance()->PlaySoundw(L"BroBrickRun0.wav", CSoundMgr::PLAYER);
	//			m_fWalkTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		}

	//	}
	//	else if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_RIGHT))
	//	{

	//		m_bIsHang = false;
	//		m_bDownRight = true;
	//		m_tInfo.vPos.x += m_fSpeed;
	//		m_tInfo.vSize.x = SCALEX;
	//		m_bCurState = WALK;
	//		m_gCurState = GUNWALK;
	//		m_fWalkTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		if (m_fWalkTime > 0.11f)
	//		{
	//			CSoundMgr::GetInstance()->PlaySoundw(L"BroWoodRun0.wav", CSoundMgr::PLAYER);
	//			m_fWalkTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//		}
	//	}	
	//	else
	//	{
	//		m_bDownLeft = false;
	//		m_bDownRight = false;
	//		m_bCurState = STAND;
	//		m_gCurState = GUNSTAND;
	//		
	//	}
	//}

	//if (!m_bIsGround)
	//{
	//	if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_LEFT))
	//	{
	//		m_bDownLeft = true;
	//		m_tInfo.vPos.x -= m_fSpeed;
	//		m_tInfo.vSize.x = -SCALEX;

	//	}
	//	else if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_RIGHT))
	//	{
	//		m_bDownRight = true;
	//		m_tInfo.vPos.x += m_fSpeed;
	//		m_tInfo.vSize.x = SCALEX;
	//	}
	//	m_bDownLeft = false;
	//	m_bDownRight = false;
	//	m_bCurState = JUMP;
	//	m_gCurState = GUNSTAND;
	//}


	//if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_SHIFT))
	//{
	//	m_bIsShift = true;
	//}
	//else
	//	m_bIsShift = false;

	//if (m_bIsHang)
	//{
	//	if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_LEFT))
	//	{
	//		m_bDownLeft = true;
	//		if (m_bIsShift)
	//		{
	//			if (iMotionCount % 2 == 0)
	//			{
	//				m_bCurState = CLIMBLEFTHANG;
	//				m_gCurState = GUNEND;
	//			}
	//			else
	//			{
	//				m_bCurState = CLIMBRIGHTHANG;
	//				m_gCurState = GUNEND;
	//			}
	//		}
	//		else
	//		{
	//			m_bCurState = FALL;
	//			m_gCurState = GUNSTAND;
	//		}
	//	}
	//	else if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_RIGHT))
	//	{

	//		m_bDownRight = true;
	//		if (m_bIsShift)
	//		{
	//			if (iMotionCount % 2 == 0)
	//			{
	//				m_bCurState = CLIMBRIGHTHANG;
	//				m_gCurState = GUNEND;
	//			}
	//			else
	//			{
	//				m_bCurState = CLIMBLEFTHANG;
	//				m_gCurState = GUNEND;
	//			}
	//		}
	//		else
	//		{
	//			m_bCurState = FALL;
	//			m_gCurState = GUNSTAND;
	//		}
	//	}
	//	else
	//	{
	//		m_bIsHang = false;
	//		m_bDownLeft = false;
	//		m_bDownRight = false;
	//		m_bCurState = FALL;
	//		m_gCurState = GUNSTAND;
	//	}
	//}
	//

	//if (GET_INSTANCE(CKeyMgr)->KeyCombine(KEY_LEFT, KEY_SPACE))//대쉬 
	//{
	//	m_tInfo.vPos.x -= 2.f;
	//	m_bCurState = DASH;
	//	m_gCurState = GUNWALK;
	//	m_fWalkTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	//}
	//if (GET_INSTANCE(CKeyMgr)->KeyCombine(KEY_RIGHT, KEY_SPACE))//대쉬
	//{
	//	m_tInfo.vPos.x += 2.f;
	//	m_bCurState = DASH;
	//	m_gCurState = GUNWALK;
	//}


	//if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_S))//캐릭터 변하게했어용 
	//{
	//	m_pCurState = BROHARD;
	//}
	//if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_D))//캐릭터 변하게했어용 
	//{
	//	m_pCurState = CHUCKBRO;
	//}
	//if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_F))//캐릭터 변하게했어용 
	//{
	//	m_pCurState = BROMMANDO;
	//}
	//if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_A))//캐릭터 변하게했어용 
	//{
	//	m_pCurState = RAMBRO;
	//}
	//if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_G))//캐릭터 변하게했어용 
	//{
	//	m_pCurState = BRODREDD;
	//}

}





void CPlayer::IsJumping()
{

	if (m_bIsJump)
	{
		m_bCurState = JUMP;
		m_gCurState = GUNSTAND;
		float fParabola = m_fJumpPow * m_fJumpAcc -
			GRAVITY * m_fJumpAcc * m_fJumpAcc * 0.5f;

		m_tInfo.vPos.y -= fParabola;
		m_fJumpAcc += 0.3f; // 가속도는 꾸준히 증가한다.

		if ((0.f > fParabola))
		{
			m_bCurState = FALL;
			m_gCurState = GUNSTAND;
			m_fJumpAcc = 0.5f;
			m_bIsJump = false;
		}
	}
	else
	{
		if (m_bIsHang&&(m_bDownLeft||m_bDownRight)&& m_bIsShift &&!m_bIsRadder)
		{
			CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tInfo.vPos + D3DXVECTOR3{ 16.f*m_tInfo.vSize.x, -37.f, 0.f }, L"MoveEffect",
				L"LandEffect", { 0.f, 24.f }, 5.f, EFFECT_NONCOLLISION);
			GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
			m_tInfo.vPos.y += HANG_DOWN;
		}
		else if (!m_bIsRadder&&!m_bPause)
		{
			m_tInfo.vPos.y += m_fSpeed;
		}
	}
}




void CPlayer::SceneBodyChange()
{
	if (m_bCurState != m_bPreState)
	{
		switch (m_bCurState)
		{

		case CLIMBLEFTHANG://1
			m_wstrBodyStateKey = L"ClimbLeftHang";
			m_tBodyFrame = { 0.f, 1.f };
			break;
		
		case CLIMBRIGHTHANG://10
			m_wstrBodyStateKey = L"ClimbRightHang";
			m_tBodyFrame = { 0.f, 1.f };
			break;
	
		case FALL://3
			m_wstrBodyStateKey = L"Fall";
			m_tBodyFrame = { 0.f, 3.f };
			break;
		case JUMP://3
			m_wstrBodyStateKey = L"Jump";
			m_tBodyFrame = { 0.f, 3.f };
			break;
		case RUN://8
			m_wstrBodyStateKey = L"Run";
			m_tBodyFrame = { 0.f, 8.f };
			break;
	
		case STAND://1
			m_wstrBodyStateKey = L"Stand";
			m_tBodyFrame = { 0.f, 1.f };
			break;
		case THROW://8
			m_wstrBodyStateKey = L"Throw";
			m_tBodyFrame = { 0.f, 8.f };
			break;
		case WALK://8
			m_wstrBodyStateKey = L"Walk";
			m_tBodyFrame = { 0.f, 8.f };
			break;
		case DASH:
			m_wstrBodyStateKey = L"Dash";
			m_tBodyFrame = { 0.f, 7.f };
			break;
		}
		m_bPreState = m_bCurState;
	}
}

void CPlayer::SceneGunChange()
{
	if (m_gCurState != m_gPreState)
	{
		switch (m_gCurState)
		{
		case GUNSHOOT:
			m_wstrGunStateKey = L"GunShoot";
			m_tGunFrame = { 0.f, 4.f };
			break;

		case GUNSTAND:
			m_wstrGunStateKey = L"GunStand";
			m_tGunFrame = { 0.f, 1.f };
			break;

		case GUNWALK:
			m_wstrGunStateKey = L"GunWalk";
			m_tGunFrame = { 0.f, 8.f };
			break;
		case GUNEND:
			m_wstrGunStateKey = L"GunEnd";
			m_tGunFrame = { 0.f, 1.f };
			break;

		}
		m_gPreState = m_gCurState;
	}
}

void CPlayer::BroChange()
{
	if (m_pCurState != m_pPreState)
	{
		switch (m_pCurState)
		{
		case RAMBRO:
			m_wstrObjKey = L"RamBro";
			m_wstrObjKey2 = L"RamBroGun";	
			m_tData.iPlayerNumber = 0;
			break;

		case BROHARD:
			m_wstrObjKey = L"BroHard";
			m_wstrObjKey2 = L"BroHardGun";		
			m_tData.iPlayerNumber = 1;
			break;

		case CHUCKBRO:
			m_wstrObjKey = L"ChuckBro";
			m_wstrObjKey2 = L"ChuckBroGun";		
			m_tData.iPlayerNumber = 2;
			break;
		case BROMMANDO:
			m_wstrObjKey = L"Brommando";
			m_wstrObjKey2 = L"BrommandoGun";	
			m_tData.iPlayerNumber = 3;
			break;

		case BRODREDD:
			m_wstrObjKey = L"BroDredd";
			m_wstrObjKey2 = L"BroDreddGun";
			m_tData.iPlayerNumber = 4;
			break;
		}
		GET_INSTANCE(CDataSubject)->Notify(CDataSubject::PLAYER_DATA, &m_tData);
		m_pPreState = m_pCurState;
	}
}

void CPlayer::SetRebirthPoint(D3DXVECTOR3 vRebirthPoint)
{
	
		GET_INSTANCE(CSoundMgr)->PlaySoundw(L"Yeah.wav", CSoundMgr::UI);
		m_vRebirthPoint = vRebirthPoint;
	
}

