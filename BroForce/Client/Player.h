#pragma once
#include "Obj.h"
class CMotion; 

class CPlayer :
	public CObj
{
public:
	CPlayer();
	virtual ~CPlayer();
public:
	int GetPlayerNumber() { return (int)(m_pCurState); }
	enum PLAYERNUMBER { RAMBRO, BROHARD, CHUCKBRO, BROMMANDO ,BRODREDD};
	enum BODYSTATE {CLIMBLEFTHANG, CLIMBRIGHTHANG,  FALL, JUMP, RUN,  STAND, THROW, WALK,DASH };
	enum GUNSTATE { GUNSHOOT, GUNSTAND, GUNWALK,GUNEND };

public:	
	DATA& GetData() { return m_tData; }

public:
	virtual HRESULT Initialize();
	virtual void LateInit();
	virtual int Update();
	virtual void LateUpdate();
	virtual void Render();
	virtual void Release();
	virtual void UpdateRect() override;

	
public:
	
	void LifeDeduct();
	void LifeUp();
	void MoveBodyFrame(float  fSpeed);
	void MoveGunFrame(float fSpeed);

	void KeyShoot();

	void KeyInput();
	void IsJumping();
	void SceneBodyChange();
	void SceneGunChange();
	void BroChange();
public:
	bool& GetIsHang() { return m_bIsHang; }
	void SetIsHang(bool tf) { m_bIsHang = tf; }

	bool& GetIsGround() { return m_bIsGround; }
	void SetIsGround(bool tf) { m_bIsGround = tf; }

	bool& GetIsRadder() { return m_bIsRadder; }
	void SetIsRadder(bool tf) { m_bIsRadder = tf; }


	bool& GetIsDownUp() { return m_bDownUp; }
	bool& GetIsDownDown() { return m_bDownDown; }

	bool& GetIsDownLeft() { return m_bDownLeft; }

	bool& GetIsDownRight() { return m_bDownRight; }
	
	bool& GetIsJump() { return m_bIsJump; }
	void SetIsJump(bool tf) { m_bIsJump = tf; }

	void SetRebirthPoint(D3DXVECTOR3 vRebirthPoint);
	void SetLifeCount() { m_tData.iLife--; }
	void SetLifePlus() { m_tData.iLife++; }

	int GetLifeCOunt() { return m_tData.iLife; }
	bool GetNoDamage() { return m_bNoDamage; }
	void SetPause(bool tf) { m_bPause = tf; }
private:

	template <typename T>
	CObj* CreateBullet()
	{
		return CObjFactory<T>::CreateBullet(m_tInfo.vPos, m_tInfo.vSize);
	}

	template <typename T>
	CObj* CreateBullet(float fAngle)
	{
		return CObjFactory<T>::CreateBullet(m_tInfo.vPos, m_tInfo.vSize, fAngle, m_pCurState, BRO_BULLET);
	}
	template <typename T>
	CObj* CreateBullet(float fAngle,float Who)
	{
		return CObjFactory<T>::CreateBullet(m_tInfo.vPos, m_tInfo.vSize, fAngle, Who, BRO_BULLET);
	}

	template <typename T>
	CObj* CreateBullet(D3DXVECTOR3 vPos, float fAngle)
	{
		return CObjFactory<T>::CreateBullet(m_tInfo.vPos + vPos, m_tInfo.vSize, fAngle, m_pCurState, BRO_BULLET);
	}


	template <typename T>
	CObj* CreateGrenade(D3DXVECTOR3 vPos, float fAngle)
	{
		return CObjFactory<T>::CreateGrenade(m_tInfo.vPos + vPos, m_tInfo.vSize, fAngle, m_pCurState);
	}
	template <typename T>
		CObj* CreateGrenade(D3DXVECTOR3 vPos, float fAngle,float who)
	{
		return CObjFactory<T>::CreateGrenade(m_tInfo.vPos + vPos, m_tInfo.vSize, fAngle, who);
	}
	
private:
	float m_fYeahTime;
	float m_fWalkTime;
	bool m_bIsShift;
	bool m_bIsFly;
	int m_iTickCount;
	DWORD m_dHitAlpha;
	float m_bShootTime;
	float m_fGunFrameSpeed;
	float m_fBodyFrameSpeed;
	bool m_bIsRadder;		//사다리
	bool m_bIsJump;			//점프
	bool m_bIsFall;			//떨어짐
	bool m_bPause;
	bool m_bDownLeft;		//왼쪽키
	bool m_bDownRight;		//오른쪽키
	bool m_bDownUp;			//위
	bool m_bDownDown;		//아래

	bool m_bIsAttack;

	bool m_bIsGround;
	bool m_bIsHang;

	bool m_bBroHardShootHand;

	int iMotionCount;
	int m_iLifeCount;
	bool m_bRebirth;
	bool m_bNoDamage;
private:
	float m_fJumpPow;
	float m_fJumpAcc;
	float fShootSpeed;//연사속도

private:
	D3DXVECTOR3 m_vRebirthPoint;

private:

	CMotion* m_pMotion;

	DATA	m_tData;

	FRAME	m_tBodyFrame;
	FRAME	m_tGunFrame;

	wstring m_wstrBodyStateKey = L"";
	wstring m_wstrGunStateKey = L"";

	PLAYERNUMBER m_pCurState;
	PLAYERNUMBER m_pPreState;


	BODYSTATE m_bCurState;
	BODYSTATE m_bPreState;

	GUNSTATE m_gCurState;
	GUNSTATE m_gPreState;

};

