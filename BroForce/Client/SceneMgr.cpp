#include "stdafx.h"
#include "SceneMgr.h"
#include"Logo.h"
#include "StageOne.h"
#include "StageTwo.h"
#include "StageBoss.h"

IMPLEMENT_SINGLETON(CSceneMgr)

CSceneMgr::CSceneMgr()
	: m_pScene(nullptr), m_ePreScene(STAGEEND), m_eCurScene(STAGEEND)
{
}


CSceneMgr::~CSceneMgr()
{
	Release();
}

HRESULT CSceneMgr::SceneChange(SCENE_ID eScene)
{
	m_eCurScene = eScene;

	if (m_ePreScene != m_eCurScene)
	{
		SafeDelete(m_pScene);

		switch (m_eCurScene)
		{
		case LOGO:
			m_pScene = new CLogo;
			break;
		case STAGEONE:
			m_pScene = new CStageOne;
			break;
		case STAGETWO:
			m_pScene = new CStageTwo;
			break;
		case STAGEBOSS:
			m_pScene = new CStageBoss;
			break;
		}

		if (FAILED(m_pScene->Initialize()))
		{
			ERR_MSG(L"Scene Change Failed!");
			SafeDelete(m_pScene);
			return E_FAIL;
		}

		m_ePreScene = m_eCurScene;
	}
	if (m_eCurScene == STAGEONE)
	{
		g_StageX = STAGE_ONE_X;
		g_StageY = STAGE_ONE_Y;
	}
	else if(m_eCurScene == STAGETWO)
	{
		g_StageX = STAGE_TWO_X;
		g_StageY = STAGE_TWO_Y;
	}
	else if (m_eCurScene == STAGEBOSS)
	{
		g_StageX = STAGE_BOSS_X;
		g_StageY = STAGE_BOSS_Y;
	}

	return S_OK;
}

void CSceneMgr::Update()
{
	NULL_CHECK(m_pScene);
	m_pScene->Update();
}

void CSceneMgr::LateUpdate()
{
	NULL_CHECK(m_pScene);
	m_pScene->LateUpdate();
}

void CSceneMgr::Render()
{
	NULL_CHECK(m_pScene);
	m_pScene->Render();
}

void CSceneMgr::Release()
{
	SafeDelete(m_pScene);
}
