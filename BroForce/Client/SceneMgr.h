#pragma once

class CScene;
class CSceneMgr
{
	DECLARE_SINGLETON(CSceneMgr)

public:
	enum SCENE_ID { LOGO, STAGEONE,STAGETWO , STAGEBOSS , GAMEOVER ,STAGEEND};

private:
	CSceneMgr();
	~CSceneMgr();

public:
	SCENE_ID& GetCurScene() { return m_eCurScene; }
	HRESULT SceneChange(SCENE_ID eScene);
	void Update();
	void LateUpdate();
	void Render();
	void Release();

private:
	CScene*		m_pScene;
	SCENE_ID	m_ePreScene;
	SCENE_ID	m_eCurScene;
};

