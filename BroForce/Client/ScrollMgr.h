#pragma once
class CScrollMgr
{
public:
	CScrollMgr();
	~CScrollMgr();

public:
	static const D3DXVECTOR3& GetScroll();

public:
	static void SetScroll(const D3DXVECTOR3& vScroll);

	static void SettingScroll(const D3DXVECTOR3 & vScroll);

private:
	static D3DXVECTOR3 m_vScroll;
};

