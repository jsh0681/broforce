#pragma once
#include "Texture.h"

class CSingleTexture :
	public CTexture
{
public:
	CSingleTexture();
	virtual ~CSingleTexture();

public:
	// CTexture을(를) 통해 상속됨
	virtual const TEXINFO * GetTexInfo(const wstring & wstrStateKey = L"", const int & iImgIndex = 0) const override;

	virtual HRESULT InsertTexture(const wstring & wstrFilePath,
		const wstring & wstrStateKey = L"",
		const int & iImgCount = 0) override;

	virtual void Release() override;
private:
	TEXINFO* m_pTexInfo;
};

