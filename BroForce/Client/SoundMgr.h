#pragma once
class CSoundMgr
{
	DECLARE_SINGLETON(CSoundMgr)

public:
	enum SOUNDID { BGM, PLAYER, MOOK, SUICIDEMOOK, BULLET, EXPLOSIVE, GRENADE ,BOSS, BOSSBULLET, EFFECT , UI, END };

private:
	CSoundMgr();
	~CSoundMgr();

public:
	void Initialize();
	void Update();
	void PlaySoundw(const TCHAR* pSoundKey, SOUNDID eID);
	void PlayBGM(const TCHAR* pSoundKey);
	void StopSound(SOUNDID eID);
	void StopAll();
	void Release();

private:
	void LoadSoundFile(const char* pFilePath);

private:
	FMOD_SYSTEM*	m_pSystem;
	FMOD_CHANNEL*	m_pChannelArr[END];

	map<const TCHAR*, FMOD_SOUND*>	m_MapSound;
};

