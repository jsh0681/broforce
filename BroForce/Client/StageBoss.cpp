#include "stdafx.h"
#include "StageBoss.h"
#include "Terrain.h"
#include "Flag.h"
#include "Player.h"
#include "Bullet.h"
#include "Mook.h"
#include "SuicideMook.h"
#include"Explosive.h"
#include"Hostage.h"
#include"Hellicopter.h"
#include "MookTank.h"
#include"MegaHellicopter.h"

CStageBoss::CStageBoss() : m_fCountTime(0.f), m_bCountStart(true), m_bCount(false), m_fCountDown(0.f), m_fChangeTime(0.f)
{
}


CStageBoss::~CStageBoss()
{
	Release();
}

HRESULT CStageBoss::Initialize()
{
	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/StageBossBack.png", L"StageBossBack")))
	{
		ERR_MSG(L"../Data/StageBossBack.png");
		return E_FAIL;
	}

	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/StageBoss.png", L"StageBoss")))
	{
		ERR_MSG(L"../Data/StageBoss.png");
		return E_FAIL;
	}


	CObj* pObj = CObjFactory<CTerrain>::CreateObj();
	NULL_CHECK_RETURN(pObj, E_FAIL);
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::TERRAIN);


	pObj = CObjFactory<CPlayer>::CreateObj(D3DXVECTOR3(150.f,1750.f, 0.f));
	NULL_CHECK_RETURN(pObj, E_FAIL);
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::PLAYER);

	pObj = CObjFactory<CHellicopter>::CreateObj();
	NULL_CHECK_RETURN(pObj, E_FAIL);
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::HELLICOPTER);

	pObj = CObjFactory<CMegaHellicopter>::CreateObj();
	NULL_CHECK_RETURN(pObj, E_FAIL);
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::MEGAHELLICOPTER);

	//pObj = CObjFactory<CMookTank>::CreateObj();
	//NULL_CHECK_RETURN(pObj, E_FAIL);
	//GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::MOOKTANK);

	if (FAILED(LoadMook(L"../Data/StageBossMook.dat")))
		return E_FAIL;

	if (FAILED(LoadSuicideMook(L"../Data/StageBossSuicideMook.dat")))
		return E_FAIL;

	if (FAILED(LoadFlag(L"../Data/StageBossFlag.dat")))
		return E_FAIL;

	if (FAILED(LoadExplosive(L"../Data/StageBossGas.dat")))
		return E_FAIL;


	if (FAILED(LoadHostage(L"../Data/StageBossHostage.dat")))
		return E_FAIL;

	CScrollMgr::SettingScroll(D3DXVECTOR3(0.f, 1500.f, 0.f));
	GET_INSTANCE(CSoundMgr)->PlayBGM(L"StageBossBgm.wav");
	
	return S_OK;
}

void CStageBoss::Update()
{
	if (m_bCountStart)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"ThreeTwoOneGo.wav", CSoundMgr::UI);
		m_bCountStart = false;
	}

	m_fCountTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_fCountTime > 1.f)
	{
		m_fCountDown = 1;
		if (m_fCountTime > 2.5f)
		{
			m_fCountDown = 2;
			if (m_fCountTime > 4.f)
			{
				m_fCountDown = 3;
				if (m_fCountTime > 4.5f)
				{
					m_bCount = true;
				}
			}

		}
	}
	ScrollLock();
	GET_INSTANCE(CObjMgr)->Update();
}

void CStageBoss::LateUpdate()
{
	GET_INSTANCE(CObjMgr)->LateUpdate();
}

void CStageBoss::Render()
{
	const D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
	const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"StageBossBack");
	NULL_CHECK(pTexInfo);

	float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

	D3DXMATRIX matScale, matRotZ, matTrans, matWorld;

	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixRotationZ(&matRotZ, -D3DXToRadian(0.f));
	D3DXMatrixTranslation(&matTrans, pTexInfo->tImgInfo.Width * 0.5f - vScroll.x, pTexInfo->tImgInfo.Height * 0.5f - vScroll.y, 0.f);

	matWorld = matScale * matRotZ * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));

	pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"StageBoss");
	NULL_CHECK(pTexInfo);

	fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	fCenterY = pTexInfo->tImgInfo.Height * 0.5f;


	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixRotationZ(&matRotZ, -D3DXToRadian(0.f));
	D3DXMatrixTranslation(&matTrans, pTexInfo->tImgInfo.Width * 0.5f - vScroll.x, pTexInfo->tImgInfo.Height * 0.5f - vScroll.y, 0.f);

	matWorld = matScale * matRotZ * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	if (!m_bCount)
	{
		pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"Stage", L"CountDown", (int)m_fCountDown);
		NULL_CHECK(pTexInfo);

		float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
		float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

		D3DXMATRIX matScale, matRotZ, matTrans, matWorld;

		D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
		D3DXMatrixRotationZ(&matRotZ, -D3DXToRadian(0.f));
		D3DXMatrixTranslation(&matTrans, 400, 200, 0.f);

		matWorld = matScale * matRotZ * matTrans;

		GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
		GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
			&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	}

	GET_INSTANCE(CObjMgr)->Render();
}

HRESULT CStageBoss::LoadMook(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CMook>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);

		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::MOOK);
	}

	CloseHandle(hFile);
	return S_OK;
}


HRESULT CStageBoss::LoadFlag(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CFlag>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);

		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::FLAG);
	}

	CloseHandle(hFile);
	return S_OK;
}

HRESULT CStageBoss::LoadExplosive(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CExplosive>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);

		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::EXPLOSIVE);
	}

	CloseHandle(hFile);
	return S_OK;
}

HRESULT CStageBoss::LoadSuicideMook(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CSuicideMook>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);

		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::SUICIDEMOOK);
	}

	CloseHandle(hFile);
	return S_OK;
}


HRESULT CStageBoss::LoadHostage(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CHostage>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);

		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::HOSTAGE);
	}

	CloseHandle(hFile);
	return S_OK;
}






void CStageBoss::Release()
{
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::MEGAHELLICOPTER);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::GRENADE);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::EXPLOSIVE);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::EFFECT);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::FLAG);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::SUICIDEMOOK);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::MOOK);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::UI);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::PLAYER);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::TERRAIN);
	CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
}

void CStageBoss::ScrollLock()
{

	float fDeltaTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();

	float fOffSetX = WINCX*0.5f;

	float fOffSetY = WINCY*0.5f;//200

	CObj* m_pPlayer = dynamic_cast<CPlayer*>(GET_INSTANCE(CObjMgr)->GetPlayer());

	if (m_pPlayer->GetInfo().vPos.x <STAGE_BOSS_X - 400.f &&fOffSetX <= m_pPlayer->GetInfo().vPos.x - CScrollMgr::GetScroll().x)//�ʻ��� x ũ�� -wincx/2
		CScrollMgr::SetScroll({ 300.f * fDeltaTime, 0.f, 0.f });

	if (m_pPlayer->GetInfo().vPos.x > 0.f + 400.f &&fOffSetX >= m_pPlayer->GetInfo().vPos.x - CScrollMgr::GetScroll().x) //0+wincx/2
		CScrollMgr::SetScroll({ -300.f * fDeltaTime, 0.f, 0.f });

	if (m_pPlayer->GetInfo().vPos.y < STAGE_BOSS_Y - 300.f&& fOffSetY <= m_pPlayer->GetInfo().vPos.y - CScrollMgr::GetScroll().y)//�ʻ��� Y ũ�� -wincy/2
		CScrollMgr::SetScroll({ 0.f, 300.f * fDeltaTime, 0.f });

	if (m_pPlayer->GetInfo().vPos.y > 0.f + 300.f&&fOffSetY >= m_pPlayer->GetInfo().vPos.y - CScrollMgr::GetScroll().y)//ž 0+wincy/2
		CScrollMgr::SetScroll({ 0.f, -300.f * fDeltaTime, 0.f });


}

