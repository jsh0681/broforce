#pragma once
#include "Scene.h"
class CStageBoss :
	public CScene
{
public:
	CStageBoss();
	virtual ~CStageBoss();

	// CScene을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	HRESULT LoadMook(const wstring & wstrFilePath);
	HRESULT LoadFlag(const wstring & wstrFilePath);
	HRESULT LoadExplosive(const wstring & wstrFilePath);
	HRESULT LoadSuicideMook(const wstring & wstrFilePath);
	HRESULT LoadHostage(const wstring & wstrFilePath);
	virtual void Release() override;
	void ScrollLock();
private:

	float m_fChangeTime;
	bool m_bCount;
	bool m_bCountStart;
	float m_fCountTime;
	float m_fCountDown;
};

