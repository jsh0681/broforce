#include "stdafx.h"
#include "StageOne.h"
#include "Terrain.h"
#include "Flag.h"
#include "Player.h"
#include "Bullet.h"
#include "Mook.h"
#include "SuicideMook.h"
#include"Explosive.h"
#include"Hostage.h"
#include"Hellicopter.h"

CStageOne::CStageOne() :m_fCountTime(0.f), m_bCountStart(true), m_bCount(false), m_fCountDown(0.f)
{
}


CStageOne::~CStageOne()
{
	Release();
}

HRESULT CStageOne::Initialize()
{
	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/StageOneBack.png", L"StageOneBack")))
	{
		ERR_MSG(L"../Data/StageOneBack.png");
		return E_FAIL;
	}
	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/StageOne.png", L"StageOne")))
	{
		ERR_MSG(L"../Data/StageOne.png");
		return E_FAIL;
	}

	CObj* pObj = CObjFactory<CTerrain>::CreateObj();
	NULL_CHECK_RETURN(pObj, E_FAIL);
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::TERRAIN);


	pObj = CObjFactory<CPlayer>::CreateObj(D3DXVECTOR3( 200, 660.f, 0.f ));
	NULL_CHECK_RETURN(pObj, E_FAIL);
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::PLAYER);

	pObj = CObjFactory<CHellicopter>::CreateObj(D3DXVECTOR3(-10.f, 150.f,0.f));
	NULL_CHECK_RETURN(pObj, E_FAIL);
	GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::HELLICOPTER);


	if (FAILED(LoadMook(L"../Data/StageOneMook.dat")))
		return E_FAIL;
	
	if (FAILED(LoadSuicideMook(L"../Data/StageOneSuicideMook.dat")))
		return E_FAIL;

	if (FAILED(LoadFlag(L"../Data/StageOneFlag.dat")))
		return E_FAIL;
	
	if (FAILED(LoadExplosive(L"../Data/StageOneGas.dat")))
		return E_FAIL;


	if (FAILED(LoadHostage(L"../Data/StageOneHostage.dat")))
		return E_FAIL;
	
	CScrollMgr::SettingScroll(D3DXVECTOR3(0.f,350.f,0.f));
	GET_INSTANCE(CSoundMgr)->PlayBGM(L"StageOneBgm.wav");
	
	return S_OK;
}

void CStageOne::Update()
{
	if(m_bCountStart)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"ThreeTwoOneGo.wav", CSoundMgr::UI);
		m_bCountStart = false;
	}
	SceneChange();
	m_fCountTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_fCountTime > 1.f)
	{
		m_fCountDown = 1;
		if (m_fCountTime > 2.5f)
		{
			m_fCountDown = 2;
			if (m_fCountTime > 4.f)
			{
				m_fCountDown = 3;
				if (m_fCountTime > 4.5f)
				{
					m_bCount = true;
				}
			}

		}
	}
	ScrollLock();
	GET_INSTANCE(CObjMgr)->Update();
}

void CStageOne::LateUpdate()
{
	GET_INSTANCE(CObjMgr)->LateUpdate();
}

void CStageOne::Render()
{
	const D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();
	const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"StageOneBack");
	NULL_CHECK(pTexInfo);

	float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

	D3DXMATRIX matScale, matRotZ, matTrans, matWorld;

	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixRotationZ(&matRotZ, -D3DXToRadian(0.f));
	D3DXMatrixTranslation(&matTrans, pTexInfo->tImgInfo.Width * 0.5f - vScroll.x, pTexInfo->tImgInfo.Height * 0.5f - vScroll.y, 0.f);

	matWorld = matScale * matRotZ * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));

	pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"StageOne");
	NULL_CHECK(pTexInfo);

	fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	fCenterY = pTexInfo->tImgInfo.Height * 0.5f;


	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixRotationZ(&matRotZ, -D3DXToRadian(0.f));
	D3DXMatrixTranslation(&matTrans, pTexInfo->tImgInfo.Width * 0.5f - vScroll.x, pTexInfo->tImgInfo.Height * 0.5f - vScroll.y, 0.f);

	matWorld = matScale * matRotZ * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	if (!m_bCount)
	{
		pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"Stage", L"CountDown", m_fCountDown);
		NULL_CHECK(pTexInfo);

		float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
		float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

		D3DXMATRIX matScale, matRotZ, matTrans, matWorld;

		D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
		D3DXMatrixRotationZ(&matRotZ, -D3DXToRadian(0.f));
		D3DXMatrixTranslation(&matTrans, 400, 200, 0.f);

		matWorld = matScale * matRotZ * matTrans;

		GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
		GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
			&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	}

	GET_INSTANCE(CObjMgr)->Render();

}

HRESULT CStageOne::LoadMook(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CMook>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);
		
		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::MOOK);
	}

	CloseHandle(hFile);
	return S_OK;
}


HRESULT CStageOne::LoadFlag(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CFlag>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);

		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::FLAG);
	}

	CloseHandle(hFile);
	return S_OK;
}

HRESULT CStageOne::LoadExplosive(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CExplosive>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);

		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::EXPLOSIVE);
	}

	CloseHandle(hFile);
	return S_OK;
}

HRESULT CStageOne::LoadSuicideMook(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CSuicideMook>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);

		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::SUICIDEMOOK);
	}

	CloseHandle(hFile);
	return S_OK;
}
void CStageOne::SceneChange()
{
	if (g_bDestroy)
	{
		m_fChangeTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
		if (m_fChangeTime > 3.f)
		{
			g_bDestroy = false;
			GET_INSTANCE(CSceneMgr)->SceneChange(CSceneMgr::SCENE_ID::STAGETWO);
		}
	}
}

HRESULT CStageOne::LoadHostage(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadObj Failed!");
		return E_FAIL;
	}

	DWORD dwByte = 0;
	CObj* pObj = nullptr;
	while (true)
	{
		pObj = CObjFactory<CHostage>::CreateObj();
		NULL_CHECK_RETURN(pObj, E_FAIL);

		ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

		if (0 == dwByte)
		{
			SafeDelete(pObj);
			break;
		}

		GET_INSTANCE(CObjMgr)->AddObject(pObj, CObjMgr::HOSTAGE);
	}

	CloseHandle(hFile);
	return S_OK;
}






void CStageOne::Release()
{
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::HELLICOPTER);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::GRENADE);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::EXPLOSIVE);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::EFFECT);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::FLAG);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::SUICIDEMOOK);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::MOOK);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::UI);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::PLAYER);
	GET_INSTANCE(CObjMgr)->GroupRelease(CObjMgr::TERRAIN);
	CSoundMgr::GetInstance()->StopSound(CSoundMgr::BGM);
}

void CStageOne::ScrollLock()
{

	float fDeltaTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();

	float fOffSetX = WINCX*0.5f;

	float fOffSetY = WINCY*0.5f;//200

	CObj* m_pPlayer = dynamic_cast<CPlayer*>(GET_INSTANCE(CObjMgr)->GetPlayer());

	if (m_pPlayer->GetInfo().vPos.x <STAGE_ONE_X - 400.f &&fOffSetX <= m_pPlayer->GetInfo().vPos.x - CScrollMgr::GetScroll().x)//�ʻ��� x ũ�� -wincx/2
		CScrollMgr::SetScroll({ 300.f * fDeltaTime, 0.f, 0.f });

	if (m_pPlayer->GetInfo().vPos.x > 0.f + 400.f &&fOffSetX >= m_pPlayer->GetInfo().vPos.x - CScrollMgr::GetScroll().x) //0+wincx/2
		CScrollMgr::SetScroll({ -300.f * fDeltaTime, 0.f, 0.f });

	if (m_pPlayer->GetInfo().vPos.y < STAGE_ONE_Y - 300.f&& fOffSetY <= m_pPlayer->GetInfo().vPos.y - CScrollMgr::GetScroll().y)//�ʻ��� Y ũ�� -wincy/2
		CScrollMgr::SetScroll({ 0.f, 300.f * fDeltaTime, 0.f });

	if (m_pPlayer->GetInfo().vPos.y > 0.f + 300.f&&fOffSetY >= m_pPlayer->GetInfo().vPos.y - CScrollMgr::GetScroll().y)//ž 0+wincy/2
		CScrollMgr::SetScroll({ 0.f, -300.f * fDeltaTime, 0.f });


}



