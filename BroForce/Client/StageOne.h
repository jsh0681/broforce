#pragma once
#include "Scene.h"
class CMook;
class CSuicideMook;
class CExplosive;
class CFlag;

class CStageOne :
	public CScene
{
public:
	CStageOne();
	virtual ~CStageOne();

public:
	// CScene을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	HRESULT LoadMook(const wstring & wstrFilePath);
	HRESULT LoadFlag(const wstring & wstrFilePath);
	HRESULT LoadExplosive(const wstring & wstrFilePath);
	HRESULT LoadSuicideMook(const wstring & wstrFilePath);
	void SceneChange();
	HRESULT LoadHostage(const wstring & wstrFilePath);
	virtual void Release() override;


	void ScrollLock();

private:
	float m_fChangeTime;
	bool m_bCount;
	bool m_bCountStart;
	float m_fCountTime;
	float m_fCountDown;
	template<typename T>
	HRESULT LoadTile(const wstring & wstrFilePath,int iNumber)
	{
		HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

		if (INVALID_HANDLE_VALUE == hFile)
		{
			ERR_MSG(L"LoadObj Failed!");
			return E_FAIL;
		}

		DWORD dwByte = 0;
		CObj* pObj = nullptr;
		while (true)
		{
			pObj = CObjFactory<T>::CreateObj();
			NULL_CHECK_RETURN(pObj, E_FAIL);

			ReadFile(hFile, &(pObj->GetObj()), sizeof(OBJ), &dwByte, nullptr);

			if (0 == dwByte)
			{
				SafeDelete(pObj);
				break;
			}

			GET_INSTANCE(CObjMgr)->AddObject(pObj, (CObjMgr::OBJ_ID)iNumber);
		}

		CloseHandle(hFile);
		return S_OK;
	}

};

