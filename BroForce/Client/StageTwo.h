#pragma once
#include "Scene.h"
class CStageTwo :
	public CScene
{
public:
	CStageTwo();
	virtual ~CStageTwo();

	// CScene을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
public:
	HRESULT LoadMook(const wstring & wstrFilePath);
	HRESULT LoadFlag(const wstring & wstrFilePath);
	HRESULT LoadExplosive(const wstring & wstrFilePath);
	HRESULT LoadSuicideMook(const wstring & wstrFilePath);
	HRESULT LoadHostage(const wstring & wstrFilePath);
	void ScrollLock();

	void SceneChange();

private:
	float m_fChangeTime;
	bool m_bCount;
	bool m_bCountStart;
	float m_fCountTime;
	float m_fCountDown;

};

