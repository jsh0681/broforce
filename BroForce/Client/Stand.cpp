#include "stdafx.h"
#include "Stand.h"


CStand::CStand()
{
	m_tFrame.m_wstrObjKey = L"BroHard";
	m_tFrame.m_wstrStateKey = L"Stand";
	m_tFrame.m_fFrameSpeed = 5.f;
	m_tFrame.fFrame = 0.f;
	m_tFrame.fMaxCount = 1.f;
}


CStand::~CStand()
{
}

void CStand::MoveFrame(float fSpeed)
{
	m_tFrame.fFrame += m_tFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tFrame.fFrame > m_tFrame.fMaxCount)
	{
		m_tFrame.fFrame = 0.f;
	}
}


