#pragma once
#include "Motion.h"
class CStand :
	public CMotion
{
public:
	CStand();
	virtual ~CStand();

	// CMotion을(를) 통해 상속됨
	virtual void MoveFrame(float fSpeed) override;
};

