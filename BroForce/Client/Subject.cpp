#include "stdafx.h"
#include "Subject.h"
#include "Observer.h"

CSubject::CSubject()
{
}


CSubject::~CSubject()
{
	Release();
}

void CSubject::Release()
{
	// Subject는 Observer를 해제할 권리는 없다.
	m_ObserverLst.clear();
}

void CSubject::Subscribe(CObserver* pObserver)
{
	m_ObserverLst.push_back(pObserver);
}

void CSubject::UnSubscribe(CObserver* pObserver)
{
	auto iter_find = find_if(m_ObserverLst.begin(), m_ObserverLst.end(),
		[&pObserver](CObserver* p)
	{
		if (p == pObserver)
			return true;

		return false;
	});

	if (m_ObserverLst.end() == iter_find)
		return;

	m_ObserverLst.erase(iter_find);
}

void CSubject::Notify(int iMessage, void* pData)
{
	for (auto& pObserver : m_ObserverLst)
		pObserver->Update(iMessage, pData);
}


