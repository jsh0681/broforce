#include "stdafx.h"
#include "SuicideMook.h"
#include"Effect.h"
#include"NonAnimEffect.h"
#include"AnimEffect.h"

CSuicideMook::CSuicideMook() : m_bIsGround(false), m_bIsDetect(false), m_fIgnitionTime(0.f), m_fFrameSpeed(3.f), m_fFaintTime(0.f), m_bIsFaint(false),
m_bSoundOn(false)
{
}


CSuicideMook::~CSuicideMook()
{
	Release();
}

HRESULT CSuicideMook::Initialize()
{
	m_tObj.vSize = { 1.0f,1.0f,0.0f };
	m_tObj.vDir = { 1.0f,0.0f,0.0f };
	m_tObj.vLook = { 1.0f,0.0f,0.0f };

	m_wstrObjKey = L"SuicideMook";
	m_wstrBodyStateKey = L"Stand";
	
	
	m_bCurState = SUICIDEMOOKSTAND;
	m_bPreState = m_bCurState;

	m_tBodyFrame = { 0.f, 1.f };
	m_fSpeed = 4.f;
	return S_OK;
}

void CSuicideMook::LateInit()
{
}

void CSuicideMook::UpdateRect()
{
	m_tRect.left = LONG(m_tInfo.vPos.x - (m_pTexInfo->tImgInfo.Width*0.5f) * 0.5f);
	m_tRect.top = LONG(m_tInfo.vPos.y - (m_pTexInfo->tImgInfo.Height*0.5f) * 0.5f);
	m_tRect.right = LONG(m_tInfo.vPos.x + (m_pTexInfo->tImgInfo.Width*0.5f) * 0.5f);
	m_tRect.bottom = LONG(m_tInfo.vPos.y + (m_pTexInfo->tImgInfo.Height*0.5f) * 0.5f);
}

int CSuicideMook::Update()
{
	m_tObj.vDir = { 1.0f,0.0f,0.0f };
	m_tObj.vLook = { 1.0f,0.0f,0.0f };
	CObj::LateInit();
	if (m_tObj.vPos.x < 0 || g_StageX < m_tObj.vPos.x || m_tObj.vPos.y < 0 || g_StageY < m_tObj.vPos.y)
	{
		IsDead();
	}
	MoveBodyFrame(3.f);
	
	if (m_bIsDead)
	{
		CSoundMgr::GetInstance()->PlaySoundw(L"MookSplat.wav", CSoundMgr::SUICIDEMOOK); 

		int iRand = GetRandomNumber(1, 2);
		switch (iRand)
		{
		case 1:
			CSoundMgr::GetInstance()->PlaySoundw(L"Explosion1.wav", CSoundMgr::EFFECT);
			break;
		case 2:
			CSoundMgr::GetInstance()->PlaySoundw(L"Explosion2.wav", CSoundMgr::EFFECT);
			break;
		}
		
		CObj* pEffect = CEffectFactory<CAnimEffect, CEffect>::CreateEffect(m_tObj.vPos , L"GrenadeEffect",
			L"BrommandoGrenadeExplosion", { 0.f, 30.f }, 3.f, EFFECT_COLLISION);
		GET_INSTANCE(CObjMgr)->AddObject(pEffect, CObjMgr::EFFECT);
		CCollisionMgr::BodyParticle(*this, 70, 110, 5, 8);
		CCollisionMgr::BloodParticle(*this, 60, 130, 6, 9);
		CCollisionMgr::BloodParticle(*this, 70, 110, 8, 10);

		return DEAD_OBJ;
	}


	StateSet();
	SceneBodyChange();

	SuicideMookMove();
	D3DXMATRIX matScale,matRotz , matTrans;
	D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

	D3DXMatrixScaling(&matScale, m_tObj.vSize.x, m_tObj.vSize.y, m_tObj.vSize.z);
	D3DXMatrixRotationZ(&matRotz, D3DXToRadian(0.f));
	D3DXMatrixTranslation(&matTrans, m_tObj.vPos.x - vScroll.x, m_tObj.vPos.y - vScroll.y, m_tObj.vPos.z);

	m_tObj.matWorld = matScale *matRotz* matTrans;

	if (m_bIsDetect&&!m_bIsFaint)
	{
		D3DXVec3TransformNormal(&m_tObj.vDir, &m_tObj.vLook, &m_tObj.matWorld);
		m_tObj.vPos += m_fSpeed*m_tObj.vDir;
	}


	return NO_EVENT;
}

void CSuicideMook::LateUpdate()
{
	if (m_tObj.vPos.y > g_StageY)//�ٴڿ� ������ �׾��
	{
		IsDead();
	}
}

void CSuicideMook::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(m_wstrObjKey,
		m_wstrBodyStateKey, int(m_tBodyFrame.fFrame));
	NULL_CHECK(m_pTexInfo);
	UpdateRect();


	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.8f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tObj.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CSuicideMook::Release()
{
}

void CSuicideMook::MoveBodyFrame(float fSpeed)
{
	m_tBodyFrame.fFrame += m_tBodyFrame.fMaxCount * fSpeed* GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tBodyFrame.fFrame > m_tBodyFrame.fMaxCount)
	{
		if (!m_bIsHit)
			m_tBodyFrame.fFrame = 0.f;
		else
			IsDead();
	}
}

void CSuicideMook::StateSet()
{
	if (m_bIsGround)
	{
		if (m_bIsFaint)
		{
			m_bCurState = SUICIDEMOOKSTAND;
			m_fFaintTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			if (m_fFaintTime > 3.f)
			{
				m_bIsFaint = false;
				m_fFaintTime = GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			}
		}
		if (m_bIsDetect&&!m_bIsFaint)
		{
			m_bCurState = SUICIDEMOOKIGINTION; 
			if (!m_bSoundOn)
			{
				m_bSoundOn = true;
				CSoundMgr::GetInstance()->PlaySoundw(L"c.wav", CSoundMgr::SUICIDEMOOK);
			}
			m_fIgnitionTime += GET_INSTANCE(CTimeMgr)->GetDeltaTime();
			if (m_fIgnitionTime > 0.4f)
			{
				
				m_bCurState = SUICIDEMOOKRUN;
				if (m_fIgnitionTime > 3.f)
				{
					IsDead();
				}
			}
		}
		else
		{
			m_bCurState = SUICIDEMOOKSTAND;
		}
	}
}

void CSuicideMook::SceneBodyChange()
{
	if (m_bCurState != m_bPreState)
	{
		switch (m_bCurState)
		{

		case SUICIDEMOOKDEAD://7
			m_wstrBodyStateKey = L"Dead";
			m_tBodyFrame = { 0.f, 15.f };
			break;


		case SUICIDEMOOKFALL://10
			m_wstrBodyStateKey = L"Fall";
			m_tBodyFrame = { 0.f, 3.f };
			break;

		case SUICIDEMOOKIGINTION://1
			m_wstrBodyStateKey = L"Ignition";
			m_tBodyFrame = { 0.f, 17.f };
			break;

		case SUICIDEMOOKJUMP://3
			m_wstrBodyStateKey = L"Jump";
			m_tBodyFrame = { 0.f, 3.f };
			break;
		case SUICIDEMOOKRUN://3
			m_wstrBodyStateKey = L"Run";
			m_tBodyFrame = { 0.f, 8.f };
			break;
		case SUICIDEMOOKSTAND ://3
			m_wstrBodyStateKey = L"Stand";
			m_tBodyFrame = { 0.f, 1.f };
			break;
		case SUICIDEMOOKWALK://3
			m_wstrBodyStateKey = L"Walk";
			m_tBodyFrame = { 0.f, 8.f };
			break;

		}
		m_bPreState = m_bCurState;
	}
}

void CSuicideMook::SuicideMookMove()
{
	
	m_tObj.vPos.y += m_fSpeed;

}
