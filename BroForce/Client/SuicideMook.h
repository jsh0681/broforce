#pragma once
#include "Obj.h"

class CSuicideMook :
	public CObj
{
	enum BODYSTATE { SUICIDEMOOKRUN, SUICIDEMOOKJUMP, SUICIDEMOOKFALL , SUICIDEMOOKSTAND, SUICIDEMOOKDEAD, SUICIDEMOOKWALK, SUICIDEMOOKIGINTION};
public:
	CSuicideMook();
	virtual ~CSuicideMook();

	// CObj��(��) ���� ��ӵ�
	virtual HRESULT Initialize() override;
	virtual void LateInit();
	virtual void UpdateRect() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;

public:
	void MoveBodyFrame(float fSpeed);
	void StateSet();
	void SceneBodyChange();
	void SuicideMookMove();
public:
	void SetIsGround(bool bIsGround) { m_bIsGround = bIsGround; }
	void SetIsDetect(bool bIsDetect) { m_bIsDetect = bIsDetect; }
	bool GetIsDetect() { return m_bIsDetect; }
	void SetIsHit(bool bIsHit) { m_bIsHit = bIsHit; }

	void SetIsFaint(bool bIsFaint) { m_bIsFaint = bIsFaint; }
private:
	bool m_bSoundOn;
	float m_fFaintTime;
	bool m_bIsFaint;
	float m_fFrameSpeed;
	float m_fIgnitionTime;
	bool m_bIsHit;//�Ѹ����� 
	bool m_bIsGround;
	bool m_bIsDetect;

private:
	FRAME	m_tBodyFrame;

	wstring m_wstrBodyStateKey = L"";

	BODYSTATE m_bCurState;
	BODYSTATE m_bPreState;

};



