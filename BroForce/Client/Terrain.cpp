#include "stdafx.h"
#include "Terrain.h"
#include "Mouse.h"

CTerrain::CTerrain()
{
}


CTerrain::~CTerrain()
{
	Release();
}

HRESULT CTerrain::Initialize()
{
	if (GET_INSTANCE(CSceneMgr)->GetCurScene() == 1 )
	{
		if (FAILED(LoadTile(L"../Data/StageOne.dat")))
			return E_FAIL;
	}
	if (GET_INSTANCE(CSceneMgr)->GetCurScene() == 2)
	{
		if (FAILED(LoadTile(L"../Data/StageTwo.dat")))
			return E_FAIL;
	}
	if (GET_INSTANCE(CSceneMgr)->GetCurScene() == 3)
	{
		if (FAILED(LoadTile(L"../Data/StageBoss.dat")))
			return E_FAIL;
	}
	return S_OK;
}

void CTerrain::LateInit()
{
	MakeGraph();
}

int CTerrain::Update()
{
	CObj::LateInit();

	return NO_EVENT;
}

void CTerrain::LateUpdate()
{

}

void CTerrain::Render()
{
	D3DXMATRIX matScale, matTrans, matWorld;
	const D3DXVECTOR3 vScroll = CScrollMgr::GetScroll();

	// �ø� ����
	int iCullX = int(vScroll.x) / TILECX;
	int iCullY = int(vScroll.y) / TILECY;
	int iCullEndX = iCullX + WINCX / TILECX;
	int iCullEndY = iCullY + WINCY / TILECY;

	int iIndex = 0;

	for (int i = iCullY; i < iCullEndY + 2; ++i)
	{
		for (int j = iCullX; j < iCullEndX + 2; ++j)
		{
			iIndex = j + (TILEX * i);

			if (0 > iIndex || m_vecTile.size() <= (size_t)iIndex)
				continue;

			D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
			D3DXMatrixTranslation(&matTrans, m_vecTile[iIndex]->vPos.x - vScroll.x
				, m_vecTile[iIndex]->vPos.y - vScroll.y, 0.f);

			matWorld = matScale * matTrans;


			const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
				L"Stage", L"Tile", m_vecTile[iIndex]->byDrawID);
			NULL_CHECK(pTexInfo);

			float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
			float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

			GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
			GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
				&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
		}
	}
}

void CTerrain::Release()
{
	for_each(m_vecTile.begin(), m_vecTile.end(), SafeDelete<TILE*>);
	m_vecTile.clear();
	m_vecTile.shrink_to_fit();
}

HRESULT CTerrain::LoadTile(const wstring & wstrFilePath)
{
	HANDLE hFile = CreateFile(wstrFilePath.c_str(), GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		ERR_MSG(L"LoadTile Failed!");
		return E_FAIL;
	}

	if (!m_vecTile.empty())
		Release();

	DWORD dwByte = 0;
	TILE* pTile = nullptr;

	while (true)
	{
		pTile = new TILE;
		ReadFile(hFile, pTile, sizeof(TILE), &dwByte, nullptr);
		pTile->iParentIndex = 60;
		if (0 == dwByte)
		{
			SafeDelete(pTile);
			break;
		}

		m_vecTile.push_back(pTile);
	}

	CloseHandle(hFile);
	return S_OK;
}

void CTerrain::MakeGraph()
{
	m_vecGraph.resize(m_vecTile.size());

	for (int i = 0; i < TILEY; ++i)
	{
		for (int j = 0; j < TILEX; ++j)
		{
			int iIndex = j + (TILEX * i);
			if ((0 != i))
			{
				if ((0 == m_vecTile[iIndex - TILEX]->byOption))
					m_vecGraph[iIndex].push_back(m_vecTile[iIndex - TILEX]);
			}
			if ((TILEY -1 != i))
			{
				if ((0 == m_vecTile[iIndex + TILEX]->byOption))
					m_vecGraph[iIndex].push_back(m_vecTile[iIndex + TILEX]);
			}
			if ((0 != j))
			{
				if ((0 == m_vecTile[iIndex - 1]->byOption))
					m_vecGraph[iIndex].push_back(m_vecTile[iIndex - 1]);
			}
			if ((TILEX -1 != j))
			{
				if ( (0 == m_vecTile[iIndex + 1]->byOption))
					m_vecGraph[iIndex].push_back(m_vecTile[iIndex + 1]);
			}
		}
	}
}

void CTerrain::UpdateRect()
{
}
