#pragma once

#include "Obj.h"

class CTerrain : public CObj
{
public:
	CTerrain();
	virtual ~CTerrain();

public:
	 vector<TILE*>& GetVecTile()  { return m_vecTile; }
	const vector<list<TILE*>>& GetGraph() const { return m_vecGraph; }
	void SetDamage(int iIndex, int iDamamge) { m_vecTile[iIndex]->iParentIndex -= iDamamge; };

public:
	virtual HRESULT Initialize();
	virtual void LateInit();
	virtual int Update();
	virtual void LateUpdate();
	virtual void Render();
	virtual void Release();
	// CObj을(를) 통해 상속됨
	virtual void UpdateRect() override;

private:
	INFO m_tInfo;
	HRESULT LoadTile(const wstring& wstrFilePath);
	void MakeGraph();
private:
	vector<TILE*>	m_vecTile;
	vector<list<TILE*>>		m_vecGraph;

};

