#pragma once
class CTexture
{
public:
	CTexture();
	virtual ~CTexture();

public:
	virtual const TEXINFO* GetTexInfo(const wstring& wstrStateKey = L"",
		const int& iImgIndex = 0) const PURE;

public:
	virtual HRESULT InsertTexture(const wstring& wstrFilePath,
		const wstring& wstrStateKey = L"",	/* 멀티 텍스쳐일 경우 사용되는 인수 */
		const int& iImgCount = 0 /* 멀티 텍스쳐일 경우 사용되는 인수 */)	PURE;
	virtual void Release() PURE;
};

