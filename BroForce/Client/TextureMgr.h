#pragma once

class CTexture;
class CTextureMgr
{
	DECLARE_SINGLETON(CTextureMgr)

public:
	enum TEX_ID { TEX_SINGLE, TEX_MULTI, TEX_END };

private:
	CTextureMgr();
	~CTextureMgr();

public:
	const TEXINFO* GetTexInfo(const wstring& wstrObjectKey,
		const wstring& wstrStateKey = L"", const int& iImgIndex = 0) const;

public:
	HRESULT ReadImgPath(const wstring& wstrImagePath);
	HRESULT InsertTexture(TEX_ID eTex_ID, const wstring& wstrFilePath,
		const wstring& wstrObjectKey,
		const wstring& wstrStateKey = L"",	/* 멀티 텍스쳐일 경우 사용되는 인수 */
		const int& iImgCount = 0 /* 멀티 텍스쳐일 경우 사용되는 인수 */);
	void Release();


private:
	map<wstring, CTexture*>	m_MapTexture;
};

