#pragma once
class CTimeMgr
{
	DECLARE_SINGLETON(CTimeMgr)

private:
	CTimeMgr();
	~CTimeMgr();

public:
	const float& GetDeltaTime() const { return m_fDeltaTime; }

public:
	void InitTime();
	void UpdateTime();

private:
	LARGE_INTEGER	m_CurTime;
	LARGE_INTEGER	m_OldTime;
	LARGE_INTEGER	m_CpuTick;	// CPU 진동수 (1초 주기로 진동하는 횟수)
	float			m_fDeltaTime;

};

