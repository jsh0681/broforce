#include "stdafx.h"
#include "UI.h"


CUI::CUI()
	: m_pObserver(nullptr)
{
}


CUI::~CUI()
{
	Release();
}

HRESULT CUI::Initialize()
{
	return E_NOTIMPL;
}

void CUI::UpdateRect()
{
}

void CUI::LateInit()
{
	CObj::LateInit();
}

int CUI::Update()
{
	return 0;
}

void CUI::LateUpdate()
{
}

void CUI::Render()
{
}

void CUI::Release()
{
}
