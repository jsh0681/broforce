#pragma once
#include "Obj.h"
class CObserver;

class CUI :
	public CObj
{
public:
	CUI();
	virtual ~CUI();

	// CObj을(를) 통해 상속됨
	virtual HRESULT Initialize() override;
	virtual void UpdateRect() override;
	virtual void LateInit();
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
protected:
	wstring m_wstrStateKey = L"";
	FRAME	m_tUIFrame;
	CObserver*	m_pObserver;
};

