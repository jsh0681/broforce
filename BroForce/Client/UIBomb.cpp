#include "stdafx.h"
#include "UIBomb.h"
#include "UIObserver.h"


CUIBomb::CUIBomb():iBombCount(0)
{
}


CUIBomb::~CUIBomb()
{
	Release();
}

HRESULT CUIBomb::Initialize()
{
	m_tInfo.vPos = { 110.f,555.f,0.f };

	m_pObserver = new CUIObserver;

	GET_INSTANCE(CDataSubject)->Subscribe(m_pObserver);

	m_wstrObjKey = L"Stage";
	m_wstrStateKey = L"UIGrenade";
	return S_OK;
}

void CUIBomb::LateInit()
{
}

int CUIBomb::Update()
{
	CUI::LateInit();	
	D3DXMatrixTranslation(&m_tInfo.matWorld, m_tInfo.vPos.x, m_tInfo.vPos.y, 0.f);

	iBombCount = dynamic_cast<CUIObserver*>(m_pObserver)->GetData().iBombCount;
	return NO_EVENT;
}


void CUIBomb::LateUpdate()
{
}

void CUIBomb::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
		m_wstrObjKey, m_wstrStateKey, iBombCount);
	NULL_CHECK(m_pTexInfo);

	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.5f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CUIBomb::Release()
{
	GET_INSTANCE(CDataSubject)->UnSubscribe(m_pObserver);
	SafeDelete(m_pObserver);
}
