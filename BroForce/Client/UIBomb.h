#pragma once
#include "UI.h"
class CUIBomb :
	public CUI
{
public:
	CUIBomb();
	virtual ~CUIBomb();
public:
	virtual HRESULT Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
private:

	int iBombCount;
};

