#include "stdafx.h"
#include "UIFace.h"
#include "UIObserver.h"


CUIFace::CUIFace():m_iFaceNumber(0), m_iPlayerNumber(0), m_iMoveFace(10)
{
}


CUIFace::~CUIFace()
{
	Release();
}

HRESULT CUIFace::Initialize()
{
	m_tInfo.vPos = { 32.f,536.f,0.f };

	m_pObserver = new CUIObserver;
	GET_INSTANCE(CDataSubject)->Subscribe(m_pObserver);

	m_wstrObjKey = L"UIFace";
	m_wstrStateKey = L"RamBroFace";
	m_tUIFrame = { 0.f,4.f };

	m_fCurState = RAMBROFACE;
	m_fPreState = m_fCurState;

	return S_OK;
}

void CUIFace::LateInit()
{
	
}

int CUIFace::Update()
{
	CUI::LateInit(); 

	m_fCurState = (FACESTATE)dynamic_cast<CUIObserver*>(m_pObserver)->GetData().iPlayerNumber;
	m_iFaceNumber = dynamic_cast<CUIObserver*>(m_pObserver)->GetData().iFaceNumber;
	StateChange(); 
	
	D3DXMatrixTranslation(&m_tInfo.matWorld, m_tInfo.vPos.x, m_tInfo.vPos.y , 0.f);



	return NO_EVENT;
}
void CUIFace::MoveFrame(float fSpeed)
{
	m_tUIFrame.fFrame += m_tUIFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tUIFrame.fFrame > m_tUIFrame.fMaxCount)
	{
		m_tUIFrame.fFrame = 0.f;
	}
}
void CUIFace::LateUpdate()
{
	MoveFrame(3.f);
}

void CUIFace::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
		m_wstrObjKey, m_wstrStateKey, m_iFaceNumber);
	NULL_CHECK(m_pTexInfo);

	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.5f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CUIFace::Release()
{
	GET_INSTANCE(CDataSubject)->UnSubscribe(m_pObserver);
	SafeDelete(m_pObserver);
}

void CUIFace::StateChange()
{
	if (m_fCurState != m_fPreState)
	{
		switch (m_fCurState)
		{
		case RAMBROFACE:
			m_wstrStateKey = L"RamBroFace";
			m_tUIFrame = { 0.f,4.f };
			break;
		case BROHARDFACE:	
			m_wstrStateKey = L"BroHardFace";
			m_tUIFrame = { 0.f,4.f };
			break;
		case CHUCKBROFACE:	
			m_wstrStateKey = L"ChuckBroFace";
			m_tUIFrame = { 0.f,4.f };
			break;
		case BROMMANDOFACE:	
			m_wstrStateKey = L"BrommandoFace";
			m_tUIFrame = { 0.f,4.f };
			break;
		case BRODREDDFACE:
			m_wstrStateKey = L"BroDreddFace";
			m_tUIFrame = { 0.f,4.f };
			break;

		}
		m_fPreState = m_fCurState;
	}
}
