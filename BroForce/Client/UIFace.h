#pragma once
#include "UI.h"
class CUIFace :
	public CUI
{
public:
	enum FACESTATE { RAMBROFACE, BROHARDFACE, CHUCKBROFACE, BROMMANDOFACE ,BRODREDDFACE};
public:
	CUIFace();
	virtual ~CUIFace();
public:
	virtual HRESULT Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	void MoveFrame(float fSpeed);
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
	void StateChange();
private:
	int m_iMoveFace;
	FACESTATE m_fCurState;
	FACESTATE m_fPreState;
	int m_iPlayerNumber;
	int	m_iFaceNumber;
};

