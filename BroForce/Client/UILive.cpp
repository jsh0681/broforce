#include "stdafx.h"
#include "UILive.h"
#include "UIObserver.h"


CUILive::CUILive():m_iLife(0)
{
}


CUILive::~CUILive()
{
	Release();
}

HRESULT CUILive::Initialize()
{
	m_tInfo.vPos = { 120.f,580.f,0.f };

	m_pObserver = new CUIObserver;

	GET_INSTANCE(CDataSubject)->Subscribe(m_pObserver);

	m_wstrObjKey = L"Stage";
	m_wstrStateKey = L"UILives";

	return S_OK;
}

void CUILive::LateInit()
{
}

int CUILive::Update()
{
	CUI::LateInit();
	D3DXMatrixTranslation(&m_tInfo.matWorld, m_tInfo.vPos.x, m_tInfo.vPos.y, 0.f);

	m_iLife = dynamic_cast<CUIObserver*>(m_pObserver)->GetData().iLife;
	return NO_EVENT;
}


void CUILive::LateUpdate()
{
}

void CUILive::Render()
{
	m_pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
		m_wstrObjKey, m_wstrStateKey, m_iLife);
	NULL_CHECK(m_pTexInfo);

	float fCenterX = m_pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = m_pTexInfo->tImgInfo.Height * 0.5f;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&m_tInfo.matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(m_pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
}

void CUILive::Release()
{
	GET_INSTANCE(CDataSubject)->UnSubscribe(m_pObserver);
	SafeDelete(m_pObserver);
}
