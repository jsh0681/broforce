#pragma once
#include "UI.h"
class CUILive :
	public CUI
{
public:
	CUILive();
	virtual ~CUILive();
public:
	virtual HRESULT Initialize() override;
	virtual void LateInit() override;
	virtual int Update() override;
	virtual void LateUpdate() override;
	virtual void Render() override;
	virtual void Release() override;
private:

	int	m_iLife;
};

