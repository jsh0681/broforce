#include "stdafx.h"
#include "UIObserver.h"


CUIObserver::CUIObserver()
{
}


CUIObserver::~CUIObserver()
{
	
}

void CUIObserver::Update(int iMessage, void* pData)
{
	// pull observer: 옵저버가 필요한 데이터를 subject에게 얻어오는 방식.
	list<void*>* pDataLst = GET_INSTANCE(CDataSubject)->GetDataLst(iMessage);
	NULL_CHECK(pDataLst);

	// find: <algorithm>에서 제공하는 탐색 알고리즘.
	auto iter_find = find(pDataLst->begin(), pDataLst->end(), pData);

	if (pDataLst->end() == iter_find)
		return;

	switch (iMessage)
	{
	case CDataSubject::PLAYER_DATA:
		m_tData = *reinterpret_cast<DATA*>(*iter_find);
		break;
	}
}
