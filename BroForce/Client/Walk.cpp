#include "stdafx.h"
#include "Walk.h"


CWalk::CWalk()
{
	m_tFrame.m_wstrObjKey = L"BroHard";
	m_tFrame.m_wstrStateKey = L"Walk";
	m_tFrame.m_fFrameSpeed = 5.f;
	m_tFrame.fFrame = 0.f;
	m_tFrame.fMaxCount = 8.f;
}


CWalk::~CWalk()
{
}

void CWalk::MoveFrame(float fSpeed)
{
	m_tFrame.fFrame += m_tFrame.fMaxCount * fSpeed * GET_INSTANCE(CTimeMgr)->GetDeltaTime();
	if (m_tFrame.fFrame > m_tFrame.fMaxCount)
	{
		m_tFrame.fFrame = 0.f;
	}
}
