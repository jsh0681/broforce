#pragma once
#include "Motion.h"
class CWalk :
	public CMotion
{
public:
	CWalk();
	virtual ~CWalk();

	// CMotion을(를) 통해 상속됨
	virtual void MoveFrame(float fSpeed) override;
};

