// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
// Windows 헤더 파일:
#include <windows.h>

// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

// Standard Headers
#include <list>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include <fstream>
#include<iostream>
using namespace std;
#include<time.h>
#include<math.h>
// Libraries
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")


// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
// User Headers
#include <random>
#include "Define.h"
#include "Struct.h"
#include "Typedef.h"
#include "Extern.h"
#include "Function.h"
#include "Constant.h"
#include <stdlib.h>
#include<process.h>
#include <io.h>


// FMOD
#include "fmod.h"
#pragma comment(lib, "fmodex_vc.lib")

// vld
#include "vld.h"
#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

// 멀티미디어 재생에 필요한 헤더와 라이브러리
#include <Vfw.h>
#pragma comment (lib, "vfw32.lib")

// Managers
#include <vld.h>
#include "AbstractFactory.h"
#include "ObjMgr.h"
#include "TimeMgr.h"
#include "MathMgr.h"
#include "ScrollMgr.h"
#include "DeviceMgr.h"
#include "TextureMgr.h"
#include"CollisionMgr.h"
#include "DataSubject.h"
#include "SceneMgr.h"
#include "AstarMgr.h"
#include"KeyMgr.h"
#include"SoundMgr.h"



