#pragma once
#define MIN_STR 64
#define MID_STR 128
#define MAX_STR 256

#define NO_EVENT 0
#define DEAD_OBJ 1

#define WINCX 800
#define WINCY 600

#define TILEX 84
#define TILEY 80

#define TILECX 32
#define TILECY 32

#define RUBY     0x01
#define SAPPHIRE 0x02
#define DIAMOND	 0x04

#define SCALEX 1.f
#define SCALEY 1.f

#define GRAVITY 9.81f

#define TILE_BACK 0
#define TILE_BOOM 1
#define TILE_GAS 3
#define TILE_RADDER 4
#define TILE_FLAG 10
#define TILE_BRIDGE 2

#define RADDER_SPEED 4.f
#define HANG_DOWN 1.5f

#define SHOOT_HAND_LEFT 0
#define SHOOT_HAND_RIGHT 1

#define STAGE_ONE_X 1870.f
#define STAGE_ONE_Y 1058.f


#define STAGE_TWO_X 2351.f
#define STAGE_TWO_Y 1058.f

#define STAGE_BOSS_X 2351.f
#define STAGE_BOSS_Y 2116.f


#define EFFECT_COLLISION 0
#define EFFECT_NONCOLLISION 1
#define EFFECT_MOVING 2


#define RAMBRO_BULLET 0 
#define BROHARD_BULLET 1 
#define CHUCKBRO_BULLET 2 
#define BROMMANDO_BULLET 3 
#define TANK_MISSILE 4 
#define BOSS_BULLET 5 
#define BOSS_BOMB 6 
#define BRODREDD_BULLET 7 

#define RAMBRO_GRENADE 0 
#define BROHARD_GRENADE 1 
#define CHUCKBRO_GRENADE 2 
#define BROMMANDO_GRENADE 3 
#define BRODREDD_GRENADE 4 
 
#define BRO_BULLET 1
#define MOOK_BULLET 2


#define NULL_CHECK(PTR) if(nullptr == PTR) return;
#define NULL_CHECK_RETURN(PTR, RETURN_VAL) if(nullptr == PTR) return RETURN_VAL;


#define ERR_MSG(message)						\
MessageBox(g_hWnd, message, L"Error!", MB_OK);

#define NO_COPY(ClassName)						\
private:										\
	ClassName(const ClassName& _Obj);			\
	ClassName& operator=(const ClassName& _Obj);

#define DECLARE_SINGLETON(ClassName)			\
		NO_COPY(ClassName)						\
public:											\
	static ClassName* GetInstance()				\
	{											\
		if(nullptr == m_pInstance)				\
			m_pInstance = new ClassName;		\
		return m_pInstance;						\
	}											\
	void DestroyInstance()						\
	{											\
		if(m_pInstance)							\
		{										\
			delete m_pInstance;					\
			m_pInstance = nullptr;				\
		}										\
	}											\
private:										\
	static ClassName*	m_pInstance;			

#define IMPLEMENT_SINGLETON(ClassName)			\
ClassName* ClassName::m_pInstance = nullptr;

#define GET_INSTANCE(ClassName)					\
ClassName::GetInstance()



