#pragma once

template <typename T>
void SafeDelete(T& ptr)
{
	if (ptr)
	{
		delete ptr;
		ptr = nullptr;
	}
}

template <typename T>
T GetRandomNumber(T min, T max)
{
	random_device rd;
	mt19937_64 rnd(rd());
	uniform_int_distribution<T> range(min, max);
	return range(rnd);
}