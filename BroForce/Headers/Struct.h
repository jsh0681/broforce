#pragma once

typedef struct tagInfo
{
	D3DXVECTOR3	vPos;	// 위치벡터
	D3DXVECTOR3 vSize;
	D3DXVECTOR3 vDir;	// 방향벡터
	D3DXVECTOR3 vLook;	// 가상의 기준 축
	D3DXMATRIX	matWorld;
}INFO;


typedef struct tagObject
{
	D3DXVECTOR3	vPos;	// 위치벡터
	D3DXVECTOR3 vSize;
	D3DXVECTOR3 vDir;	// 방향벡터
	D3DXVECTOR3 vLook;	// 가상의 기준 축
	D3DXMATRIX	matWorld;
	BYTE		byDrawID;
	BYTE		byOption;
}OBJ;


typedef struct tagFrame
{
	float	fFrame;
	float	fMaxCount;
}FRAME;//8바이트



typedef struct tagFrame2
{
	wstring m_wstrObjKey;
	wstring m_wstrStateKey;
	float m_fFrameSpeed;
	float	fFrame;
	float	fMaxCount;
}FRAME2;

typedef struct tagTexture
{
	LPDIRECT3DTEXTURE9	pTexture;	// 이미지 한장을 제어할 Com객체
	D3DXIMAGE_INFO		tImgInfo;	// 이미지 한장 정보.
}TEXINFO;

typedef struct tagTile
{
	D3DXVECTOR3 vPos;
	D3DXVECTOR3 vSize;
	BYTE		byDrawID;
	BYTE		byOption;

	int			iIndex = 0;
	int			iParentIndex = 0;
}TILE;//40







typedef struct tagUnit
{
#ifdef _AFX
	CString strName = L"";
#else
	wstring wstrName = L"";
#endif

	int		iAtt = 0;
	int		iDef = 0;
	BYTE	byJobIndex = 0;
	BYTE	byItem = 0;
}UNITDATA;//12

typedef struct tagImagePath
{
	wstring wstrObjKey = L"";
	wstring wstrStateKey = L"";
	int		iCount = 0;
	wstring	wstrPath = L"";
}IMGPATH;

typedef struct tagWeapon
{
	float fDamage;
	float fCrossroad;
	float fExplosionRange;
	float fImageSizeX;
	float fImageSizeY;

}WEAPON;//20


typedef struct tagData
{
	int iPlayerNumber;
	int iFaceNumber;
	int iLife;
	int iBombCount;
}DATA;//16