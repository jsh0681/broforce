#pragma once
class CDeviceMgr
{
	DECLARE_SINGLETON(CDeviceMgr)

private:
	CDeviceMgr();
	~CDeviceMgr();

public:
	LPDIRECT3DDEVICE9 GetDevice() { return m_pDevice; }
	LPD3DXSPRITE GetSprite() { return m_pSprite; }
	LPD3DXFONT GetFont() { return m_pFont; }

public:
	HRESULT InitDevice();
	void Render_Begin();
	void Render_End(HWND hWnd = nullptr);
	void Release();

private:
	void SetParameters(D3DPRESENT_PARAMETERS& d3dpp);

private:
	// COM(Component Object Model) 객체

	// DirectX에서 여러장치를 제어하기 위한 총괄 인터페이스
	// 장치 검증과 LPDIRECT3DDEVICE9객체를 생성하는 용도로 활용한다.
	LPDIRECT3D9			m_p3D;

	// DirectX에서 그래픽 장치를 제어하기 위한 인터페이스
	LPDIRECT3DDEVICE9	m_pDevice;

	// DirectX에서 그래픽 장치를 이용하여 2D 이미지를 출력해주는 Com객체
	LPD3DXSPRITE		m_pSprite;

	// DirectX에서 그래픽 장치를 이용하여 폰트 출력해주는 Com객체
	LPD3DXFONT			m_pFont;
};

