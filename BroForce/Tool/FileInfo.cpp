#include "stdafx.h"
#include "FileInfo.h"


CFileInfo::CFileInfo()
{
}


CFileInfo::~CFileInfo()
{
}

CString CFileInfo::ConvertRelativePath(CString strFullPath)
{
	TCHAR szCurrentPath[MAX_STR] = L"";
	TCHAR szRelativePath[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szCurrentPath);

	// PathRelativePathTo(C, A, FILE_ATTRIBUTE_DIRECTORY, B, FILE_ATTRIBUTE_DIRECTORY);
	// A에서 B로 찾아가는 상대경로를 만든 뒤 C에 저장.
	// 단, A와 B는 같은 드라이브 안에 있어야 한다.
	PathRelativePathTo(szRelativePath, szCurrentPath, FILE_ATTRIBUTE_DIRECTORY,
		strFullPath, FILE_ATTRIBUTE_DIRECTORY);

	return CString(szRelativePath);
}

void CFileInfo::DirInfoExtraction(const wstring& wstrPath, list<IMGPATH*>& ImgPathLst)
{
	// MFC에서 제공하는 파일 탐색 및 제어 클래스.
	CFileFind find;

	// wstrPath = L"D:\\김태윤\\4개월차\\90기\\Framework90\\Texture\\Stage\\Effect"
	wstring wstrFindPath = wstrPath + L"\\*.*";

	// wstrFindPath = L"D:\\김태윤\\4개월차\\90기\\Framework90\\Texture\\Stage\\
	\*.*
	find.FindFile(wstrFindPath.c_str());

	int iContinue = 1;

	while (iContinue)
	{
		iContinue = find.FindNextFile();

		if (find.IsDots())	// 찾은 것이 .이나 ..같은 마커라면 건너뜀.
			continue;
		else if (find.IsDirectory())	// 찾은 것이 폴더라면 재귀 호출
		{
			// wstrFindPath = L"D:\\김태윤\\4개월차\\90기\\Framework90\\Texture\\Stage\\Effect\\BossMultiAttack
			DirInfoExtraction(find.GetFilePath().GetString(), ImgPathLst);
		}
		else // 파일을 찾음.
		{
			if (find.IsSystem())	// 찾은 것이 시스템 파일이라면 건너뜀.
				continue;

			TCHAR szCurrentFind[MAX_STR] = L"";

			// GetFilePath: 현재 찾아낸 경로를 뱉어냄.
			lstrcpy(szCurrentFind, find.GetFilePath().GetString());
			// find.GetFilePath() = // L"D:\\김태윤\\4개월차\\90기\\Framework90\\Texture\\Stage\\Effect\\BossMultiAttack\\BossMultiAttack0.png

			PathRemoveFileSpec(szCurrentFind);
			// szCurrentFind = // L"D:\\김태윤\\4개월차\\90기\\Framework90\\Texture\\Stage\\Effect\\BossMultiAttack

			IMGPATH* pImgPath = new IMGPATH;
			pImgPath->iCount = DirFileCount(szCurrentFind);

			///////////////////////////////////////////////////////////////////////////////////////////

			// GetFileTitle: 현재 경로로부터 파일명만 얻어옴.
			// wstrTextureName = L"BossMultiAttack0"
			wstring wstrTextureName = find.GetFileTitle().GetString();

			// substr(시작, 개수): 현재 문자열의 시작점부터 개수만큼 얻어옴.
			// wstrTextureName = L"BossMultiAttack"
			wstrTextureName = wstrTextureName.substr(0, wstrTextureName.length() - 1);

			// wstrTextureName =  L"BossMultiAttack%d.png"
			wstrTextureName += L"%d.png";

			// PathCombine(a, b ,c): b와 c를 결합한 최종 문자열(경로)을 a에 저장.
			// b와 c 사이에 \\가 자동으로 삽입된다.
			// szCurrentFind = L"D:\\김태윤\\4개월차\\90기\\Framework90\\Texture\\Stage\\Effect\\BossMultiAttack\\BossMultiAttack%d.png
			PathCombine(szCurrentFind, szCurrentFind, wstrTextureName.c_str());

			// ..\\Texture\\Stage\\Effect\\BossMultiAttack\\BossMultiAttack%d.png
			pImgPath->wstrPath = ConvertRelativePath(szCurrentFind);

			//////////////////////////////////////////////////////////////////////////////////////

			// szCurrentFind = D:\\김태윤\\4개월차\\90기\\Framework90\\Texture\\Stage\\Effect\\BossMultiAttack
			PathRemoveFileSpec(szCurrentFind);

			// PathFindFileName: 경로 상에서 파일 이름을 찾아내어 반환.
			// 만약, 파일 이름이 존재하지 않으면 말단 폴더명을 찾아준다.
			pImgPath->wstrStateKey = PathFindFileName(szCurrentFind);

			//////////////////////////////////////////////////////////////////////////////////////

			// szCurrentFind = D:\\김태윤\\4개월차\\90기\\Framework90\\Texture\\Stage\\Effect
			PathRemoveFileSpec(szCurrentFind);

			// PathFindFileName: 경로 상에서 파일 이름을 찾아내어 반환.
			// 만약, 파일 이름이 존재하지 않으면 말단 폴더명을 찾아준다.
			pImgPath->wstrObjKey = PathFindFileName(szCurrentFind);

			ImgPathLst.push_back(pImgPath);
			break;
		}

	}
}

int CFileInfo::DirFileCount(const wstring & wstrPath)
{
	CFileFind find;
	wstring wstrFindPath = wstrPath + L"\\*.*";
	find.FindFile(wstrFindPath.c_str());

	int iCount = 0;
	int iContinue = 1;

	while (iContinue)
	{
		iContinue = find.FindNextFile();

		if (find.IsDots())	// 찾은 것이 .이나 ..같은 마커라면 건너뜀.
			continue;
		else if (find.IsDirectory())	// 찾은 것이 폴더라면 건너뜀
			continue;
		else // 파일을 찾음.
		{
			if (find.IsSystem())	// 찾은 것이 시스템 파일이라면 건너뜀.
				continue;

			++iCount;
		}
	}

	return iCount;
}
