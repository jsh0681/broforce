// MapTool.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tool.h"
#include "MapTool.h"
#include "afxdialogex.h"
#include "ToolView.h"
#include "Miniview.h"
#include"MainFrm.h"
#include"FileInfo.h"
#include"Terrain.h"



IMPLEMENT_DYNAMIC(CMapTool, CDialog)

CMapTool::CMapTool(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_MAPTOOL, pParent)
	, m_strFindName(_T(""))
	, m_byOption(0)
{

}

CMapTool::~CMapTool()
{
	for_each(m_MapImage.begin(), m_MapImage.end(),
		[](auto& MyPair)
	{
		SafeDelete(MyPair.second);
	});
	m_MapImage.clear();
}

void CMapTool::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ListBox);
	DDX_Control(pDX, IDC_PICTURE, m_PictureCtrl[0]);
	DDX_Control(pDX, IDC_PICTURE2, m_PictureCtrl[1]);
	DDX_Control(pDX, IDC_PICTURE3, m_PictureCtrl[2]);
	DDX_Control(pDX, IDC_PICTURE4, m_PictureCtrl[3]);
	DDX_Control(pDX, IDC_PICTURE5, m_PictureCtrl[4]);
	DDX_Control(pDX, IDC_PICTURE6, m_PictureCtrl[5]);
	DDX_Control(pDX, IDC_PICTURE7, m_PictureCtrl[6]);
	DDX_Control(pDX, IDC_PICTURE8, m_PictureCtrl[7]);
	DDX_Control(pDX, IDC_PICTURE9, m_PictureCtrl[8]);
	DDX_Control(pDX, IDC_PICTURE10, m_PictureCtrl[9]);
	DDX_Control(pDX, IDC_PICTURE11, m_PictureCtrl[10]);
	DDX_Control(pDX, IDC_PICTURE12, m_PictureCtrl[11]);
	DDX_Control(pDX, IDC_PICTURE13, m_PictureCtrl[12]);
	DDX_Control(pDX, IDC_PICTURE14, m_PictureCtrl[13]);
	DDX_Control(pDX, IDC_PICTURE15, m_PictureCtrl[14]);
	DDX_Control(pDX, IDC_PICTURE16, m_PictureCtrl[15]);
	DDX_Control(pDX, IDC_PICTURE17, m_PictureCtrl[16]);
	DDX_Control(pDX, IDC_PICTURE18, m_PictureCtrl[17]);
	DDX_Control(pDX, IDC_PICTURE19, m_PictureCtrl[18]);
	DDX_Control(pDX, IDC_PICTURE20, m_PictureCtrl[19]);
	DDX_Control(pDX, IDC_PICTURE21, m_PictureCtrl[20]);
	DDX_Control(pDX, IDC_PICTURE22, m_PictureCtrl[21]);
	DDX_Control(pDX, IDC_PICTURE23, m_PictureCtrl[22]);
	DDX_Control(pDX, IDC_PICTURE24, m_PictureCtrl[23]);
	DDX_Control(pDX, IDC_PICTURE25, m_PictureCtrl[24]);
	DDX_Control(pDX, IDC_PICTURE26, m_PictureCtrl[25]);
	DDX_Control(pDX, IDC_PICTURE27, m_PictureCtrl[26]);
	DDX_Control(pDX, IDC_PICTURE28, m_PictureCtrl[27]);
	DDX_Control(pDX, IDC_PICTURE29, m_PictureCtrl[28]);
	DDX_Control(pDX, IDC_PICTURE30, m_PictureCtrl[29]);
	DDX_Control(pDX, IDC_PICTURE31, m_PictureCtrl[30]);
	DDX_Control(pDX, IDC_PICTURE32, m_PictureCtrl[31]);
	DDX_Control(pDX, IDC_PICTURE33, m_PictureCtrl[32]);
	DDX_Control(pDX, IDC_PICTURE34, m_PictureCtrl[33]);
	DDX_Control(pDX, IDC_PICTURE35, m_PictureCtrl[34]);
	DDX_Control(pDX, IDC_PICTURE36, m_PictureCtrl[35]);
	DDX_Control(pDX, IDC_PICTURE37, m_PictureCtrl[36]);
	DDX_Control(pDX, IDC_PICTURE38, m_PictureCtrl[37]);
	DDX_Control(pDX, IDC_PICTURE39, m_PictureCtrl[38]);
	DDX_Control(pDX, IDC_PICTURE40, m_PictureCtrl[39]);
	DDX_Control(pDX, IDC_PICTURE41, m_PictureCtrl[40]);
	DDX_Control(pDX, IDC_PICTURE42, m_PictureCtrl[41]);
	DDX_Control(pDX, IDC_PICTURE43, m_PictureCtrl[42]);
	DDX_Control(pDX, IDC_PICTURE44, m_PictureCtrl[43]);
	DDX_Control(pDX, IDC_PICTURE45, m_PictureCtrl[44]);
	DDX_Control(pDX, IDC_PICTURE46, m_PictureCtrl[45]);
	DDX_Control(pDX, IDC_PICTURE47, m_PictureCtrl[46]);
	DDX_Control(pDX, IDC_PICTURE48, m_PictureCtrl[47]);
	DDX_Control(pDX, IDC_PICTURE49, m_PictureCtrl[48]);
	DDX_Control(pDX, IDC_PICTURE50, m_PictureCtrl[49]);
	DDX_Control(pDX, IDC_PICTURE51, m_PictureCtrl[50]);
	DDX_Control(pDX, IDC_PICTURE52, m_PictureCtrl[51]);
	DDX_Control(pDX, IDC_PICTURE53, m_PictureCtrl[52]);
	DDX_Control(pDX, IDC_PICTURE54, m_PictureCtrl[53]);
	DDX_Control(pDX, IDC_PICTURE55, m_PictureCtrl[54]);
	DDX_Control(pDX, IDC_PICTURE56, m_PictureCtrl[55]);
	DDX_Control(pDX, IDC_PICTURE57, m_PictureCtrl[56]);
	DDX_Control(pDX, IDC_PICTURE58, m_PictureCtrl[57]);
	DDX_Control(pDX, IDC_PICTURE59, m_PictureCtrl[58]);
	DDX_Control(pDX, IDC_PICTURE60, m_PictureCtrl[59]);
	DDX_Control(pDX, IDC_PICTURE61, m_PictureCtrl[60]);
	DDX_Control(pDX, IDC_PICTURE62, m_PictureCtrl[61]);
	DDX_Control(pDX, IDC_PICTURE63, m_PictureCtrl[62]);
	DDX_Control(pDX, IDC_PICTURE64, m_PictureCtrl[63]);
	DDX_Control(pDX, IDC_PICTURE65, m_PictureCtrl[64]);


	DDX_Text(pDX, IDC_EDIT1, m_strFindName);
	DDX_Text(pDX, IDC_EDIT7, m_byOption);
}


BEGIN_MESSAGE_MAP(CMapTool, CDialog)
	ON_WM_DROPFILES()
	ON_LBN_SELCHANGE(IDC_LIST1, &CMapTool::OnLbnSelchangeList)
	ON_BN_CLICKED(IDC_BUTTON1, &CMapTool::OnBnClickedSave)
	ON_BN_CLICKED(IDC_BUTTON2, &CMapTool::OnBnClickedLoad)

	ON_BN_CLICKED(IDC_BUTTON3, &CMapTool::OnBnClickedDelete)
	ON_EN_CHANGE(IDC_EDIT1, &CMapTool::OnEnChangeFindName)
	ON_BN_CLICKED(IDC_BUTTON4, &CMapTool::OnBnClickedTileAdd)
	ON_BN_CLICKED(IDC_BUTTON6, &CMapTool::OnBnClickedChange)

	ON_STN_CLICKED(IDC_PICTURE, &CMapTool::OnStnClickedTile)
	ON_STN_CLICKED(IDC_PICTURE2, &CMapTool::OnStnClickedTile2)
	ON_STN_CLICKED(IDC_PICTURE3, &CMapTool::OnStnClickedTile3)
	ON_STN_CLICKED(IDC_PICTURE4, &CMapTool::OnStnClickedTile4)
	ON_STN_CLICKED(IDC_PICTURE5, &CMapTool::OnStnClickedTile5)
	ON_STN_CLICKED(IDC_PICTURE6, &CMapTool::OnStnClickedTile6)
	ON_STN_CLICKED(IDC_PICTURE7, &CMapTool::OnStnClickedTile7)
	ON_STN_CLICKED(IDC_PICTURE8, &CMapTool::OnStnClickedTile8)
	ON_STN_CLICKED(IDC_PICTURE9, &CMapTool::OnStnClickedTile9)
	ON_STN_CLICKED(IDC_PICTURE10, &CMapTool::OnStnClickedTile10)
	ON_STN_CLICKED(IDC_PICTURE11, &CMapTool::OnStnClickedTile11)
	ON_STN_CLICKED(IDC_PICTURE12, &CMapTool::OnStnClickedTile12)
	ON_STN_CLICKED(IDC_PICTURE13, &CMapTool::OnStnClickedTile13)
	ON_STN_CLICKED(IDC_PICTURE14, &CMapTool::OnStnClickedTile14)
	ON_STN_CLICKED(IDC_PICTURE15, &CMapTool::OnStnClickedTile15)
	ON_STN_CLICKED(IDC_PICTURE16, &CMapTool::OnStnClickedTile16)
	ON_STN_CLICKED(IDC_PICTURE17, &CMapTool::OnStnClickedTile17)
	ON_STN_CLICKED(IDC_PICTURE18, &CMapTool::OnStnClickedTile18)
	ON_STN_CLICKED(IDC_PICTURE19, &CMapTool::OnStnClickedTile19)
	ON_STN_CLICKED(IDC_PICTURE20, &CMapTool::OnStnClickedTile20)
	ON_STN_CLICKED(IDC_PICTURE21, &CMapTool::OnStnClickedTile21)
	ON_STN_CLICKED(IDC_PICTURE22, &CMapTool::OnStnClickedTile22)
	ON_STN_CLICKED(IDC_PICTURE23, &CMapTool::OnStnClickedTile23)
	ON_STN_CLICKED(IDC_PICTURE24, &CMapTool::OnStnClickedTile24)
	ON_STN_CLICKED(IDC_PICTURE25, &CMapTool::OnStnClickedTile25)
	ON_STN_CLICKED(IDC_PICTURE26, &CMapTool::OnStnClickedTile26)
	ON_STN_CLICKED(IDC_PICTURE27, &CMapTool::OnStnClickedTile27)
	ON_STN_CLICKED(IDC_PICTURE28, &CMapTool::OnStnClickedTile28)
	ON_STN_CLICKED(IDC_PICTURE29, &CMapTool::OnStnClickedTile29)
	ON_STN_CLICKED(IDC_PICTURE30, &CMapTool::OnStnClickedTile30)
	ON_STN_CLICKED(IDC_PICTURE31, &CMapTool::OnStnClickedTile31)
	ON_STN_CLICKED(IDC_PICTURE32, &CMapTool::OnStnClickedTile32)
	ON_STN_CLICKED(IDC_PICTURE33, &CMapTool::OnStnClickedTile33)
	ON_STN_CLICKED(IDC_PICTURE34, &CMapTool::OnStnClickedTile34)
	ON_STN_CLICKED(IDC_PICTURE35, &CMapTool::OnStnClickedTile35)
	ON_STN_CLICKED(IDC_PICTURE36, &CMapTool::OnStnClickedTile36)
	ON_STN_CLICKED(IDC_PICTURE37, &CMapTool::OnStnClickedTile37)
	ON_STN_CLICKED(IDC_PICTURE38, &CMapTool::OnStnClickedTile38)
	ON_STN_CLICKED(IDC_PICTURE39, &CMapTool::OnStnClickedTile39)
	ON_STN_CLICKED(IDC_PICTURE40, &CMapTool::OnStnClickedTile40)
	ON_STN_CLICKED(IDC_PICTURE41, &CMapTool::OnStnClickedTile41)
	ON_STN_CLICKED(IDC_PICTURE42, &CMapTool::OnStnClickedTile42)
	ON_STN_CLICKED(IDC_PICTURE43, &CMapTool::OnStnClickedTile43)
	ON_STN_CLICKED(IDC_PICTURE44, &CMapTool::OnStnClickedTile44)
	ON_STN_CLICKED(IDC_PICTURE45, &CMapTool::OnStnClickedTile45)
	ON_STN_CLICKED(IDC_PICTURE46, &CMapTool::OnStnClickedTile46)
	ON_STN_CLICKED(IDC_PICTURE47, &CMapTool::OnStnClickedTile47)
	ON_STN_CLICKED(IDC_PICTURE48, &CMapTool::OnStnClickedTile48)
	ON_STN_CLICKED(IDC_PICTURE49, &CMapTool::OnStnClickedTile49)
	ON_STN_CLICKED(IDC_PICTURE50, &CMapTool::OnStnClickedTile50)
	ON_STN_CLICKED(IDC_PICTURE51, &CMapTool::OnStnClickedTile51)
	ON_STN_CLICKED(IDC_PICTURE52, &CMapTool::OnStnClickedTile52)
	ON_STN_CLICKED(IDC_PICTURE53, &CMapTool::OnStnClickedTile53)
	ON_STN_CLICKED(IDC_PICTURE54, &CMapTool::OnStnClickedTile54)
	ON_STN_CLICKED(IDC_PICTURE55, &CMapTool::OnStnClickedTile55)
	ON_STN_CLICKED(IDC_PICTURE56, &CMapTool::OnStnClickedTile56)
	ON_STN_CLICKED(IDC_PICTURE57, &CMapTool::OnStnClickedTile57)
	ON_STN_CLICKED(IDC_PICTURE58, &CMapTool::OnStnClickedTile58)
	ON_STN_CLICKED(IDC_PICTURE59, &CMapTool::OnStnClickedTile59)
	ON_STN_CLICKED(IDC_PICTURE60, &CMapTool::OnStnClickedTile60)
	ON_STN_CLICKED(IDC_PICTURE61, &CMapTool::OnStnClickedTile61)
	ON_STN_CLICKED(IDC_PICTURE62, &CMapTool::OnStnClickedTile62)
	ON_STN_CLICKED(IDC_PICTURE63, &CMapTool::OnStnClickedTile63)
	ON_STN_CLICKED(IDC_PICTURE64, &CMapTool::OnStnClickedTile64)
	ON_STN_CLICKED(IDC_PICTURE65, &CMapTool::OnStnClickedTile65)
END_MESSAGE_MAP()


// CMapTool 메시지 처리기입니다.

// HDROP hDropInfo: 드롭된 파일들의 정보를 갖는 구조체 핸들.
void CMapTool::OnDropFiles(HDROP hDropInfo)
{
	CDialog::OnDropFiles(hDropInfo);

	CString strRelativePath = L"";
	CString strFileName = L"";

	TCHAR szFilePath[MAX_STR] = L"";
	TCHAR szFileName[MAX_STR] = L"";

	// DragQueryFile: 두번째 인자가 -1일 경우 드래그 앤 드롭된 전체 파일 개수를 반환.
	int iCount = DragQueryFile(hDropInfo, -1, nullptr, 0);

	for (int i = 0; i < iCount; ++i)
	{
		DragQueryFile(hDropInfo, i, szFilePath, MAX_STR);
		//상대경로 변환
		strRelativePath = CFileInfo::ConvertRelativePath(szFilePath);
		//파일명만 출력
		strFileName = PathFindFileName(strRelativePath);
		//확장명 생략
		lstrcpy(szFileName, strFileName.GetString());
		//경로상의 확장자를 제거.
		PathRemoveExtension(szFileName);

		auto& iter_find = m_MapImage.find(szFileName);
		if (iter_find == m_MapImage.end())
		{
			CImage* pImage = new CImage;
			pImage->Load(strRelativePath);

			m_MapImage[szFileName] = pImage;
			m_ListBox.AddString(szFileName);
		}
	}
	SetHorizontalScroll();


}

void CMapTool::SetHorizontalScroll()
{
	CString strName = L"";
	CSize size;

	int iCX = 0;
	CDC* pDC = m_ListBox.GetDC();

	//GetCount : ListBox 목록에 나열된 데이터 개수를 반환.
	int ListStringCount = m_ListBox.GetCount();

	for (int i = 0; i < ListStringCount; ++i)
	{
		m_ListBox.GetText(i, strName);
		//GetTextExtent: 현재 문자열의 길이를 픽셀 단위로 크기를 반환.
		size = pDC->GetTextExtent(strName);

		if (iCX < size.cx)
			iCX = size.cx;
	}
	//참조카운터를 각 객체들이 가지는데 기 참조카운터가 0이 되면 ReleaseDC에서 지워준다.
	m_ListBox.ReleaseDC(pDC);
	//GetHorizontalExtent: 가로로 스크롤할 수 있는 최대범위
	if (iCX > m_ListBox.GetHorizontalExtent())
		m_ListBox.SetHorizontalExtent(iCX);//인자값만큼 스크롤 범위를 늘린다.
}


void CMapTool::OnLbnSelchangeList()
{
	UpdateData(TRUE);
	CString strSelectName = L"";
	int iIndex = m_ListBox.GetCurSel();

	if (iIndex == -1)
		return;

	m_ListBox.GetText(iIndex, strSelectName);

	auto& iter_find = m_MapImage.find(strSelectName);

	if (iter_find == m_MapImage.end())
		return;

	int i = 0;

	for (i = 0; i < strSelectName.GetLength(); ++i)
	{
		if (0 != isdigit(strSelectName[i]))
			break;
	}
	//Delete(index, count) : index부터 count만큼 제거 
	strSelectName.Delete(0, i);

	//_tstoi : 문자열 -> 정수 변환
	m_iDrawID = _tstoi(strSelectName);

	UpdateData(FALSE);
}

void CMapTool::OnBnClickedSave()
{
	//UpdateData(TRUE);	
	//
	//CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	//CToolView* pToolView = dynamic_cast<CToolView*>(pMainFrame->m_MainSplit.GetPane(0, 1));
	//
	//CFileDialog Dlg(FALSE, L"dat", L"제목 없음.dat", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
	//	L"Data Files(*.dat)|*.dat||", this);
	////Dlg.Create();, Dlg.DoModal(); 이렇게 두가지가 있다. 창이 떳을때 그창 이외에는 눌리지 않게하는것이 DoModal형식 창이떠있어도 주변 클릭가능한것은 ModalLess형식 

	//TCHAR szBuf[MAX_STR] = L"";
	////GetCurrentDirectory 현재 작업 경로를 얻어오는 함수.
	//GetCurrentDirectory(MAX_STR, szBuf);
	////PathRemoveFileSpec 경로 상의 파일 명을 제거하는 함수. 파일명이 없다면 말단 폴더명을 제거.
	////C:\\Users\\JeongSeongHo\\Documents\\Visual Studio 2015\\Projects\\MFC\\MapTool 181017\\Data
	//PathRemoveFileSpec(szBuf);

	//lstrcat(szBuf, L"\\Data");
	//Dlg.m_ofn.lpstrInitialDir = szBuf;//L"C:\\Users\\JeongSeongHo\\Documents\\Visual Studio 2015\\Projects\\MFC\\MapTool 181017HW\\Data";

	//if (IDOK == Dlg.DoModal())
	//{
	//	//GetPathName : 대화상자에 작성한 경로명을 얻어오는 함수.
	//	CString strFilePath = Dlg.GetPathName();
	//	//GetString -> string을 tChar* 형식으로 반환해주는 함수
	//	HANDLE hFile = CreateFile(strFilePath.GetString(), GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
	//	if (INVALID_HANDLE_VALUE == hFile)
	//	{
	//		AfxMessageBox(L"Save Failed");
	//	}
	//	DWORD dwByte = 0;
	//	for (auto& pTile : pToolView->m_pTerrain->GetVecTile())
	//	{
	//		WriteFile(hFile, pTile, sizeof(TILE), &dwByte, nullptr);
	//	}
	//}
	//pMainFrame->Invalidate(FALSE);
	//UpdateData(FALSE);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	// CFileDialog: 파일 열기 or 저장 작업에 필요한 대화상자를 생성해주는 MFC 클래스.
	CFileDialog Dlg(FALSE, L"dat", L"*.dat", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Data Files(*.dat)|*.dat||", this);

	TCHAR szBuf[MAX_STR] = L"";

	// GetCurrentDirectory: 현재 작업 경로를 얻어오는 함수.
	// D:\\김태윤\\4개월차\\90기\\Framework90\\Debug
	GetCurrentDirectory(MAX_STR, szBuf);

	// PathRemoveFileSpec: 경로 상에 파일명을 제거하는 함수. 파일명이 없다면 말단 폴더명을 제거.
	// D:\\김태윤\\4개월차\\90기\\Framework90
	PathRemoveFileSpec(szBuf);

	// D:\\김태윤\\4개월차\\90기\\Framework90\\Data
	lstrcat(szBuf, L"\\Data");

	// 대화상자를 열었을 때 초기 경로 설정
	Dlg.m_ofn.lpstrInitialDir = szBuf;	// 절대경로

	if (IDOK == Dlg.DoModal())
	{
		// GetPathName: 대화상자에 작성한 경로명을 얻어오는 함수.
		CString strFilePath = Dlg.GetPathName();
		HANDLE hFile = CreateFile(strFilePath.GetString(), GENERIC_WRITE,
			0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);

		if (INVALID_HANDLE_VALUE == hFile)
		{
			AfxMessageBox(L"Save Failed!");
			return;
		}

		DWORD dwByte = 0;

		CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
		NULL_CHECK(pMainFrm);

		CToolView* pMainView = dynamic_cast<CToolView*>(pMainFrm->m_MainSplit.GetPane(0, 1));
		NULL_CHECK(pMainView);

		CTerrain* pTerrain = pMainView->m_pTerrain;
		NULL_CHECK(pTerrain);

		const vector<TILE*>& vecTile = pTerrain->GetVecTile();

		for (size_t i = 0; i < vecTile.size(); ++i)
			WriteFile(hFile, vecTile[i], sizeof(TILE), &dwByte, nullptr);

		CloseHandle(hFile);
	}

	UpdateData(FALSE);
}


void CMapTool::OnBnClickedLoad()
{

	//UpdateData(TRUE);

	//CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	//CToolView* pToolView = dynamic_cast<CToolView*>(pMainFrame->m_MainSplit.GetPane(0, 1));

	//
	//CFileDialog Dlg(TRUE, L"dat", L"*.dat", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
	//	L"Data Files(*.dat)|*.dat||", this);

	//TCHAR szBuf[MAX_STR] = L"";
	//GetCurrentDirectory(MAX_STR, szBuf);
	//PathRemoveFileSpec(szBuf);
	//lstrcat(szBuf, L"\\Data");
	//Dlg.m_ofn.lpstrInitialDir = szBuf;	// 절대경로

	//if (IDOK == Dlg.DoModal())
	//{
	//	CString strFilePath = Dlg.GetPathName();
	//	HANDLE hFile = CreateFile(strFilePath.GetString(), GENERIC_READ,
	//		0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	//	if (INVALID_HANDLE_VALUE == hFile)
	//	{
	//		AfxMessageBox(L"Load Failed!");
	//		return;
	//	}

	//	DWORD dwByte = 0;
	//	TILE pTile = {};
	//	int iIndex = 0;
	//	while (true)
	//	{
	//		ReadFile(hFile, &pTile, sizeof(TILE), &dwByte, nullptr);

	//		if (0 == dwByte)
	//		{
	//			break;
	//		}
	//		*(pToolView->m_pTerrain->GetVecTile()[iIndex]) = pTile;
	//		iIndex++;
	//	}

	//	CloseHandle(hFile);
	//	pMainFrame->Invalidate(FALSE);
	//}
	//UpdateData(FALSE);
	UpdateData(TRUE);

	// CFileDialog: 파일 열기 or 저장 작업에 필요한 대화상자를 생성해주는 MFC 클래스.
	CFileDialog Dlg(TRUE, L"dat", L"*.dat", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Data Files(*.dat)|*.dat||", this);

	TCHAR szBuf[MAX_STR] = L"";

	// GetCurrentDirectory: 현재 작업 경로를 얻어오는 함수.
	// D:\\김태윤\\4개월차\\90기\\Framework90\\Debug
	GetCurrentDirectory(MAX_STR, szBuf);

	// PathRemoveFileSpec: 경로 상에 파일명을 제거하는 함수. 파일명이 없다면 말단 폴더명을 제거.
	// D:\\김태윤\\4개월차\\90기\\Framework90
	PathRemoveFileSpec(szBuf);

	// D:\\김태윤\\4개월차\\90기\\Framework90\\Data
	lstrcat(szBuf, L"\\Data");

	// 대화상자를 열었을 때 초기 경로 설정
	Dlg.m_ofn.lpstrInitialDir = szBuf;	// 절대경로

	if (IDOK == Dlg.DoModal())
	{
		// GetPathName: 대화상자에 작성한 경로명을 얻어오는 함수.
		CString strFilePath = Dlg.GetPathName();
		HANDLE hFile = CreateFile(strFilePath.GetString(), GENERIC_READ,
			0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

		if (INVALID_HANDLE_VALUE == hFile)
		{
			AfxMessageBox(L"Load Failed!");
			return;
		}

		CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
		NULL_CHECK(pMainFrm);

		CToolView* pMainView = dynamic_cast<CToolView*>(pMainFrm->m_MainSplit.GetPane(0, 1));
		NULL_CHECK(pMainView);

		CMiniView* pMiniView = dynamic_cast<CMiniView*>(pMainFrm->m_SecondSplit.GetPane(0, 0));
		NULL_CHECK(pMiniView);

		CTerrain* pTerrain = pMainView->m_pTerrain;
		NULL_CHECK(pTerrain);

		const vector<TILE*>& vecTile = pTerrain->GetVecTile();
		DWORD dwByte = 0;
		TILE* pTile = nullptr;

		if (!vecTile.empty())
			pTerrain->Release();

		while (true)
		{
			pTile = new TILE;
			ReadFile(hFile, pTile, sizeof(TILE), &dwByte, nullptr);

			if (0 == dwByte)
			{
				delete pTile;
				break;
			}

			pTerrain->InsertTile(pTile);
		}

		pMainView->Invalidate(FALSE);
		pMiniView->Invalidate(FALSE);
		CloseHandle(hFile);
	}

	UpdateData(FALSE);
}

void CMapTool::OnBnClickedDelete()
{
	UpdateData(TRUE);//DoDateExchange함수를 호출하게 함 
	CString strFindName = L"";
	int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, strFindName);

	auto& iter_find = m_MapImage.find(strFindName);
	if (m_MapImage.end() == iter_find)//없는것
		return;
	SafeDelete(iter_find->second);
	m_MapImage.erase(iter_find);
	m_ListBox.DeleteString(iIndex);
	HBITMAP hDeleteBitMap = m_PictureCtrl[iIndex].GetBitmap();
	DeleteObject(hDeleteBitMap);
	m_PictureCtrl[iIndex].SetBitmap(nullptr);
	Invalidate(FALSE);
	UpdateData(FALSE);
}


void CMapTool::OnEnChangeFindName()
{// 내용물이 변할때마다 호출하겠다.
	UpdateData(TRUE);
	TCHAR Dest[MAX_STR] = L"Tile";
	lstrcat(Dest, m_strFindName);
	auto& iter_find = m_MapImage.find(Dest);
	if (m_MapImage.end() == iter_find)//없는것
		return;
	int iIndex = m_ListBox.FindString(0, Dest);//몇번인덱스 다음것부터 찾는것, 누구를,
	if (-1 == iIndex)
		return;
	m_ListBox.SetCurSel(iIndex);


	UpdateData(FALSE);
}


void CMapTool::OnBnClickedTileAdd()
{
	CString  strReadPath, strDirName, strFileName;

	TCHAR szFileName[MAX_STR] = L"";
	TCHAR szTempBuf[MAX_STR] = L"";
	TCHAR szBuf[MAX_STR] = L"";
	GetCurrentDirectory(MAX_STR, szBuf);//경로가 들어감
	PathRemoveFileSpec(szBuf);//절대경로까지 들어감

	lstrcat(szBuf, L"\\Texture\\Stage\\Tile");//상대경로를 추가함

	int iIndex = GetDirFilesNum(szBuf);//상대경로까지 들어가서 그안쪽 파일의 인덱스를 저장해둠


	strFileName = PathFindFileName(szBuf);
	//확장명 생략
	lstrcpy(szFileName, strFileName.GetString());
	//경로상의 확장자를 제거.
	PathRemoveExtension(szFileName);
	lstrcpy(szTempBuf, szBuf);
	CString strRelativePath = L"";
	CString Index = L"";
	for (int i = 0; i<iIndex; i++)
	{
		Index.Format(_T("%d"), i);//타일 번호를 CString으로 변환
		lstrcat(szBuf, L"\\Tile");
		lstrcat(szBuf, Index);//변환한 것을 이름과 합침
		lstrcat(szBuf, L".png");
		lstrcat(szFileName, Index);

		strRelativePath = CFileInfo::ConvertRelativePath(szBuf);
		auto& iter_find = m_MapImage.find(szFileName);
		if (iter_find == m_MapImage.end())
		{
			CImage* pImage = new CImage;
			pImage->Load(strRelativePath);
			m_MapImage[szFileName] = pImage;
			m_ListBox.AddString(szFileName);
		}
		lstrcpy(szBuf, szTempBuf);
		lstrcpy(szFileName, L"Tile");

	}


	CString strSelectName = L"";

	TCHAR szBuffer[MAX_STR] = L"";
	GetCurrentDirectory(MAX_STR, szBuffer);//경로가 들어감
	PathRemoveFileSpec(szBuffer);//절대경로까지 들어감

	lstrcat(szBuffer, L"\\Texture\\Stage\\Tile");//상대경로를 추가함
	int iCount = GetDirFilesNum(szBuffer);//상대경로까지 들어가서 그안쪽 파일의 인덱스를 저장해둠 몇번 돌릴지 나왔고 
	for (int i = 0; i < iCount; i++)
	{
		m_ListBox.GetText(i, strSelectName);

		auto& iter_find = m_MapImage.find(strSelectName);

		if (iter_find == m_MapImage.end())
			return;

		m_PictureCtrl[i].SetBitmap(*(iter_find->second));
	}
}


int CMapTool::GetDirFilesNum(CString dirName)
{
	int count = 0;
	CFileFind finder;

	BOOL bWorking = finder.FindFile(dirName + "/*.*");

	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
		{
			continue;
		}

		count++;

	}
	finder.Close();

	return count;
}



void CMapTool::OnBnClickedChange()
{
	UpdateData(TRUE);
	CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	CToolView* pToolView = dynamic_cast<CToolView*>(pMainFrame->m_MainSplit.GetPane(0, 1));
	int i = 0;
	int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	auto& iter_find = m_MapImage.find(m_strFindName);

	if (iter_find == m_MapImage.end())
		return;

	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);
	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
	UpdateData(FALSE);
}

void CMapTool::OnStnClickedTile()
{

	m_ListBox.SetCurSel(0);
	int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}
void CMapTool::OnStnClickedTile2()
{
	m_ListBox.SetCurSel(1);
	int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}
void CMapTool::OnStnClickedTile3()
{
	m_ListBox.SetCurSel(2); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}
void CMapTool::OnStnClickedTile4()
{
	m_ListBox.SetCurSel(3); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile5()
{
	m_ListBox.SetCurSel(4); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile6()
{
	m_ListBox.SetCurSel(5); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile7()
{
	m_ListBox.SetCurSel(6); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile8()
{
	m_ListBox.SetCurSel(7); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile9()
{
	m_ListBox.SetCurSel(8); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile10()
{
	m_ListBox.SetCurSel(9); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile11()
{
	m_ListBox.SetCurSel(10); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile12()
{
	m_ListBox.SetCurSel(11); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile13()
{
	m_ListBox.SetCurSel(12); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile14()
{
	m_ListBox.SetCurSel(13); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile15()
{
	m_ListBox.SetCurSel(14); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile16()
{
	m_ListBox.SetCurSel(15); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile17()
{
	m_ListBox.SetCurSel(16); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile18()
{
	m_ListBox.SetCurSel(17); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile19()
{
	m_ListBox.SetCurSel(18); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile20()
{
	m_ListBox.SetCurSel(19); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile21()
{
	m_ListBox.SetCurSel(20); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile22()
{
	m_ListBox.SetCurSel(21); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile23()
{
	m_ListBox.SetCurSel(22); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile24()
{
	m_ListBox.SetCurSel(23); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile25()
{
	m_ListBox.SetCurSel(24); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile26()
{
	m_ListBox.SetCurSel(25); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile27()
{
	m_ListBox.SetCurSel(26); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile28()
{
	m_ListBox.SetCurSel(27); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile29()
{
	m_ListBox.SetCurSel(28); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile30()
{
	m_ListBox.SetCurSel(29); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}
void CMapTool::OnStnClickedTile31()
{
	m_ListBox.SetCurSel(30); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile32()
{
	m_ListBox.SetCurSel(31); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile33()
{
	m_ListBox.SetCurSel(32); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile34()
{
	m_ListBox.SetCurSel(33); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile35()
{
	m_ListBox.SetCurSel(34); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile36()
{
	m_ListBox.SetCurSel(35); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile37()
{
	m_ListBox.SetCurSel(36); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile38()
{
	m_ListBox.SetCurSel(37); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile39()
{
	m_ListBox.SetCurSel(38); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile40()
{
	m_ListBox.SetCurSel(39); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile41()
{
	m_ListBox.SetCurSel(40); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}void CMapTool::OnStnClickedTile42()
{
	m_ListBox.SetCurSel(41); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile43()
{
	m_ListBox.SetCurSel(42);
	int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile44()
{
	m_ListBox.SetCurSel(43); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile45()
{
	m_ListBox.SetCurSel(44); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile46()
{
	m_ListBox.SetCurSel(45); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile47()
{
	m_ListBox.SetCurSel(46); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile48()
{
	m_ListBox.SetCurSel(47); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile49()
{
	m_ListBox.SetCurSel(48); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile50()
{
	m_ListBox.SetCurSel(49); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile51()
{
	m_ListBox.SetCurSel(50); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile52()
{
	m_ListBox.SetCurSel(51); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile53()
{
	m_ListBox.SetCurSel(52); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile54()
{
	m_ListBox.SetCurSel(53); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile55()
{
	m_ListBox.SetCurSel(54); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile56()
{
	m_ListBox.SetCurSel(55); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile57()
{
	m_ListBox.SetCurSel(56); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile58()
{
	m_ListBox.SetCurSel(57); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile59()
{
	m_ListBox.SetCurSel(58); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile60()
{
	m_ListBox.SetCurSel(59); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}

void CMapTool::OnStnClickedTile61()
{
	m_ListBox.SetCurSel(60); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}



void CMapTool::OnStnClickedTile62()
{
	m_ListBox.SetCurSel(61); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}



void CMapTool::OnStnClickedTile63()
{
	m_ListBox.SetCurSel(62); int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}



void CMapTool::OnStnClickedTile64()
{
	m_ListBox.SetCurSel(63);
	int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}



void CMapTool::OnStnClickedTile65()
{
	m_ListBox.SetCurSel(64); 
	
	int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	int i = 0;
	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);

	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
}



