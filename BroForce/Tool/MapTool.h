#pragma once
#include "afxwin.h"

class CMapTool : public CDialog
{
	DECLARE_DYNAMIC(CMapTool)

public:
	CMapTool(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CMapTool();

	// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MAPTOOL };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:		// Message Function
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnLbnSelchangeList();
	afx_msg void OnBnClickedSave();
	afx_msg void OnBnClickedLoad();
	afx_msg void OnBnClickedDelete();
	afx_msg void OnEnChangeFindName();
	afx_msg void OnBnClickedTileAdd();
	afx_msg void OnBnClickedChange();
	afx_msg void OnStnClickedTile();
	void OnStnClickedTile2();
	void OnStnClickedTile3();
	void OnStnClickedTile4();
	void OnStnClickedTile5();
	void OnStnClickedTile6();
	void OnStnClickedTile7();
	void OnStnClickedTile8();
	void OnStnClickedTile9();
	void OnStnClickedTile10();
	void OnStnClickedTile11();
	void OnStnClickedTile12();
	void OnStnClickedTile13();
	void OnStnClickedTile14();
	void OnStnClickedTile15();
	void OnStnClickedTile16();
	void OnStnClickedTile17();
	void OnStnClickedTile18();
	void OnStnClickedTile19();
	void OnStnClickedTile20();
	void OnStnClickedTile21();
	void OnStnClickedTile22();
	void OnStnClickedTile23();
	void OnStnClickedTile24();
	void OnStnClickedTile25();
	void OnStnClickedTile26();
	void OnStnClickedTile27();
	void OnStnClickedTile28();
	void OnStnClickedTile29();
	void OnStnClickedTile30();
	void OnStnClickedTile31();
	void OnStnClickedTile32();
	void OnStnClickedTile33();
	void OnStnClickedTile34();
	void OnStnClickedTile35();
	void OnStnClickedTile36();
	void OnStnClickedTile37();
	void OnStnClickedTile38();
	void OnStnClickedTile39();
	void OnStnClickedTile40();
	void OnStnClickedTile41();
	void OnStnClickedTile42();
	void OnStnClickedTile43();
	void OnStnClickedTile44();
	void OnStnClickedTile45();
	void OnStnClickedTile46();
	void OnStnClickedTile47();
	void OnStnClickedTile48();
	void OnStnClickedTile49();
	void OnStnClickedTile50();
	void OnStnClickedTile51();
	void OnStnClickedTile52();
	void OnStnClickedTile53();
	void OnStnClickedTile54();
	void OnStnClickedTile55();
	void OnStnClickedTile56();
	void OnStnClickedTile57();
	void OnStnClickedTile58();
	void OnStnClickedTile59();
	void OnStnClickedTile60();
	void OnStnClickedTile61();
	void OnStnClickedTile62();
	void OnStnClickedTile63();
	void OnStnClickedTile64();
	void OnStnClickedTile65();

public:
	void SetHorizontalScroll();
	int GetDirFilesNum(CString dirName);

public:		// Control
	CListBox m_ListBox;
public:
	//CImage : MFC에서 제공하는 이미지 클래스 jpeg, png, bmp, gif
	CStatic m_PictureCtrl[65];
	map<CString, CImage*> m_MapImage;
	vector<TILE*>	m_vecTile;
	int m_iDrawID;
	int m_iOption;
	CString m_strFindName;
	BYTE m_byOption;
	int DeleteCount;
};
