#include "stdafx.h"
#include "MultiTexture.h"


CMultiTexture::CMultiTexture()
{
}


CMultiTexture::~CMultiTexture()
{
	Release();
}

const TEXINFO * CMultiTexture::GetTexInfo(const wstring& wstrStateKey,
	const int& iImgIndex) const
{
	if (m_MapMulti.empty())
		return nullptr;

	auto& iter_find = m_MapMulti.find(wstrStateKey);

	if (m_MapMulti.end() == iter_find)
		return nullptr;

	return iter_find->second[iImgIndex];
}

HRESULT CMultiTexture::InsertTexture(const wstring& wstrFilePath,
	const wstring& wstrStateKey, const int& iImgCount)
{
	auto& iter_find = m_MapMulti.find(wstrStateKey);

	if (m_MapMulti.end() != iter_find)
		return E_FAIL;

	TCHAR szFullPath[MAX_STR] = L"";

	for (int i = 0; i < iImgCount; ++i)
	{
		swprintf_s(szFullPath, wstrFilePath.c_str(), i);

		TEXINFO* pTexInfo = new TEXINFO;

		if (FAILED(D3DXGetImageInfoFromFile(szFullPath, &(pTexInfo->tImgInfo))))
		{
			ERR_MSG(szFullPath);
			SafeDelete(pTexInfo);
			return E_FAIL;
		}

		if (FAILED(D3DXCreateTextureFromFileEx(GET_INSTANCE(CDeviceMgr)->GetDevice(),
			szFullPath, /* 이미지 경로 */
			pTexInfo->tImgInfo.Width, /* 이미지 가로 크기 */
			pTexInfo->tImgInfo.Height, /* 이미지 세로 크기 */
			pTexInfo->tImgInfo.MipLevels, /* 밉 레벨 */
			0, /* 불러올 이미지의 사용방식. 특수한 경우가 아닌 이상 0 */
			pTexInfo->tImgInfo.Format, /* 이미지의 픽셀 포맷 */
			D3DPOOL_MANAGED, /* 메모리 사용방식 */
			D3DX_DEFAULT, D3DX_DEFAULT, 0, nullptr, nullptr, &(pTexInfo->pTexture))))
		{
			ERR_MSG(L"MultiTexture Load Failed!");
			SafeDelete(pTexInfo);
			return E_FAIL;
		}

		m_MapMulti[wstrStateKey].push_back(pTexInfo);
	}

	return S_OK;
}

void CMultiTexture::Release()
{
	for_each(m_MapMulti.begin(), m_MapMulti.end(),
		[](auto& MyPair)
	{
		for_each(MyPair.second.begin(), MyPair.second.end(),
			[](auto& pTexInfo)
		{
			pTexInfo->pTexture->Release();
			SafeDelete(pTexInfo);
		});
	});

	m_MapMulti.clear();
}
