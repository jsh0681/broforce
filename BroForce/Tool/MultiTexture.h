#pragma once
#include "Texture.h"
class CMultiTexture :
	public CTexture
{
public:
	CMultiTexture();
	virtual ~CMultiTexture();

public:
	// CTexture을(를) 통해 상속됨
	virtual const TEXINFO * GetTexInfo(const wstring& wstrStateKey,
		const int& iImgIndex) const override;
	virtual HRESULT InsertTexture(const wstring& wstrFilePath,
		const wstring & wstrStateKey, const int& iImgCount) override;
	virtual void Release() override;

private:
	map<wstring, vector<TEXINFO*>>	m_MapMulti;
};

