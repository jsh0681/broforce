// MyForm.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tool.h"
#include "MyForm.h"


// CMyForm

IMPLEMENT_DYNCREATE(CMyForm, CFormView)

CMyForm::CMyForm()
	: CFormView(IDD_MYFORM)
{

}

CMyForm::~CMyForm()
{
}

void CMyForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMyForm, CFormView)
	ON_BN_CLICKED(IDC_BUTTON2, &CMyForm::OnBnClickedMapTool)
	ON_BN_CLICKED(IDC_BUTTON5, &CMyForm::OnBnClickedPopUp)
	ON_BN_CLICKED(IDC_BUTTON7, &CMyForm::OnBnClickedPathFind)
	ON_BN_CLICKED(IDC_BUTTON1, &CMyForm::OnBnClickedObjectTool)
END_MESSAGE_MAP()


// CMyForm 진단입니다.

#ifdef _DEBUG
void CMyForm::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMyForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CMyForm 메시지 처리기입니다.


void CMyForm::OnInitialUpdate()
{
	CFormView::OnInitialUpdate(); 
	m_Font.CreatePointFont(180, L"궁서");
	if (nullptr == m_MapTool.GetSafeHwnd())
	{
		// Create: 해당 ID를 가진 Dialog를 생성
		m_MapTool.Create(IDD_MAPTOOL);
	}

	if (nullptr == m_PopUp.GetSafeHwnd())
	{
		// Create: 해당 ID를 가진 Dialog를 생성
		m_PopUp.Create(IDD_POPUP);
	}
	if (nullptr == m_PathFind.GetSafeHwnd())
	{
		// Create: 해당 ID를 가진 Dialog를 생성
		m_PathFind.Create(IDD_PATHFIND);
	}
	if (nullptr == m_ObjTool.GetSafeHwnd())
	{
		// Create: 해당 ID를 가진 Dialog를 생성
		m_ObjTool.Create(IDD_OBJECTTOOL);
	}
	
	
}



void CMyForm::OnBnClickedMapTool()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_MapTool.ShowWindow(SW_SHOW); // 창 모양으로 출력
}



void CMyForm::OnBnClickedPopUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_PopUp.ShowWindow(SW_SHOW); // 창 모양으로 출력
}



void CMyForm::OnBnClickedPathFind()
{
	m_PathFind.ShowWindow(SW_SHOW);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMyForm::OnBnClickedObjectTool()
{
	m_ObjTool.ShowWindow(SW_SHOW);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
