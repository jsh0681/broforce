#pragma once



#include "MapTool.h"
#include "PopUp.h"
#include "PathFind.h"
#include "ObjectTool.h"

class CMyForm : public CFormView
{
	DECLARE_DYNCREATE(CMyForm)

protected:
	CMyForm();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CMyForm();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MYFORM };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate();
	DECLARE_MESSAGE_MAP()

public:
	// message function
	afx_msg void OnBnClickedMapTool();
	afx_msg void OnBnClickedPopUp();
	afx_msg void OnBnClickedPathFind();
	afx_msg void OnBnClickedObjectTool();

public:
	// CFont: MFC에서 제공하는 Font 객체
	CFont		m_Font;
	CMapTool	m_MapTool;
	CPopUp		m_PopUp;
	CPathFind   m_PathFind;
	CObjectTool m_ObjTool;
};


