// ObjectTool.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tool.h"
#include "ObjectTool.h"
#include "afxdialogex.h"


#include "ToolView.h"
#include "Miniview.h"
#include"MainFrm.h"
#include"FileInfo.h"
#include"Obstacle.h"


// CObjectTool 대화 상자입니다.

IMPLEMENT_DYNAMIC(CObjectTool, CDialog)

CObjectTool::CObjectTool(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_OBJECTTOOL, pParent)
	, m_strFindName(_T(""))
{

}

CObjectTool::~CObjectTool()
{
}

void CObjectTool::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ListBox);
	DDX_Text(pDX, IDC_EDIT2, m_strFindName);
	DDX_Control(pDX, IDC_PICTURE, m_PictureCtrl);
}


BEGIN_MESSAGE_MAP(CObjectTool, CDialog)
	ON_WM_DROPFILES()
	ON_BN_CLICKED(IDC_BUTTON1, &CObjectTool::OnBnClickedAddObject)
	ON_BN_CLICKED(IDC_BUTTON8, &CObjectTool::OnBnClickedChange)
	ON_BN_CLICKED(IDC_BUTTON9, &CObjectTool::OnBnClickedSave)
	ON_BN_CLICKED(IDC_BUTTON10, &CObjectTool::OnBnClickedLoad)
	ON_LBN_SELCHANGE(IDC_LIST1, &CObjectTool::OnLbnSelchangeListBox)
	ON_EN_CHANGE(IDC_EDIT2, &CObjectTool::OnEnChangeFindName)
END_MESSAGE_MAP()


// CObjectTool 메시지 처리기입니다.


void CObjectTool::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialog::OnDropFiles(hDropInfo);

	CString strRelativePath = L"";
	CString strFileName = L"";

	TCHAR szFilePath[MAX_STR] = L"";
	TCHAR szFileName[MAX_STR] = L"";

	// DragQueryFile: 두번째 인자가 -1일 경우 드래그 앤 드롭된 전체 파일 개수를 반환.
	int iCount = DragQueryFile(hDropInfo, -1, nullptr, 0);

	for (int i = 0; i < iCount; ++i)
	{
		DragQueryFile(hDropInfo, i, szFilePath, MAX_STR);
		//상대경로 변환
		strRelativePath = CFileInfo::ConvertRelativePath(szFilePath);
		//파일명만 출력
		strFileName = PathFindFileName(strRelativePath);
		//확장명 생략
		lstrcpy(szFileName, strFileName.GetString());
		//경로상의 확장자를 제거.
		PathRemoveExtension(szFileName);

		auto& iter_find = m_ObjImage.find(szFileName);
		if (iter_find == m_ObjImage.end())
		{
			CImage* pImage = new CImage;
			pImage->Load(strRelativePath);

			m_ObjImage[szFileName] = pImage;
			m_ListBox.AddString(szFileName);
		}
	}
	SetHorizontalScroll();
}


void CObjectTool::SetHorizontalScroll()
{
	CString strName = L"";
	CSize size;

	int iCX = 0;
	CDC* pDC = m_ListBox.GetDC();

	//GetCount : ListBox 목록에 나열된 데이터 개수를 반환.
	int ListStringCount = m_ListBox.GetCount();

	for (int i = 0; i < ListStringCount; ++i)
	{
		m_ListBox.GetText(i, strName);
		//GetTextExtent: 현재 문자열의 길이를 픽셀 단위로 크기를 반환.
		size = pDC->GetTextExtent(strName);

		if (iCX < size.cx)
			iCX = size.cx;
	}
	//참조카운터를 각 객체들이 가지는데 기 참조카운터가 0이 되면 ReleaseDC에서 지워준다.
	m_ListBox.ReleaseDC(pDC);
	//GetHorizontalExtent: 가로로 스크롤할 수 있는 최대범위
	if (iCX > m_ListBox.GetHorizontalExtent())
		m_ListBox.SetHorizontalExtent(iCX);//인자값만큼 스크롤 범위를 늘린다.
}



int CObjectTool::GetDirFilesNum(CString dirName)
{
	int count = 0;
	CFileFind finder;

	BOOL bWorking = finder.FindFile(dirName + "/*.*");

	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
		{
			continue;
		}

		count++;

	}
	finder.Close();

	return count;
}


void CObjectTool::OnBnClickedAddObject()
{
	CString  strReadPath, strDirName, strFileName;

	TCHAR szFileName[MAX_STR] = L"";
	TCHAR szTempBuf[MAX_STR] = L"";
	TCHAR szBuf[MAX_STR] = L"";
	GetCurrentDirectory(MAX_STR, szBuf);//경로가 들어감
	PathRemoveFileSpec(szBuf);//절대경로까지 들어감

	lstrcat(szBuf, L"\\Texture\\Stage\\Obj");//상대경로를 추가함

	int iIndex = GetDirFilesNum(szBuf);//상대경로까지 들어가서 그안쪽 파일의 인덱스를 저장해둠


	strFileName = PathFindFileName(szBuf);
	//확장명 생략
	lstrcpy(szFileName, strFileName.GetString());
	//경로상의 확장자를 제거.
	PathRemoveExtension(szFileName);
	lstrcpy(szTempBuf, szBuf);
	CString strRelativePath = L"";
	CString Index = L"";
	for (int i = 0; i<iIndex; i++)
	{
		Index.Format(_T("%d"), i);//타일 번호를 CString으로 변환
		lstrcat(szBuf, L"\\Obj");
		lstrcat(szBuf, Index);//변환한 것을 이름과 합침
		lstrcat(szBuf, L".png");
		lstrcat(szFileName, Index);

		strRelativePath = CFileInfo::ConvertRelativePath(szBuf);
		auto& iter_find = m_ObjImage.find(szFileName);
		if (iter_find == m_ObjImage.end())
		{
			CImage* pImage = new CImage;
			pImage->Load(strRelativePath);
			m_ObjImage[szFileName] = pImage;
			m_ListBox.AddString(szFileName);
		}
		lstrcpy(szBuf, szTempBuf);
		lstrcpy(szFileName, L"Obj");

	}


	CString strSelectName = L"";

	TCHAR szBuffer[MAX_STR] = L"";
	GetCurrentDirectory(MAX_STR, szBuffer);//경로가 들어감
	PathRemoveFileSpec(szBuffer);//절대경로까지 들어감

	lstrcat(szBuffer, L"\\Texture\\Stage\\Obj");//상대경로를 추가함
	int iCount = GetDirFilesNum(szBuffer);//상대경로까지 들어가서 그안쪽 파일의 인덱스를 저장해둠 몇번 돌릴지 나왔고 
	for (int i = 0; i < iCount; i++)
	{
		m_ListBox.GetText(i, strSelectName);

		auto& iter_find = m_ObjImage.find(strSelectName);

		if (iter_find == m_ObjImage.end())
			return;
	}
}


void CObjectTool::OnBnClickedChange()
{
	UpdateData(TRUE);
	CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	CToolView* pToolView = dynamic_cast<CToolView*>(pMainFrame->m_MainSplit.GetPane(0, 1));
	int i = 0;
	int iIndex = m_ListBox.GetCurSel();
	if (-1 == iIndex)
		return;
	m_ListBox.GetText(iIndex, m_strFindName);
	auto& iter_find = m_ObjImage.find(m_strFindName);

	if (iter_find == m_ObjImage.end())
		return;

	for (i = 0; i < m_strFindName.GetLength(); ++i)
	{
		if (0 != isdigit(m_strFindName[i]))
			break;
	}
	m_strFindName.Delete(0, i);
	m_iDrawID = _tstoi(m_strFindName);
	m_iOption = m_byOption;
	UpdateData(FALSE);
}


void CObjectTool::OnBnClickedSave()
{
	UpdateData(TRUE);

	// CFileDialog: 파일 열기 or 저장 작업에 필요한 대화상자를 생성해주는 MFC 클래스.
	CFileDialog Dlg(FALSE, L"dat", L"*.dat", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Data Files(*.dat)|*.dat||", this);

	TCHAR szBuf[MAX_STR] = L"";

	// GetCurrentDirectory: 현재 작업 경로를 얻어오는 함수.
	// D:\\김태윤\\4개월차\\90기\\Framework90\\Debug
	GetCurrentDirectory(MAX_STR, szBuf);

	// PathRemoveFileSpec: 경로 상에 파일명을 제거하는 함수. 파일명이 없다면 말단 폴더명을 제거.
	// D:\\김태윤\\4개월차\\90기\\Framework90
	PathRemoveFileSpec(szBuf);

	// D:\\김태윤\\4개월차\\90기\\Framework90\\Data
	lstrcat(szBuf, L"\\Data");

	// 대화상자를 열었을 때 초기 경로 설정
	Dlg.m_ofn.lpstrInitialDir = szBuf;	// 절대경로

	if (IDOK == Dlg.DoModal())
	{
		// GetPathName: 대화상자에 작성한 경로명을 얻어오는 함수.
		CString strFilePath = Dlg.GetPathName();
		HANDLE hFile = CreateFile(strFilePath.GetString(), GENERIC_WRITE,
			0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr); 


		if (INVALID_HANDLE_VALUE == hFile)
		{
			AfxMessageBox(L"Save Failed!");
			return;
		}

		DWORD dwByte = 0;

		CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
		NULL_CHECK(pMainFrm);

		CToolView* pMainView = dynamic_cast<CToolView*>(pMainFrm->m_MainSplit.GetPane(0, 1));
		NULL_CHECK(pMainView);

		CObstacle* pObstacle = pMainView->m_pObstacle;
		NULL_CHECK(pObstacle);

		const vector<OBJ*>& vecTile = pObstacle->GetVecTile();

		for (size_t i = 0; i < vecTile.size(); ++i)
			WriteFile(hFile, vecTile[i], sizeof(OBJ), &dwByte, nullptr);

		CloseHandle(hFile);
	}

	UpdateData(FALSE);
}


void CObjectTool::OnBnClickedLoad()
{
	UpdateData(TRUE);

	// CFileDialog: 파일 열기 or 저장 작업에 필요한 대화상자를 생성해주는 MFC 클래스.
	CFileDialog Dlg(TRUE, L"dat", L"*.dat", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Data Files(*.dat)|*.dat||", this);

	TCHAR szBuf[MAX_STR] = L"";

	// GetCurrentDirectory: 현재 작업 경로를 얻어오는 함수.
	// D:\\김태윤\\4개월차\\90기\\Framework90\\Debug
	GetCurrentDirectory(MAX_STR, szBuf);

	// PathRemoveFileSpec: 경로 상에 파일명을 제거하는 함수. 파일명이 없다면 말단 폴더명을 제거.
	// D:\\김태윤\\4개월차\\90기\\Framework90
	PathRemoveFileSpec(szBuf);

	// D:\\김태윤\\4개월차\\90기\\Framework90\\Data
	lstrcat(szBuf, L"\\Data");

	// 대화상자를 열었을 때 초기 경로 설정
	Dlg.m_ofn.lpstrInitialDir = szBuf;	// 절대경로

	if (IDOK == Dlg.DoModal())
	{
		// GetPathName: 대화상자에 작성한 경로명을 얻어오는 함수.
		CString strFilePath = Dlg.GetPathName();
		HANDLE hFile = CreateFile(strFilePath.GetString(), GENERIC_READ,
			0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

		if (INVALID_HANDLE_VALUE == hFile)
		{
			AfxMessageBox(L"Load Failed!");
			return;
		}

		CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
		NULL_CHECK(pMainFrm);

		CToolView* pMainView = dynamic_cast<CToolView*>(pMainFrm->m_MainSplit.GetPane(0, 1));
		NULL_CHECK(pMainView);

		CMiniView* pMiniView = dynamic_cast<CMiniView*>(pMainFrm->m_SecondSplit.GetPane(0, 0));
		NULL_CHECK(pMiniView);

		CObstacle* pObstacle = pMainView->m_pObstacle;
		NULL_CHECK(pObstacle);

		const vector<OBJ*>& vecTile = pObstacle->GetVecTile();
		DWORD dwByte = 0;
		OBJ* pTile = nullptr;

		if (!vecTile.empty())
			pObstacle->Release();

		while (true)
		{
			pTile = new OBJ;
			ReadFile(hFile, pTile, sizeof(OBJ), &dwByte, nullptr);

			if (0 == dwByte)
			{
				delete pTile;
				break;
			}

			pObstacle->InsertTile(pTile);
		}

		pMainView->Invalidate(FALSE);
		pMiniView->Invalidate(FALSE);
		CloseHandle(hFile);
	}

	UpdateData(FALSE);
}


void CObjectTool::OnLbnSelchangeListBox()
{
	UpdateData(TRUE);
	CString strSelectName = L"";
	int iIndex = m_ListBox.GetCurSel();

	if (iIndex == -1)
		return;

	m_ListBox.GetText(iIndex, strSelectName);

	auto& iter_find = m_ObjImage.find(strSelectName);

	if (iter_find == m_ObjImage.end())
		return;

	m_PictureCtrl.SetBitmap(*(iter_find->second));//그림을 출력

	int i = 0;

	for (i = 0; i < strSelectName.GetLength(); ++i)
	{
		if (0 != isdigit(strSelectName[i]))
			break;
	}
	//Delete(index, count) : index부터 count만큼 제거 
	strSelectName.Delete(0, i);

	//_tstoi : 문자열 -> 정수 변환
	m_iDrawID = _tstoi(strSelectName);
	
	UpdateData(FALSE);
}




void CObjectTool::OnEnChangeFindName()
{
		UpdateData(TRUE);
		TCHAR Dest[MAX_STR] = L"Obj";
		lstrcat(Dest, m_strFindName);
		auto& iter_find = m_ObjImage.find(Dest);
		if (m_ObjImage.end() == iter_find)//없는것
			return;
		int iIndex = m_ListBox.FindString(0, Dest);//몇번인덱스 다음것부터 찾는것, 누구를,
		if (-1 == iIndex)
			return;
		m_ListBox.SetCurSel(iIndex);
	
	
		UpdateData(FALSE);
}
