#pragma once
#include "afxwin.h"


// CObjectTool 대화 상자입니다.

class CObjectTool : public CDialog
{
	DECLARE_DYNAMIC(CObjectTool)

public:
	CObjectTool(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CObjectTool();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_OBJECTTOOL };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnBnClickedChange();
	afx_msg void OnBnClickedSave();
	afx_msg void OnBnClickedLoad();
	afx_msg void OnLbnSelchangeListBox();
	afx_msg void OnBnClickedAddObject();
	afx_msg void OnEnChangeFindName();
		int GetDirFilesNum(CString dirName);
	void SetHorizontalScroll();
public:
	CListBox m_ListBox;

	int m_iDrawID;
	int m_iOption;
	map<CString, CImage*> m_ObjImage;
	BYTE m_byOption;
	vector<TILE*>	m_vecObject;
	CString m_strFindName;
	CStatic m_PictureCtrl;
};
