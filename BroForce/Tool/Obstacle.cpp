
#include "stdafx.h"
#include "Obstacle.h"
#include "ToolView.h"
#include "ObjectTool.h"


CObstacle::CObstacle()
{
}


CObstacle::~CObstacle()
{
}

void CObstacle::Initialize()
{

}

void CObstacle::Render()
{

	D3DXMATRIX matScale, matTrans, matWorld;

	TCHAR szIndex[MIN_STR] = L"";

	float fScrollX = (float)m_pMainView->GetScrollPos(0);
	float fScrollY = (float)m_pMainView->GetScrollPos(1);

	for (size_t i = 0; i < m_vecTile.size(); ++i)
	{
		D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
		D3DXMatrixTranslation(&matTrans, m_vecTile[i]->vPos.x - fScrollX,
			m_vecTile[i]->vPos.y - fScrollY, 0.f);

		matWorld = matScale * matTrans;

		const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
			L"Stage", L"Obj", m_vecTile[i]->byDrawID);
		NULL_CHECK(pTexInfo);

		float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
		float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

		// 현재 윈도우 크기와 Directx의 후면버퍼 크기와의 비율을 구해야함.
		ResolutionSync(matWorld);

		GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
		GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
			&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));

		// Font 출력
		// D3DX 폰트 출력에는 항상 스프라이트 이미지를 생성하여 출력하므로 성능 저하가 온다.
		swprintf_s(szIndex, L"%d", i);
		GET_INSTANCE(CDeviceMgr)->GetFont()->DrawTextW(GET_INSTANCE(CDeviceMgr)->GetSprite(),
			szIndex, lstrlen(szIndex), nullptr, 0, D3DCOLOR_ARGB(255, 255, 255, 255));
	}
}



void CObstacle::Release()
{
	for_each(m_vecTile.begin(), m_vecTile.end(), SafeDelete<OBJ*>);
	m_vecTile.clear();
	m_vecTile.shrink_to_fit();
}

void CObstacle::InsertTile(OBJ * pTile)
{
	m_vecTile.push_back(pTile);
}

void CObstacle::TileChange(const D3DXVECTOR3& vPos, const D3DXVECTOR3& vSize, const BYTE& byDrawID,
	const BYTE& byOption)
{
	OBJ* m_pTile = new OBJ;
	m_pTile->vPos = vPos;
	m_pTile->vSize = vSize;
	m_pTile->byDrawID = byDrawID;
	m_pTile->byOption = byOption;
	InsertTile(m_pTile);
}




void CObstacle::ResolutionSync(D3DXMATRIX & matWorld)
{
	RECT rcView = {};
	m_pMainView->GetClientRect(&rcView);

	float fRatioX = WINCX / float(rcView.right - rcView.left);
	float fRatioY = WINCY / float(rcView.bottom - rcView.top);

	matWorld._11 *= fRatioX;
	matWorld._12 *= fRatioY;

	matWorld._21 *= fRatioX;
	matWorld._22 *= fRatioY;

	matWorld._31 *= fRatioX;
	matWorld._32 *= fRatioY;

	matWorld._41 *= fRatioX;
	matWorld._42 *= fRatioY;
}

void CObstacle::MiniRender()
{
	D3DXMATRIX matScale, matTrans, matWorld;

	float fScrollX = (float)m_pMainView->GetScrollPos(0);
	float fScrollY = (float)m_pMainView->GetScrollPos(1);

	float fRatioX = 0.3f;
	float fRatioY = 0.3f;

	float fCenterX = 0.f;
	float fCenterY = 0.f;

	for (size_t i = 0; i < m_vecTile.size(); ++i)
	{
		D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
		D3DXMatrixTranslation(&matTrans, m_vecTile[i]->vPos.x - fScrollX,
			m_vecTile[i]->vPos.y - fScrollY, 0.f);

		matWorld = matScale * matTrans;

		const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
			L"Stage", L"Obj", m_vecTile[i]->byDrawID);
		NULL_CHECK(pTexInfo);

		fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
		fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

		matWorld._11 *= fRatioX;
		matWorld._12 *= fRatioY;

		matWorld._21 *= fRatioX;
		matWorld._22 *= fRatioY;

		matWorld._31 *= fRatioX;
		matWorld._32 *= fRatioY;

		matWorld._41 *= fRatioX;
		matWorld._42 *= fRatioY;

		GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
		GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
			&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	}
}