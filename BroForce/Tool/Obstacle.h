#pragma once

class CToolView;

class CMapTool;

class CObstacle
{
public:
	CObstacle();
	~CObstacle();

public:
	void Initialize();
	void Render();
	void MiniRender();
	void Release();
public:
	const vector<OBJ*>& GetVecTile()const { return m_vecTile; }
public:
	void SetMainView(CToolView* pMainView) { m_pMainView = pMainView; }

public:
	void InsertTile(OBJ* pTile);
	void TileChange(const D3DXVECTOR3& vPos, const D3DXVECTOR3& vSize, const BYTE& byDrawID, const BYTE& byOption = 0);

public:
	void ResolutionSync(D3DXMATRIX& matWorld);
private:
	vector<OBJ*>	m_vecTile;
	CToolView*		m_pMainView;
};

