// PathFind.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tool.h"
#include "PathFind.h"
#include "afxdialogex.h"

#include "FileInfo.h"


// CPathFind 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPathFind, CDialog)

CPathFind::CPathFind(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_PATHFIND, pParent)
{

}

CPathFind::~CPathFind()
{
	Release();
}

void CPathFind::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ListBox);
}


BEGIN_MESSAGE_MAP(CPathFind, CDialog)
	ON_WM_DROPFILES()
	ON_BN_CLICKED(IDC_BUTTON1, &CPathFind::OnBnClickedSave)
	ON_BN_CLICKED(IDC_BUTTON6, &CPathFind::OnBnClickedLoad)
END_MESSAGE_MAP()


// CPathFind 메시지 처리기입니다.


void CPathFind::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CDialog::OnDropFiles(hDropInfo);

	UpdateData(TRUE);

	int iCount = DragQueryFile(hDropInfo, -1, nullptr, 0);

	TCHAR szFullPath[MAX_STR] = L"";
	TCHAR	szCount[MIN_STR] = L"";
	wstring wstrCombined = L"";

	for (int i = 0; i < iCount; ++i)
	{
		DragQueryFile(hDropInfo, i, szFullPath, MAX_STR);
		CFileInfo::DirInfoExtraction(szFullPath, m_ImgPathLst);
	}

	for (auto& pImgPath : m_ImgPathLst)
	{
		// _itow_s: 정수를 문자열로 변환.
		_itow_s(pImgPath->iCount, szCount, 10);

		wstrCombined = pImgPath->wstrObjKey + L"|" + pImgPath->wstrStateKey + L"|"
			+ szCount + L"|" + pImgPath->wstrPath;

		m_ListBox.AddString(wstrCombined.c_str());
	}

	SetHorizontalScroll();

	UpdateData(FALSE);
}

void CPathFind::SetHorizontalScroll()
{
	CString strName = L"";
	CSize	size;
	int iCX = 0;

	CDC* pDC = m_ListBox.GetDC();

	// GetCount: listbox 목록에 나열된 데이터 개수를 반환.
	for (int i = 0; i < m_ListBox.GetCount(); ++i)
	{
		m_ListBox.GetText(i, strName);

		// GetTextExtent: 현재 문자열의 길이를 픽셀 단위로 크기를 반환.
		size = pDC->GetTextExtent(strName);

		if (iCX < size.cx)
			iCX = size.cx;
	}

	m_ListBox.ReleaseDC(pDC);

	// GetHorizontalExtent: 가로로 스크롤할 수 있는 최대 범위
	if (iCX > m_ListBox.GetHorizontalExtent())
		m_ListBox.SetHorizontalExtent(iCX); // 인자 값만큼 스크롤 범위를 늘린다.
}

void CPathFind::Release()
{
	for_each(m_ImgPathLst.begin(), m_ImgPathLst.end(), SafeDelete<IMGPATH*>);
	m_ImgPathLst.clear();
}


void CPathFind::OnBnClickedSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	CFileDialog Dlg(FALSE, L"txt", L"*.txt", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Data Files(*.txt)|*.txt||", this);

	TCHAR szBuf[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szBuf);
	PathRemoveFileSpec(szBuf);
	lstrcat(szBuf, L"\\Data");

	Dlg.m_ofn.lpstrInitialDir = szBuf;	// 절대경로

	if (IDOK == Dlg.DoModal())
	{
		CString strFilePath = Dlg.GetPathName();

		wofstream fout;
		fout.open(strFilePath.GetString());

		if (fout.fail())
		{
			AfxMessageBox(strFilePath.GetString());
			return;
		}

		for (auto& pImgPath : m_ImgPathLst)
			fout << pImgPath->wstrObjKey << L"|" << pImgPath->wstrStateKey << L"|"
			<< pImgPath->iCount << L"|" << pImgPath->wstrPath << endl;

		fout.close();
	}

	WinExec("notepad.exe ../Data/ImgPath.txt", SW_SHOW);
	UpdateData(FALSE);
}


void CPathFind::OnBnClickedLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	CFileDialog Dlg(TRUE, L"txt", L"*.txt", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Data Files(*.txt)|*.txt||", this);

	TCHAR szBuf[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szBuf);
	PathRemoveFileSpec(szBuf);
	lstrcat(szBuf, L"\\Data");

	Dlg.m_ofn.lpstrInitialDir = szBuf;	// 절대경로

	if (IDOK == Dlg.DoModal())
	{
		CString strFilePath = Dlg.GetPathName();

		wifstream fin;
		fin.open(strFilePath.GetString());

		if (fin.fail())
		{
			AfxMessageBox(strFilePath.GetString());
			return;
		}

		if (!m_ImgPathLst.empty())
			Release();

		m_ListBox.ResetContent();

		TCHAR szObjKey[MAX_STR] = L"";
		TCHAR szStateKey[MAX_STR] = L"";
		TCHAR szCount[MIN_STR] = L"";
		TCHAR szPath[MAX_STR] = L"";

		wstring wstrCombined = L"";
		IMGPATH* pImgPath = nullptr;

		while (true)
		{
			fin.getline(szObjKey, MAX_STR, '|');
			fin.getline(szStateKey, MAX_STR, '|');
			fin.getline(szCount, MIN_STR, '|');
			fin.getline(szPath, MAX_STR);

			if (fin.eof())	// EOF에 도달하면 break
				break;

			pImgPath = new IMGPATH;
			pImgPath->wstrObjKey = szObjKey;
			pImgPath->wstrStateKey = szStateKey;
			pImgPath->iCount = _ttoi(szCount);
			pImgPath->wstrPath = szPath;

			m_ImgPathLst.push_back(pImgPath);

			wstrCombined = wstring(szObjKey) + L"|" + szStateKey + L"|" + szCount + L"|"
				+ szPath;

			m_ListBox.AddString(wstrCombined.c_str());
		}

		fin.close();
	}

	SetHorizontalScroll();

	UpdateData(FALSE);
}
