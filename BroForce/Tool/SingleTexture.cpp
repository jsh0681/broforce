#include "stdafx.h"
#include "SingleTexture.h"


CSingleTexture::CSingleTexture()
	: m_pTexInfo(nullptr)
{
}


CSingleTexture::~CSingleTexture()
{
	Release();
}

const TEXINFO* CSingleTexture::GetTexInfo(const wstring & wstrStateKey, const int & iImgIndex) const
{
	return m_pTexInfo;
}

HRESULT CSingleTexture::InsertTexture(const wstring& wstrFilePath,
	const wstring& wstrStateKey, const int& iImgCount)
{
	if (nullptr == m_pTexInfo)
	{
		m_pTexInfo = new TEXINFO;
		ZeroMemory(m_pTexInfo, sizeof(TEXINFO));
	}

	if (FAILED(D3DXGetImageInfoFromFile(wstrFilePath.c_str(), &(m_pTexInfo->tImgInfo))))
	{
		ERR_MSG(wstrFilePath.c_str());
		return E_FAIL;
	}

	if (FAILED(D3DXCreateTextureFromFileEx(GET_INSTANCE(CDeviceMgr)->GetDevice(),
		wstrFilePath.c_str(), /* 이미지 경로 */
		m_pTexInfo->tImgInfo.Width, /* 이미지 가로 크기 */
		m_pTexInfo->tImgInfo.Height, /* 이미지 세로 크기 */
		m_pTexInfo->tImgInfo.MipLevels, /* 밉 레벨 */
		0, /* 불러올 이미지의 사용방식. 특수한 경우가 아닌 이상 0 */
		m_pTexInfo->tImgInfo.Format, /* 이미지의 픽셀 포맷 */
		D3DPOOL_MANAGED, /* 메모리 사용방식 */
		D3DX_DEFAULT, D3DX_DEFAULT, 0, nullptr, nullptr, &(m_pTexInfo->pTexture))))
	{
		ERR_MSG(L"SingleTexture Load Failed!");
		return E_FAIL;
	}

	return S_OK;
}

void CSingleTexture::Release()
{
	if (m_pTexInfo)
	{
		m_pTexInfo->pTexture->Release();
		delete m_pTexInfo;
		m_pTexInfo = nullptr;
	}
}
