#include "stdafx.h"
#include "Terrain.h"
#include "ToolView.h"
#include"MapTool.h"


CTerrain::CTerrain()
{
}


CTerrain::~CTerrain()
{
	Release();
}

void CTerrain::Initialize()
{
	m_vecTile.reserve(TILEX * TILEY);
	for (int i = 0; i < TILEY; ++i)
	{
		for (int j = 0; j < TILEX; ++j)
		{
			TILE* pTile = new TILE;

			float fX = (float)j * TILECX+ TILECX/2;
			float fY = (float)i * TILECY + TILECY / 2;

			pTile->vPos = { fX, fY, 0.f };
			pTile->vSize = { (float)TILECX, (float)TILECY, 0.f };
			pTile->byDrawID = 0;
			pTile->byOption = 0; 
			pTile->iIndex = j + (TILEX * i);


			m_vecTile.push_back(pTile);
		}
	}
}

void CTerrain::Render()
{
	
	D3DXMATRIX matScale, matTrans, matWorld;

	TCHAR szIndex[MIN_STR] = L"";

	float fScrollX = (float)m_pMainView->GetScrollPos(0);
	float fScrollY = (float)m_pMainView->GetScrollPos(1);

	for (size_t i = 0; i < m_vecTile.size(); ++i)
	{
		D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
		D3DXMatrixTranslation(&matTrans, m_vecTile[i]->vPos.x - fScrollX,
			m_vecTile[i]->vPos.y - fScrollY, 0.f);

		matWorld = matScale * matTrans;

		const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
			L"Tile", L"Tile", m_vecTile[i]->byDrawID);
		NULL_CHECK(pTexInfo);

		float fCenterX = pTexInfo->tImgInfo.Width*0.5f;
		float fCenterY = pTexInfo->tImgInfo.Height*0.5f;

		// 현재 윈도우 크기와 Directx의 후면버퍼 크기와의 비율을 구해야함.
		ResolutionSync(matWorld);

		GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
		GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
			&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	}
}



void CTerrain::Release()
{
	for_each(m_vecTile.begin(), m_vecTile.end(), SafeDelete<TILE*>);
	m_vecTile.clear();
	m_vecTile.shrink_to_fit();
}

void CTerrain::InsertTile(TILE * pTile)
{
	m_vecTile.push_back(pTile);
}

void CTerrain::TileChange(const D3DXVECTOR3& vPos, const BYTE& byDrawID,
	const BYTE& byOption)
{
	if (m_vecTile.empty())
		return;

	int iIndex = SelectIndex(vPos);

	if (-1 == iIndex)
		return;

	m_vecTile[iIndex]->byDrawID = byDrawID;
	m_vecTile[iIndex]->byOption = byOption;
}

int CTerrain::SelectIndex(const D3DXVECTOR3& vPos)
{
	for (size_t i = 0; i < m_vecTile.size(); ++i)
	{
		if (IsPicking(vPos, i))
			return i;
	}

	return -1;
}

bool CTerrain::IsPicking(const D3DXVECTOR3 & vPos, const size_t &iIndex)
{
	D3DXVECTOR3 vPoint[4] =
	{
		{ m_vecTile[iIndex]->vPos.x - TILECX * 0.5f, m_vecTile[iIndex]->vPos.y + TILECY * 0.5f, 0.f },
		{ m_vecTile[iIndex]->vPos.x + TILECX * 0.5f,  m_vecTile[iIndex]->vPos.y + TILECY * 0.5f, 0.f },
		{ m_vecTile[iIndex]->vPos.x + TILECX * 0.5f, m_vecTile[iIndex]->vPos.y - TILECY * 0.5f, 0.f },
		{ m_vecTile[iIndex]->vPos.x - TILECX * 0.5f, m_vecTile[iIndex]->vPos.y - TILECY * 0.5f, 0.f }
	};

	return (vPos.x > vPoint[0].x&&vPos.y < vPoint[0].y)
		&& (vPos.x < vPoint[1].x&&vPos.y < vPoint[1].y)
		&& (vPos.x < vPoint[2].x&&vPos.y > vPoint[2].y)
		&& (vPos.x > vPoint[3].x&&vPos.y > vPoint[3].y);
}

void CTerrain::ResolutionSync(D3DXMATRIX & matWorld)
{
	RECT rcView = {};
	m_pMainView->GetClientRect(&rcView);

	float fRatioX = WINCX / float(rcView.right - rcView.left);
	float fRatioY = WINCY / float(rcView.bottom - rcView.top);

	matWorld._11 *= fRatioX;
	matWorld._12 *= fRatioY;

	matWorld._21 *= fRatioX;
	matWorld._22 *= fRatioY;

	matWorld._31 *= fRatioX;
	matWorld._32 *= fRatioY;

	matWorld._41 *= fRatioX;
	matWorld._42 *= fRatioY;
}

void CTerrain::MiniRender()
{
	D3DXMATRIX matScale, matTrans, matWorld;

	float fScrollX = (float)m_pMainView->GetScrollPos(0);
	float fScrollY = (float)m_pMainView->GetScrollPos(1);

	float fRatioX = 0.3f;
	float fRatioY = 0.3f;

	float fCenterX = 0.f;
	float fCenterY = 0.f;

	for (size_t i = 0; i < m_vecTile.size(); ++i)
	{
		D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
		D3DXMatrixTranslation(&matTrans, m_vecTile[i]->vPos.x - fScrollX,
			m_vecTile[i]->vPos.y - fScrollY, 0.f);

		matWorld = matScale * matTrans;

		const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(
			L"Tile", L"Tile", m_vecTile[i]->byDrawID);
		NULL_CHECK(pTexInfo);

		fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
		fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

		matWorld._11 *= fRatioX;
		matWorld._12 *= fRatioY;

		matWorld._21 *= fRatioX;
		matWorld._22 *= fRatioY;

		matWorld._31 *= fRatioX;
		matWorld._32 *= fRatioY;

		matWorld._41 *= fRatioX;
		matWorld._42 *= fRatioY;

		GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
		GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
			&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	}
}