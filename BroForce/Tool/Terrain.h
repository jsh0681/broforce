#pragma once

class CToolView;

class CMapTool;

class CTerrain
{
public:
	CTerrain();
	~CTerrain();

public:
	void Initialize();
	void Render();
	void MiniRender();
	void Release();
public:
	const vector<TILE*>& GetVecTile()const { return m_vecTile; }
public:
	void SetMainView(CToolView* pMainView) { m_pMainView = pMainView; }

public:
	void InsertTile(TILE* pTile);
	void TileChange(const D3DXVECTOR3& vPos, const BYTE& byDrawID, const BYTE& byOption = 0);

public:
	int SelectIndex(const D3DXVECTOR3& vPos);
	bool IsPicking(const D3DXVECTOR3& vPos, const size_t& iIndex);
	void ResolutionSync(D3DXMATRIX& matWorld);
private:
	 vector<TILE*>	m_vecTile;
	CToolView*		m_pMainView;
};

