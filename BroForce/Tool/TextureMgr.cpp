#include "stdafx.h"
#include "TextureMgr.h"
#include "SingleTexture.h"
#include "MultiTexture.h"

IMPLEMENT_SINGLETON(CTextureMgr)

CTextureMgr::CTextureMgr()
{
}


CTextureMgr::~CTextureMgr()
{
	Release();
}

const TEXINFO* CTextureMgr::GetTexInfo(
	const wstring & wstrObjectKey, const wstring & wstrStateKey, const int & iImgIndex) const
{
	if (m_MapTexture.empty())
		return nullptr;

	auto& iter_find = m_MapTexture.find(wstrObjectKey);

	if (m_MapTexture.end() == iter_find)
		return nullptr;

	return iter_find->second->GetTexInfo(wstrStateKey, iImgIndex);
}

HRESULT CTextureMgr::InsertTexture(TEX_ID eTex_ID, const wstring& wstrFilePath,
	const wstring& wstrObjectKey, const wstring& wstrStateKey, const int& iImgCount)
{
	auto& iter_find = m_MapTexture.find(wstrObjectKey);

	if (m_MapTexture.end() == iter_find)
	{
		CTexture* pTexture = nullptr;

		switch (eTex_ID)
		{
		case TEX_SINGLE:
			pTexture = new CSingleTexture;
			break;
		case TEX_MULTI:
			pTexture = new CMultiTexture;
			break;
		}

		if (FAILED(pTexture->InsertTexture(wstrFilePath, wstrStateKey, iImgCount)))
		{
			ERR_MSG(L"InsertTexture Failed!!");
			return E_FAIL;
		}

		m_MapTexture[wstrObjectKey] = pTexture;
	}
	else if (TEX_MULTI == eTex_ID)
	{
		if (FAILED(m_MapTexture[wstrObjectKey]->InsertTexture(wstrFilePath,
			wstrStateKey, iImgCount)))
		{
			ERR_MSG(L"InsertTexture Failed!!");
			return E_FAIL;
		}
	}

	return S_OK;
}

HRESULT CTextureMgr::ReadImgPath(const wstring & wstrImagePath)
{
	wifstream fin;
	fin.open(wstrImagePath.c_str());

	if (fin.fail())
	{
		ERR_MSG(wstrImagePath.c_str());
		return E_FAIL;
	}

	TCHAR szObjKey[MAX_STR] = L"";
	TCHAR szStateKey[MAX_STR] = L"";
	TCHAR szCount[MIN_STR] = L"";
	TCHAR szPath[MAX_STR] = L"";

	while (true)
	{
		fin.getline(szObjKey, MAX_STR, '|');
		fin.getline(szStateKey, MAX_STR, '|');
		fin.getline(szCount, MIN_STR, '|');
		fin.getline(szPath, MAX_STR);

		if (fin.eof())	// EOF에 도달하면 break
			break;

		int iCount = _ttoi(szCount);

		if (FAILED(InsertTexture(TEX_MULTI, szPath, szObjKey, szStateKey, iCount)))
		{
			ERR_MSG(L"ReadImgPath Failed!");
			return E_FAIL;
		}
	}

	return S_OK;
}
void CTextureMgr::Release()
{
	for (auto& MyPair : m_MapTexture)
		SafeDelete(MyPair.second);

	m_MapTexture.clear();
}
