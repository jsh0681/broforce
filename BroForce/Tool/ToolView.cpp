// ToolView.cpp : CToolView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "Tool.h"
#endif

#include "ToolDoc.h"
#include "MainFrm.h"
#include "ToolView.h"
#include "MiniView.h"
#include "Terrain.h"
#include"MyForm.h"
#include"MapTool.h"
#include"Obstacle.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CToolView

IMPLEMENT_DYNCREATE(CToolView, CScrollView)

BEGIN_MESSAGE_MAP(CToolView, CScrollView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CScrollView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEHWHEEL()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()


// 전역변수
HWND g_hWnd;


// CToolView 생성/소멸

CToolView::CToolView()
	: m_pTerrain(nullptr), m_pObstacle(nullptr)
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CToolView::~CToolView()
{
	SafeDelete(m_pObstacle);
	// 해제 순서 주의
	SafeDelete(m_pTerrain);

	GET_INSTANCE(CTextureMgr)->DestroyInstance();
	GET_INSTANCE(CDeviceMgr)->DestroyInstance();
}

BOOL CToolView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CScrollView::PreCreateWindow(cs);
}

// CToolView 그리기

void CToolView::OnDraw(CDC* pDC)
{
	CToolDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
	GET_INSTANCE(CDeviceMgr)->Render_Begin();

	CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	CToolView* pMyForm = dynamic_cast<CToolView*>(pMainFrame->m_MainSplit.GetPane(0, 1));

	
	
	const TEXINFO* pTexInfo = GET_INSTANCE(CTextureMgr)->GetTexInfo(L"StageBoss");
	NULL_CHECK(pTexInfo);

	float fCenterX = pTexInfo->tImgInfo.Width * 0.5f;
	float fCenterY = pTexInfo->tImgInfo.Height * 0.5f;

	pMyForm->SetScrollRange(0, 0, pTexInfo->tImgInfo.Width, 1);
	pMyForm->SetScrollRange(1, 0, pTexInfo->tImgInfo.Height, 1);
	float scrollx = (FLOAT)pMyForm->GetScrollPos(0);
	float scrolly = (FLOAT)pMyForm->GetScrollPos(1);

	D3DXMATRIX matScale, matRotZ, matTrans, matWorld;

	D3DXMatrixScaling(&matScale, 1.f, 1.f, 0.f);
	D3DXMatrixRotationZ(&matRotZ, -D3DXToRadian(0.f));
	D3DXMatrixTranslation(&matTrans, pTexInfo->tImgInfo.Width * 0.5f - scrollx, pTexInfo->tImgInfo.Height * 0.5f - scrolly, 0.f);

	matWorld = matScale * matRotZ * matTrans;

	GET_INSTANCE(CDeviceMgr)->GetSprite()->SetTransform(&matWorld);
	GET_INSTANCE(CDeviceMgr)->GetSprite()->Draw(pTexInfo->pTexture, nullptr,
		&D3DXVECTOR3(fCenterX, fCenterY, 0.f), nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));



	m_pTerrain->Render();
	m_pObstacle->Render();

	GET_INSTANCE(CDeviceMgr)->Render_End();
}


// CToolView 인쇄

BOOL CToolView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CToolView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CToolView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CToolView 진단

#ifdef _DEBUG
void CToolView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CToolView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CToolDoc* CToolView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CToolDoc)));
	return (CToolDoc*)m_pDocument;
}
#endif //_DEBUG


// CToolView 메시지 처리기


void CToolView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	// 스크롤 셋팅
	CScrollView::SetScrollSizes(MM_TEXT, CSize(TILECX * TILEX, TILECY * (TILEY / 2)));

	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	g_hWnd = m_hWnd;

	// AfxGetApp: MainApp을 얻어오는 함수
	// CWinApp::GetMainWnd: App이 갖고 있는 프레임 윈도를 얻어오는 함수.
	CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());

	RECT rcMainFrm = {};
	// 현재 윈도우의 전체 RECT 크기를 얻어옴. (스크린 전체 좌표 기준)
	pMainFrm->GetWindowRect(&rcMainFrm);

	// SetRect: RECT를 새로 조정하는 함수.
	SetRect(&rcMainFrm, 0, 0, rcMainFrm.right - rcMainFrm.left,
		rcMainFrm.bottom - rcMainFrm.top);

	RECT rcView = {};
	GetClientRect(&rcView); // 실제 Client 영역의 RECT 크기를 얻어옴. (0, 0 기준)

	int iRowFrm = rcMainFrm.right - rcView.right; // FrmWnd와 View의 가로 간격
	int iColFrm = rcMainFrm.bottom - rcView.bottom; // FrmWnd와 View의 세로 간격

	pMainFrm->SetWindowPos(nullptr, 200, 200, WINCX + iRowFrm, WINCY + iColFrm, SWP_NOZORDER);

	if (FAILED(GET_INSTANCE(CDeviceMgr)->InitDevice()))
	{
		AfxMessageBox(L"InitDevice Failed!!");
		return;
	}


	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_MULTI,
		L"../Texture/Stage/Tile/Tile%d.png", L"Tile", L"Tile", 64)))
		return;


	if (FAILED(GET_INSTANCE(CTextureMgr)->ReadImgPath(L"../Data/ImgPath.txt")))
		return;
	


	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/StageOne.png", L"StageOne")))
		return;
	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/StageTwo.png", L"StageTwo")))
		return;
	if (FAILED(GET_INSTANCE(CTextureMgr)->InsertTexture(CTextureMgr::TEX_SINGLE,
		L"../Texture/StageBoss.png", L"StageBoss")))
		return;

	if (nullptr == m_pTerrain)
	{
		m_pTerrain = new CTerrain;
		m_pTerrain->Initialize();
		m_pTerrain->SetMainView(this);
	}

	if (nullptr == m_pObstacle)
	{
		m_pObstacle = new CObstacle;
		m_pObstacle->Initialize();
		m_pObstacle->SetMainView(this);
	}


}

// OnLButtonDown: 마우스 한번 클릭(KeyDOWN)했을 때 호출
void CToolView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CScrollView::OnLButtonDown(nFlags, point);

	CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	CMyForm* pMyForm = dynamic_cast<CMyForm*>(pMainFrame->m_SecondSplit.GetPane(1, 0));
	// IsWindowVisible: 창이 표시되는지 여부를 확인합니다.
	if (FALSE == pMyForm->m_MapTool.IsWindowVisible())
	{
		//AfxMessageBox(L"MapTool 안열림");
		return;
	}
	int iDrawID = pMyForm->m_MapTool.m_iDrawID;
	int iOption = pMyForm->m_MapTool.m_byOption;

	float fScrollX = (float)CScrollView::GetScrollPos(0);
	float fScrollY = (float)CScrollView::GetScrollPos(1);

	m_pTerrain->TileChange(D3DXVECTOR3((float)point.x + fScrollX, (float)point.y + fScrollY, 0.f), iDrawID, iOption);

	Invalidate(FALSE);

	CMiniView* pMiniView = dynamic_cast<CMiniView*>(pMainFrame->m_SecondSplit.GetPane(0, 0));
	pMiniView->Invalidate(FALSE);
}


// OnMouseMove: 마우스가 움직이는 동안 호출.
void CToolView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CScrollView::OnMouseMove(nFlags, point);

	CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	CMyForm* pMyForm = dynamic_cast<CMyForm*>(pMainFrame->m_SecondSplit.GetPane(1, 0));

	if (FALSE == pMyForm->m_MapTool.IsWindowVisible())
	{
		//AfxMessageBox(L"MapTool 안열림");
		return;
	}

	int iDrawID = pMyForm->m_MapTool.m_iDrawID;
	int iOption = pMyForm->m_MapTool.m_byOption;

	float fScrollX = (float)CScrollView::GetScrollPos(0);
	float fScrollY = (float)CScrollView::GetScrollPos(1);

	if (GetAsyncKeyState(VK_LBUTTON) & 0x8000)
		m_pTerrain->TileChange(D3DXVECTOR3((float)point.x + fScrollX,
		(float)point.y + fScrollY, 0.f), iDrawID, iOption);

	// Invalidate: 현재 view의 화면을 갱신하는 함수. WM_PAINT메시지가 발생.
	Invalidate(FALSE);
	CMiniView* pMiniView = dynamic_cast<CMiniView*>(pMainFrame->m_SecondSplit.GetPane(0, 0));
	pMiniView->Invalidate(FALSE);
}





void CToolView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CScrollView::OnRButtonDown(nFlags, point);
	CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	CMyForm* pMyForm = dynamic_cast<CMyForm*>(pMainFrame->m_SecondSplit.GetPane(1, 0));
	// IsWindowVisible: 창이 표시되는지 여부를 확인합니다.

	if (FALSE == pMyForm->m_ObjTool.IsWindowVisible())
	{
		//AfxMessageBox(L"MapTool 안열림");
		return;
	}
	int iDrawID = pMyForm->m_ObjTool.m_iDrawID;
	int iOption = pMyForm->m_ObjTool.m_byOption;
	float fScrollX = (float)CScrollView::GetScrollPos(0);
	float fScrollY = (float)CScrollView::GetScrollPos(1);

	m_pObstacle->TileChange(D3DXVECTOR3((float)point.x + fScrollX, (float)point.y + fScrollY, 0.f), D3DXVECTOR3(-1.f, 1.f, 0.f), iDrawID, iOption);

	Invalidate(FALSE);

	CMiniView* pMiniView = dynamic_cast<CMiniView*>(pMainFrame->m_SecondSplit.GetPane(0, 0));
	pMiniView->Invalidate(FALSE);
}
